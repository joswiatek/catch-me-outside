# Catch Me Outside

Catch Me Outside: Find local communities, green spaces, and activities related to health and fitness!



## Git SHA

Phase One: 44286250b5ffebfc08d924d5211fcafbd2509031

Phase Two: 4409d8d165312e91321befbb7427b5a10277e6d6 

Phase Three: 8dd20631ceb9d470274c4b0a4113937a444144c4

Phase Four: cabf2a540375fd22b8d7cc911139d1eb41595522
Note: We made a few graphical changes to our frontend after the submission deadline. This included adding the customer visualizations, and changing the framerates/translation sizes of the animations on the splash page.


## Gitlab Pipelines

Our pipelines can be found [here](https://gitlab.com/joswiatek/catch-me-outside/pipelines).



## Website

Our production website can be found [here](https://catchmeoutside.today/).



## Completion Times Per Member

| Phase | Estimated | Actual |
| ----- | --------- | ------ |
| One   | 20        | 15     |
| Two   | 40        | 35     |
| Three | 25        | 40     |
| Four  | 10        |  8     |

## Group Members ##

| Name             | EID     | Gitlab Username |
| ---------------- | ------- | --------------- |
| Reed Hamilton    | rjh2895 | reedham1        |
| Coleman Oei      | ceo698  | SeaOeid         |
| Joel Swiatek     | jcs5369 | joswiatek       |
| Lindsey Thompson | lt9245  | lindseythompson |
| Yen Chen Wee     | yw7766  | yenchen21       |



## Notes

- Added splash page visual changes after deadline Hannah said to make a note of this
