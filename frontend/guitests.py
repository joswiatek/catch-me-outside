import unittest
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import time

class FrontendGuiTest(unittest.TestCase):
    """Include test cases on a given url"""

    def setUp(self):
        """Start web driver"""
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        self.driver = webdriver.Chrome(options=chrome_options)
        self.driver.implicitly_wait(10)
        self.baseurl = "http://localhost:3000/"

    def tearDown(self):
        """Stop web driver"""
        self.driver.quit()

    # --------------------
    # Splash Page Tests
    # --------------------
    def test_splash_page_space_card(self):
        """Find and click space splashcard on Splash Page"""
        try:
            self.driver.get(self.baseurl)
            el = self.driver.find_element_by_id("spaceSplashCard")
            el.click()
            self.assertEqual(self.driver.current_url, self.baseurl+"spaces")
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_splash_page_event_card(self):
        """Find and click event splashcard on Splash Page"""
        try:
            self.driver.get(self.baseurl)
            el = self.driver.find_element_by_id("eventSplashCard")
            el.click()
            self.assertEqual(self.driver.current_url, self.baseurl+"events")
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_splash_page_group_card(self):
        """Find and click group splashcard on Splash Page"""
        try:
            self.driver.get(self.baseurl)
            el = self.driver.find_element_by_id("groupSplashCard")
            el.click()
            self.assertEqual(self.driver.current_url, self.baseurl+"groups")
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_splash_page_search_bar(self):
        """Use search bar on Splash Page"""
        try:
            self.driver.get(self.baseurl)
            el = self.driver.find_element_by_id("globalSearchBar")
            el.send_keys("park")
            el.send_keys(Keys.ENTER)
            self.assertEqual(self.driver.current_url, self.baseurl+"search")
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    # --------------------
    # Navigation Bar Tests
    # --------------------

    def test_navbar_eventButton(self):
        """Find and click event button on navigation bar"""
        try:
            self.driver.get(self.baseurl)
            el = self.driver.find_element_by_id("eventNavButton")
            el.click()
            self.assertEqual(self.driver.current_url, self.baseurl+"events")
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_navbar_AboutButton(self):
        """Find and click about button on navigation bar"""
        try:
            self.driver.get(self.baseurl)
            el = self.driver.find_element_by_id("aboutNavButton")
            el.click()
            self.assertEqual(self.driver.current_url, self.baseurl+"about")
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_navbar_SpaceButton(self):
        """Find and click event button on navigation bar"""
        try:
            self.driver.get(self.baseurl)
            el = self.driver.find_element_by_id("spaceNavButton")
            el.click()
            self.assertEqual(self.driver.current_url, self.baseurl+"spaces")
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_navbar_groupButton(self):
        """Find and click event button on navigation bar"""
        try:
            self.driver.get(self.baseurl)
            el = self.driver.find_element_by_id("groupNavButton")
            el.click()
            self.assertEqual(self.driver.current_url, self.baseurl+"groups")
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_navbar_returnButton(self):
        """Find and click Home button on navigation bar"""
        try:
            # Start on events page and navigate to splash page
            self.driver.get(self.baseurl + "events")
            el = self.driver.find_element_by_id("homeLink")
            el.click()
            self.assertEqual(self.driver.current_url, self.baseurl)
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    # --------------------
    # Events Page Tests
    # --------------------

    def test_event_eventcard(self):
        """Find and click event card on Events page"""
        # Click on an eventcard and store name of event chosen
        try:
            self.driver.get(self.baseurl + "events")
            try:
                el = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "eventCard"))
                )
                el2 = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "eventCardName"))
                )
            except:
                self.driver.quit()
                self.fail("Driver timed out or could not find element")
            eventName = el2.text
            el.click()
            # Check if the url is in the correct format
            self.assertRegex(self.driver.current_url, self.baseurl + "events/[\d]*")
            try:
                el = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "eventSummaryName"))
                )
            except:
                self.driver.quit()
                self.fail("Driver timed out or could not find element.")
            # Check if event card led to correct event
            self.assertEqual(eventName, el.text)


        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_event_groupCard(self):
        """Find and click associated GroupCard on an Event Instance Page"""
        try:
            # Navigate to instance page
            self.driver.get(self.baseurl + "events")
            try:
                el = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "eventCard"))
                )
            except:
                self.driver.quit()
                self.fail("Driver timed out waiting for event page load")
            el.click()
            self.assertRegex(self.driver.current_url, self.baseurl+"events/[\d]*")
            # Find and click group card
            try:
                el = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "eventGroupCard"))
                )
                el2 = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "groupCardName"))
                )
            except NoSuchElementException as ex:
                self.fail(ex.msg)
            except:
                self.driver.quit()
                self.fail("Driver timed out waiting for group page load")
            groupName = el2.text
            el.click()
            # Check format of resulting url
            self.assertRegex(self.driver.current_url, self.baseurl + "groups/*")

            try:
                time.sleep(2)
                el = self.driver.find_element_by_id("groupName")
            except NoSuchElementException as ex:
                self.fail(ex.msg)
            except:
                self.driver.quit()
                self.fail("Driver timed out waiting for group page load")

            self.assertEqual(groupName, el.text, "Text: " + el.text + "at " + self.driver.current_url)
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_event_spaceCard(self):
        """Find and click associated SpaceCard on an Event Instance Page"""
        try:
            # Navigate to instance page
            self.driver.get(self.baseurl + "events")
            try:
                el = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "eventCard"))
                )
            except:
                self.driver.quit()
                self.fail("Driver timed out waiting for event page load")
            el.click()
            self.assertRegex(self.driver.current_url, self.baseurl+"events/[\d]*")
            # Find and click space card and store name of space chosen
            try:
                el = WebDriverWait(self.driver, 20).until(
                    EC.presence_of_element_located((By.ID, "eventSpaceCard"))
                )
                el2 = WebDriverWait(self.driver, 20).until(
                    EC.presence_of_element_located((By.ID, "spaceCardName"))
                )
            except NoSuchElementException as ex:
                self.fail(ex.msg)
            except:
                self.driver.quit()
                self.fail("Driver timed out waiting for group page load")
            spaceName = el2.text
            el.click()
            # Check if resulting url after click is correct
            self.assertRegex(self.driver.current_url, self.baseurl + "spaces/*")
            try:
                time.sleep(2)
                el = self.driver.find_element_by_id("spaceName")
            except NoSuchElementException as ex:
                self.fail(ex.msg)
            except:
                self.driver.quit()
                self.fail("Driver timed out waiting for space page load")
            self.assertEqual(spaceName, el.text)

        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_event_search_bar(self):
        """ Input a search into event search bar"""
        try:
            # Navigate to instance page
            self.driver.get(self.baseurl + "events")
            try:
                # Input search into search bar
                el = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "eventSearchInput"))
                )
                el.send_keys("hike")
                # Click search button
                el2 = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "eventSearchButton"))
                )
                el2.click()
            except:
                self.driver.quit()
                self.fail("Driver timed out waiting for event page load")
            try:
                # Check if results list renders
                el3 = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "eventSearchResultList"))
                )
            except:
                self.driver.quit()
                self.fail("Result list does not render on search.")
            try:
                # Check that normal event cards are not rendered after search
                el4 = self.driver.find_element_by_id("eventCard")
                self.fail("Normal Event cards rendered instead of search results.")
            except:
                pass
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_event_search_result_click(self):
        """ Input a search into event search bar and click on a result"""
        try:
            # Navigate to instance page
            self.driver.get(self.baseurl + "events")
            try:
                el = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "eventSearchInput"))
                )
                el.send_keys("hike")
                el2 = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "eventSearchButton"))
                )
                el2.click()
            except:
                self.driver.quit()
                self.fail("Driver timed out waiting for event page load")
            try:
                el3 = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "eventResultCard"))
                )
                el3.click()
                self.assertRegex(self.driver.current_url, self.baseurl + "events/[\d]*")
            except:
                self.driver.quit()
                self.fail("Result card does not render on search.")
        except NoSuchElementException as ex:
            self.fail(ex.msg)
    # --------------------
    # Space Page Tests
    # --------------------

    def test_space_spacecard(self):
        """Find and click space card on space page"""
        # Click on an spacecard and store name of space chosen
        try:
            self.driver.get(self.baseurl + "spaces")
            try:
                el = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "spaceCard"))
                )
                el2 = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "spaceCardName"))
                )
            except:
                self.driver.quit()
                self.fail("Driver timed out or could not find element")
            spaceName = el2.text
            el.click()
            # Check if the url is in the correct format
            self.assertRegex(self.driver.current_url, self.baseurl + "spaces/[\d]*")
            try:
                time.sleep(1)
                el = self.driver.find_element_by_id('spaceName')
            except:
                self.driver.quit()
                self.fail("Driver timed out or could not find element.")
            # Check if event card led to correct event
            self.assertEqual(spaceName, el.text)

        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_space_groupCard(self):
        """Find and click associated GroupCard on an Space Instance Page"""
        try:
            # Navigate to instance page
            self.driver.get(self.baseurl + "spaces")
            try:
                el = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "spaceCard"))
                )
            except:
                self.driver.quit()
                self.fail("Driver timed out waiting for space page load")
            el.click()
            self.assertRegex(self.driver.current_url, self.baseurl+"spaces/[\d]*")
            # Find and click group card
            try:
                el = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "spaceGroupCard"))
                )
                el2 = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "groupCardName"))
                )
            except NoSuchElementException as ex:
                self.fail(ex.msg)
            except:
                self.driver.quit()
                self.fail("Driver timed out waiting for group page load")
            el.click()
            # Check format of resulting url
            self.assertRegex(self.driver.current_url, self.baseurl + "groups/*")

        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_space_eventCard(self):
        """Find and click associated EventCard on an Space Instance Page"""
        try:
            # Navigate to instance page
            self.driver.get(self.baseurl + "spaces")
            try:
                el = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "spaceCard"))
                )
            except:
                self.driver.quit()
                self.fail("Driver timed out waiting for space page load")
            el.click()
            self.assertRegex(self.driver.current_url, self.baseurl+"spaces/[\d]*")
            time.sleep(3)
            # Find and click space card and store name of space chosen
            try:
                el = WebDriverWait(self.driver, 20).until(
                    EC.presence_of_element_located((By.ID, "spaceEventCard"))
                )
                el2 = WebDriverWait(self.driver, 20).until(
                    EC.presence_of_element_located((By.ID, "eventCardName"))
                )
            except NoSuchElementException as ex:
                self.fail(ex.msg)
            except:
                self.driver.quit()
                self.fail("Driver timed out waiting for event page load")
            spaceName = el2.text
            el.click()
            time.sleep(3)
            # Check if resulting url after click is correct
            self.assertRegex(self.driver.current_url, self.baseurl + "events/*")
            try:
                time.sleep(1)
                el = self.driver.find_element_by_id("eventSummaryName")
            except NoSuchElementException as ex:
                self.fail(ex.msg)
            except:
                self.driver.quit()
                self.fail("Driver timed out waiting for space page load")
            self.assertEqual(spaceName, el.text)

        except NoSuchElementException as ex:
            self.fail(ex.msg)

    # --------------------
    # Group Page Tests
    # --------------------
    def test_group_groupcard(self):
        """Find and click group card on group page"""
        # Click on an spacecard and store name of space chosen
        try:
            self.driver.get(self.baseurl + "groups")
            try:
                el = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "groupCard"))
                )
                el2 = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "groupCardName"))
                )
            except:
                self.driver.quit()
                self.fail("Driver timed out or could not find element")
            groupName = el2.text
            el.click()
            # Check if the url is in the correct format
            self.assertRegex(self.driver.current_url, self.baseurl + "groups/[\d]*")
            try:
                time.sleep(1)
                el = self.driver.find_element_by_id('groupName')
            except:
                self.driver.quit()
                self.fail("Driver timed out or could not find element.")
            # Check if event card led to correct event
            self.assertEqual(groupName, el.text)

        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_group_spaceCard(self):
        """Find and click associated spaceCard on an Group Instance Page"""
        try:
            # Navigate to instance page
            self.driver.get(self.baseurl + "groups")
            try:
                el = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "groupCard"))
                )
            except:
                self.driver.quit()
                self.fail("Driver timed out waiting for group page load")
            el.click()
            self.assertRegex(self.driver.current_url, self.baseurl+"groups/[\d]*")
            # Find and click space card
            try:
                el = WebDriverWait(self.driver, 20).until(
                    EC.presence_of_element_located((By.ID, "groupSpaceCard"))
                )
                el2 = WebDriverWait(self.driver, 20).until(
                    EC.presence_of_element_located((By.ID, "spaceCardName"))
                )
            except NoSuchElementException as ex:
                self.fail(ex.msg)
            except:
                self.driver.quit()
                self.fail("Driver timed out waiting for group instance page load")
            spaceName = el2.text
            el.click()
            # Check format of resulting url
            self.assertRegex(self.driver.current_url, self.baseurl + "spaces/*")

            try:
                time.sleep(2)
                el = self.driver.find_element_by_id("spaceName")
            except NoSuchElementException as ex:
                self.fail(ex.msg)
            except:
                self.driver.quit()
                self.fail("Driver timed out waiting for space page load")

            self.assertEqual(spaceName, el.text)
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_group_eventCard(self):
        """Find and click associated EventCard on an Group Instance Page"""
        try:
            # Navigate to instance page
            self.driver.get(self.baseurl + "groups")
            try:
                el = WebDriverWait(self.driver, 3).until(
                    EC.presence_of_element_located((By.ID, "groupCard"))
                )
            except:
                self.driver.quit()
                self.fail("Driver timed out waiting for group page load")
            el.click()
            self.assertRegex(self.driver.current_url, self.baseurl+"groups/[\d]*")
            time.sleep(3)
            # Find and click space card and store name of space chosen
            try:
                el = WebDriverWait(self.driver, 20).until(
                    EC.presence_of_element_located((By.ID, "groupEventCard"))
                )
                el2 = WebDriverWait(self.driver, 20).until(
                    EC.presence_of_element_located((By.ID, "eventCardName"))
                )
            except NoSuchElementException as ex:
                self.fail(ex.msg)
            except:
                self.driver.quit()
                self.fail("Driver timed out waiting for group instance page load")
            eventName = el2.text
            el.click()
            time.sleep(3)
            # Check if resulting url after click is correct
            self.assertRegex(self.driver.current_url, self.baseurl + "events/*")
            try:
                time.sleep(1)
                el = self.driver.find_element_by_id("eventSummaryName")
            except NoSuchElementException as ex:
                self.fail(ex.msg)
            except:
                self.driver.quit()
                self.fail("Driver timed out waiting for event page load")
            self.assertEqual(eventName, el.text)

        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_group_search_bar(self):
        """ Input a search into group search bar"""
        try:
            # Navigate to instance page
            self.driver.get(self.baseurl + "groups")
            try:
                # Input search into search bar
                el = self.driver.find_element_by_id("groupSearchInput")
                el.send_keys("hike")
                # Click search button
                el2 = self.driver.find_element_by_id("groupSearchButton")
                el2.click()
            except:
                self.driver.quit()
                self.fail("Driver timed out waiting for group page load")
                # Check if results list renders
            el3 = WebDriverWait(self.driver, 3).until(
                EC.presence_of_element_located((By.ID, "groupSearchResultList"))
            )
            try:
                # Check that normal event cards are not rendered after search
                el4 = self.driver.find_element_by_id("groupCard")
                self.fail("Normal group cards rendered instead of search results.")
            except:
                pass
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_group_search_result_click(self):
        """ Input a search into groups search bar and click on a result"""
        try:
            # Navigate to instance page
            self.driver.get(self.baseurl + "groups")
            try:
                el = self.driver.find_element_by_id('groupSearchInput')
                el.send_keys("hike")
                el2 = self.driver.find_element_by_id('groupSearchButton')
                el2.click()
            except:
                self.driver.quit()
                self.fail("Driver timed out waiting for group page load")
            el3 = WebDriverWait(self.driver, 3).until(
                EC.presence_of_element_located((By.ID, "groupResultCard"))
            )
            el3.click()
            # Check if group instance page is destination
            self.assertRegex(self.driver.current_url, self.baseurl + "groups/[\d]*")
        except NoSuchElementException as ex:
            self.fail(ex.msg)
    # --------------------
    # Global Search Page Tests
    # --------------------
    def test_search_bar_propagate(self):
        """ Test if a query from the splash page propagates to the bar in the search"""
        try:
            self.driver.get(self.baseurl)
            el = self.driver.find_element_by_id("globalSearchBar")
            el.send_keys("park")
            el.send_keys(Keys.ENTER)
            self.assertEqual(self.driver.current_url, self.baseurl+"search")
            el2 = self.driver.find_element_by_id("globalSearchBar")
            self.assertEqual("park", el2.get_attribute("value"))
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_search_bar_enter(self):
        """ Test if an enter key will initiate search """
        try:
            self.driver.get(self.baseurl + "search")
            el = self.driver.find_element_by_id("globalSearchBar")
            el.send_keys("park")
            el.send_keys(Keys.ENTER)
        except NoSuchElementException as ex:
            self.fail(ex.msg)
        try:
            # Check if results list renders
            el3 = WebDriverWait(self.driver, 3).until(
                EC.presence_of_element_located((By.ID, "globalEventResult"))
            )
        except:
            self.driver.quit()
            self.fail("Result list does not render on search.")

    def test_search_bar_click(self):
        """ Test if a button click will initiate search """
        try:
            self.driver.get(self.baseurl + "search")
            el = self.driver.find_element_by_id("globalSearchBar")
            el2 = self.driver.find_element_by_id("globalSearchButton")
            el.send_keys("park")
            el2.click()
        except NoSuchElementException as ex:
            self.fail(ex.msg)
        try:
            # Check if results list renders
            el3 = WebDriverWait(self.driver, 3).until(
                EC.presence_of_element_located((By.ID, "globalEventResult"))
            )
        except:
            self.driver.quit()
            self.fail("Result list does not render on search.")
    def test_search_group_tab(self):
        """ Tests group results tab """
        try:
            self.driver.get(self.baseurl + "search")
            el = self.driver.find_element_by_id("globalSearchBar")
            el.send_keys("park")
            el.send_keys(Keys.ENTER)
        except NoSuchElementException as ex:
            self.fail(ex.msg)
        try:
            el = self.driver.find_element_by_id("groupSearchTab")
            el.click()
            time.sleep(1)
            el2 = self.driver.find_element_by_id("globalGroupResult")
        except:
            self.driver.quit()
            self.fail("Result list does not render on search.")
    def test_search_event_tab(self):
        """ Tests event results tab """
        try:
            self.driver.get(self.baseurl + "search")
            el = self.driver.find_element_by_id("globalSearchBar")
            el.send_keys("park")
            el.send_keys(Keys.ENTER)
        except NoSuchElementException as ex:
            self.fail(ex.msg)
        try:
            el = self.driver.find_element_by_id("groupSearchTab")
            el.click()
            time.sleep(.1)
            el = self.driver.find_element_by_id("eventSearchTab")
            el.click()
            time.sleep(1)
            el2 = self.driver.find_element_by_id("globalEventResult")
        except:
            self.driver.quit()
            self.fail("Result list does not render on search.")

    def test_search_space_tab(self):
        """ Tests space results tab """
        try:
            self.driver.get(self.baseurl + "search")
            el = self.driver.find_element_by_id("globalSearchBar")
            el.send_keys("park")
            el.send_keys(Keys.ENTER)
        except NoSuchElementException as ex:
            self.fail(ex.msg)
        try:
            el = self.driver.find_element_by_id("spaceSearchTab")
            el.click()
            time.sleep(1)
            el2 = self.driver.find_element_by_id("globalSpaceResult")
        except:
            self.driver.quit()
            self.fail("Result list does not render on search.")

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(FrontendGuiTest)
    result = unittest.TextTestRunner(verbosity=2).run(suite)
    #unittest.main()

    if len(result.failures) + len(result.errors) > 0:
        print("YOUR TESTS FAILED")
        with open('result-file.txt', 'w') as f:
            f.write('FAIL')
    else:
        print("YOUR TESTS PASSED")
        with open('result-file.txt', 'w') as f:
            f.write('PASS')
