import React, {Component} from 'react'
import PropTypes from "prop-types";
import Sort from '../Sort.js';
import MultipleSelect from '../filter/MultipleSelect.js';
import MinMaxInput from '../filter/MinMaxInput.js';
import SingleSelectDropdown from '../filter/SingleSelectDropdown.js';
import Typography from '@material-ui/core/Typography';

const joinModes = [
  {
    name: 'Open',
    value: 'open'
  }, {
    name: 'Closed',
    value: 'closed'
  }, {
    name: 'Approval',
    value: 'approval'
  }
];

const choices = [
  {
    value: 'member_count',
    displayName: '# of members',
    defOrder: 'desc'
  }, {
    value: 'event_count',
    displayName: '# of events',
    defOrder: 'desc'
  }, {
    value: 'name',
    displayName: 'A to Z',
    defOrder: 'asc'
  }, {
    value: 'creation_time',
    displayName: 'Creation time',
    defOrder: 'asc'
  }
];

class GroupFilterSortBar extends Component {
  render() {
    return (<div className="filter-sort-container">
      <div className="filter-body">
        <Typography variant='subtitle1' align='left' style={{
            'fontWeight' : '600',
            'color' : '#1BC86C'
          }}>Filter by</Typography>
        <div className="filter-item">
          <Typography variant='subtitle2' align='left' style={{
              'fontWeight' : '600'
            }}>Group Category</Typography>
          <MultipleSelect updateSelection={this.props.updateGroupFilterCategory.bind(this)}/>
        </div>
        <div className="filter-item">
          <Typography variant='subtitle2' align='left' style={{
              'fontWeight' : '600',
              paddingBottom: 15
            }}>Join Mode</Typography>
          <SingleSelectDropdown updateSelection={this.props.updateGroupFilterJoinMode.bind(this)} list={joinModes} name="Join Mode"/>
        </div>
        <div className="filter-item">
          <Typography variant='subtitle2' align='left' style={{
              'fontWeight' : '600'
            }}>Group Size</Typography>
          <MinMaxInput returnFilter={this.props.updateGroupFilterMembers.bind(this)}/>
        </div>
      </div>
      <div className="sort-body">
        <Typography variant='subtitle1' align='left' style={{
            'fontWeight' : '600',
            'color' : '#1BC86C'
          }}>Sort by</Typography>
        <Sort options={choices} updateSortData={this.props.updateGroupSortData.bind(this)}/>
      </div>
    </div>)
  }
}

GroupFilterSortBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default GroupFilterSortBar;
