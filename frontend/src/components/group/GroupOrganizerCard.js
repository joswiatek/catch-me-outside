import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardMedia from '@material-ui/core/CardMedia';

const styles = {
  card: {
    maxWidth: '33%'
  },
  media: {
    height: '250',
    width: '40%'
  }
};

const GroupOrganizerCard = (props) => {
  let bio = props.data.organizer_bio
  if (bio === "") {
    bio = "No bio available."
  }
  let name = props.data.organizer_name
  let photo = props.data.organizer_photo
  return (<Card style={styles.media}>
    <CardMedia src='img' style={{
        'paddingTop' : '15px',
        'textAlign' : 'center'
      }}>
      <img src={photo} alt={name} style={{
          'height' : '80px',
          'width' : 'auto'
        }}/>
    </CardMedia>
    <CardContent style={{
        maxHeight: 'auto',
        overflow: 'hidden'
      }}>
      <Typography gutterBottom="gutterBottom" variant='h6' component="h6" align='center' style={{
          'maxHeight' : '20px',
          'overflow' : 'hidden'
        }}>
        {name}
      </Typography>
      <Typography component="p" variant='caption' align="left" style={{
          'marginTop' : '-8px',
          'marginBottom' : '10px',
          'maxHeight' : '115px',
          'overflow' : 'hidden'
        }}>
        {bio}
      </Typography>
    </CardContent>
  </Card>)
}

GroupOrganizerCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(GroupOrganizerCard);
