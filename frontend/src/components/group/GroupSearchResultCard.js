import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import {Link} from "react-router-dom";
import Highlighter from "react-highlight-words";
import {getHighlightWords, getSnippets} from '../../config'

import API from '../../config/api.js'

const styles = {
  card: {
    maxWidth: '100%',
    display: 'flex',
    flexDirection: 'row'
  },
  media: {
    width: '35%',
    height: '100%',
    float: 'left',
    marginBottom: '5px',
    marginRight: '15px'
  },
  mediaColumn: {
    height: '100%',
    float: 'left'
  },
  content: {
    flex: '1 0 auto',
    height: '200px'
  },
  icon: {
    fontSize: '18px',
    verticalAlign: 'top',
    paddingBottom: '1px',
    paddingRight: '5px',
    position: 'relative',
    top: '-1px',
    color: '#1BC86C'
  }
};

class GroupSearchResultCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      photo: ""
    }
  }
  componentDidMount() {
    // Try to set the photo from the photo, but if it's not present then set a
    // flag to get the space photo
    if (this.props.item.group_photo === "") {
      // Get the events for this group
      API.getSpaceForGroup(this.props.item.group_id).then(json => {
        // Get the space for the first event
        let photo_num = Math.floor(Math.random() * 10);
        let photo = JSON.parse(json.data.photos[photo_num]);
        let photo_url = API.buildSpaceImageURL(json.data.space_id, photo['photo_reference']);
        this.setState({photo: photo_url});
      });
    } else {
      // If there is a group photo then use it
      this.setState({photo: this.props.item.group_photo});
    }
  };
  render() {
    return (<Card className={this.props.classes.card} id={this.props.id}>
      <CardActionArea component={Link} to={"/groups/" + this.props.item.group_id}>
        <CardMedia className={this.props.classes.media} image={this.state.photo} title=""></CardMedia>
        <CardContent className={this.props.classes.content} overflow='hidden'>
          <Typography id='groupSearchCardName' gutterBottom="gutterBottom" variant="h5" component="h2" align="left">
            <Highlighter highlightClassName="YourHighlightClass" searchWords={getHighlightWords(this.props.query, this.props.item.name)}
              // search terms to highlight
              autoEscape={true} textToHighlight={this.props.item.name}/>
          </Typography>
          <Typography component="p" variant='subtitle2' align="left" style={{
              marginBottom: '10px'
            }}>
            <strong>Description:
            </strong><Highlighter highlightClassName="YourHighlightClass" searchWords={getHighlightWords(this.props.query, this.props.item.description)}
      // search terms to highlight
      autoEscape={true} textToHighlight={getSnippets(this.props.query, this.props.item.description)}/>
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>)
  }

}
GroupSearchResultCard.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(GroupSearchResultCard);
