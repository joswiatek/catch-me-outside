import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Chip from '@material-ui/core/Chip';
import Typography from '@material-ui/core/Typography';
import {Link} from 'react-router-dom';

import GroupIcon from '@material-ui/icons/Group';

import API from '../../config/api.js';

const styles = {
  card: {
    maxWidth: '100%'
  },
  media: {
    height: 250
  },
  icon: {
    fontSize: '18px',
    verticalAlign: 'top',
    paddingTop: '1px',
    paddingRight: '5px',
    position: 'relative',
    top: '-1px',
    color: '#1BC86C'
  }
};

const MONTHS = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];

class GroupCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dateString: '',
      photo_url: 'placeholder',
      numEvents: 0
    };
  }

  componentDidMount() {
    var date_created = new Date();
    date_created.setTime(this.props.item.creation_time);
    var dateString = MONTHS[date_created.getMonth()] + " " + date_created.getFullYear();
    this.setState({dateString: dateString});

    // If there's no group photo, then use the space photo for their event
    if (this.props.item.group_photo === "") {
      API.getSpaceForGroup(this.props.item.group_id).then(json => {
        let photo_num = Math.floor(Math.random() * 10);
        let photo = JSON.parse(json.data.photos[photo_num]);
        let photo_url = API.buildSpaceImageURL(json.data.space_id, photo['photo_reference']);
        this.setState({photo_url: photo_url});
      });
    } else {
      // If there is a group photo then use it
      this.setState({photo_url: this.props.item.group_photo});
    }
    // Get the number of events for this group
    API.getEventsForGroup(this.props.item.group_id).then(json => {
      this.setState({numEvents: json.data.length});
    });
  }

  render() {
    return (<Card className={this.props.classes.card} id={this.props.id}>
      <CardActionArea component={Link} to={"/groups/" + this.props.item.group_id}>
        <CardMedia className={this.props.classes.media} image={this.state.photo_url} title=""/>
        <CardContent style={{
            maxHeight: 'auto',
            overflow: 'hidden'
          }}>
          <Typography id='groupCardName' gutterBottom="gutterBottom" variant="h5" component="h2" align="left">
            {this.props.item.name}
          </Typography>
          <Typography component="p" variant="subtitle2" align="left" style={{
              fontWeight: '400'
            }}>
            Active since {this.state.dateString}
          </Typography>
          <Typography component="p" variant="subtitle2" align="left" style={{
              fontWeight: '400'
            }}>
            <GroupIcon className={this.props.classes.icon}/>{this.props.item.member_count}
          </Typography>
          <Typography component="p" variant="caption" align="left" style={{
              marginTop: '10px'
            }}>
            Number of Events: {this.state.numEvents}
          </Typography>
          <Typography component="p" variant="caption" align="left">
            Group status: {this.props.item.status}
          </Typography>
          <Typography component="p" variant="caption" align="left">
            Join mode:
            <span style={{
                color: '#1BC86C'
              }}>{this.props.item.join_mode}</span>
          </Typography>
          <div style={{
              textAlign: 'left',
              marginTop: '10px'
            }}>
            <Chip className={this.props.classes.chip} label={this.props.item.categories} style={{
                color: 'white',
                backgroundColor: '#1BC86C'
              }}/>
          </div>
        </CardContent>
      </CardActionArea>
    </Card>)
  }
}

GroupCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(GroupCard);
