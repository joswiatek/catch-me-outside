import React, {Component} from 'react'

import Typography from '@material-ui/core/Typography';

import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';

const styles = {
  root: {
    padding: '2px 12px',
    display: 'flex',
    alignItems: 'center',
    width: 385,
    height: 50,
    backgroundColor: '#eee',
    borderRadius: 50
  },
  input: {
    marginLeft: 8,
    flex: 1
  },
  iconButton: {
    padding: 10
  },
  divider: {
    width: 1,
    height: 28,
    margin: 4
  }
};

class GroupSearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      //search query
      query: ''
    };
  }

  handleInputChange = event => {
    this.setState({query: event.target.value})
  }
  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.handleSearchClick();
    }
  }
  handleSearchClick = () => {
    this.props.returnSearch(this.state.query);
  }

  render() {
    return (<div style={styles.root} elevation={1}>
      <Typography variant='h6' align='left' style={{
          'paddingLeft' : '10px',
          'paddingBottom' : '0px',
          'paddingRight' : '12px'
        }}>Find</Typography>
      <InputBase id="groupSearchInput" style={styles.input} placeholder="groups, communities" onChange={this.handleInputChange} onKeyPress={this.handleKeyPress}/>
      <IconButton id="groupSearchButton" style={styles.iconButton} aria-label="Search" onClick={this.handleSearchClick}>
        <SearchIcon/>
      </IconButton>
    </div>)
  }
}

GroupSearchBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(GroupSearchBar);
