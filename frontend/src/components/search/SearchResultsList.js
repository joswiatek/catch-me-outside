import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import EventSearchResultCard from '../event/EventSearchResultCard.js';
import GroupSearchResultCard from '../group/GroupSearchResultCard.js';
import SpaceSearchResultCard from '../space/SpaceSearchResultCard.js';

const styles = {
  card: {
    maxWidth: '100%'
  },
  searchList: {
    'list-style-type': 'none',
    'padding-left': 0
  },
  searchResult: {
    'margin': '10px'
  }

};

const SearchResultsList = (props) => {

  switch (props.type) {
    case "events":
      return (<ul id={props.id} className={props.classes.searchList}>
        {props.data.map(item => <li className={props.classes.searchResult}><EventSearchResultCard id='eventResultCard' item={item} query={props.query} key={item.event_id}/></li>)}
      </ul>)
    case "groups":
      return (<ul id={props.id} className={props.classes.searchList}>
        {props.data.map(item => <li className={props.classes.searchResult}><GroupSearchResultCard id='groupResultCard' item={item} query={props.query} key={item.group_id}/></li>)}
      </ul>)
    case "spaces":
      return (<ul id={props.id} className={props.classes.searchList}>
        {props.data.map(item => <li className={props.classes.searchResult}><SpaceSearchResultCard id='spaceSearchCard' item={item} query={props.query} key={item.space_id}/></li>)}
      </ul>)
    default:
      console.error('Unknown search list type. Using default');
      return;
  }

}

SearchResultsList.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SearchResultsList);
