import React, {Component} from 'react'

import Typography from '@material-ui/core/Typography';

import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import {withRouter} from 'react-router-dom'

const styles = {
  root: {
    padding: '2px 12px',
    display: 'flex',
    alignItems: 'center',
    maxWidth: 400,
    minWidth: 100,
    height: 50,
    backgroundColor: 'rgb(255,255,255,0.8)',
    borderRadius: 50
  },
  input: {
    marginLeft: 8,
    flex: 1,
    color: '#000'
  },
  iconButton: {
    padding: 10
  },
  divider: {
    width: 1,
    height: 28,
    margin: 4
  }
};

class AllSearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      //search query
      query: this.props.query
    };
  }

  handleInputChange = event => {
    this.setState({query: event.target.value})
  }

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.handleSearchClick();
    }
  }

  handleSearchClick = () => {
    // If search was initiated from the splash page, then we need to render the search results page
    if (this.props.splashSearch) {
      this.props.history.push('/search', {initialValue: this.state.query});
    } else {
      // If we're already on the search results page then we just need to fetch new results and re-render
      this.props.returnSearch(this.state.query);
    }
  }

  render() {
    return (<Paper style={styles.root} elevation={1}>
      <Typography variant='h6' align='left' style={{
          'paddingLeft' : '10px',
          'paddingBottom' : '0px',
          'paddingRight' : '12px'
        }}>Find</Typography>
      <InputBase id="globalSearchBar" style={styles.input} value={this.state.query} placeholder='Search for an event, group, or space...' onChange={this.handleInputChange} onKeyPress={this.handleKeyPress}/>
      <IconButton id="globalSearchButton" style={styles.iconButton} aria-label="Search" onClick={this.handleSearchClick}>
        <SearchIcon/>
      </IconButton>
    </Paper>)
  }
}

AllSearchBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withRouter(withStyles(styles)(AllSearchBar));
