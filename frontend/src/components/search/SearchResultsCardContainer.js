import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import GroupCard from '../GroupCard.js';
import SpaceCard from '../SpaceCard.js';
import EventCard from '../EventCard.js';
import API from '../../config/api.js'

//<SearchResultsCardContainer name='eventGroupCard' type="group" uid={this.state.data.group_id}/>
//<GroupCard id='groupCard' item={item} key={item.group_id}/>

class SearchResultsCardContainer extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data: null,
      userLocation: {
        accuracy: 0,
        altitude: 0,
        altitudeAccuracy: 0,
        heading: 0,
        latitude: 30.2862725,
        longitude: -97.73667119999999,
        speed: 0
      }
    };
  }

  render() {
    let card;
    let cur_id;
    if (this.props.item.hasOwnProperty('group_id')) {
      cur_id = this.props.item.group_id
      card = <GroupCard id={cur_id} item={this.props.item} key={cur_id}/>
    } else if (this.props.item.hasOwnProperty('space_id')) {
      cur_id = this.props.item.space_id
      card = <SpaceCard id={cur_id} item={this.props.item} key={cur_id} userLocation={this.state.userLocation}/>
    } else if (this.props.item.hasOwnProperty('event_id')) {
      cur_id = this.props.item.event_id
      card = <EventCard id={cur_id} item={this.props.item} key={cur_id}/>
    }

    return (<Grid key={cur_id}>
      {card}
    </Grid>);
  }
}

export default SearchResultsCardContainer;
