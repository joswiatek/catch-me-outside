import React, {Component} from 'react';
import Typography from '@material-ui/core/Typography'
import moment, {ISO_8601} from 'moment';

class DateDisplay extends Component {

  render() {
    const date = moment(this.props.date, ISO_8601)
    return (<div>
      <Typography variant="h5" align="center" className="eventDateMonth" style={{
          paddingTop: '10px'
        }}>
        {date.format('MMM')}
      </Typography>
      <Typography variant="h4" align="center" style={{
          paddingBottom: '10px'
        }}>
        {date.format('DD')}
      </Typography>
    </div>);
  }
}

export default DateDisplay;
