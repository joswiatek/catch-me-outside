import React, {Component} from 'react'

import Typography from '@material-ui/core/Typography';

import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';

import Autosuggest from 'react-autosuggest';
import API from '../../config/api.js';
import {getSuggestions} from '../../config/Helper.js'

const styles = {
  root: {
    padding: '2px 12px',
    display: 'flex',
    alignItems: 'center',
    width: 750,
    height: 50,
    backgroundColor: '#eee',
    borderRadius: 50
  },
  input: {
    marginLeft: 8,
    flex: 1
  },
  iconButton: {
    padding: 10
  },
  divider: {
    width: 1,
    height: 28,
    margin: 4
  }
};
// When suggestion is clicked, Autosuggest needs to populate the input
// based on the clicked suggestion. Teach Autosuggest how to calculate the
// input value for every given suggestion.
const getSuggestionValue = suggestion => suggestion.name;

// Use your imagination to render suggestions.
const renderSuggestion = suggestion => (<div>
  {suggestion.name}
</div>);

class EventSearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      //search query
      query: this.props.query,
      // autosuggest locations
      value: this.props.value,
      filter_space_id: this.props.space_id,
      suggestions: []
    };
  }

  componentDidMount() {
    // Retrieve and save the space names
    API.getSpaceNames().then(json => {
      this.setState({space_names: json.data})
    })
  }

  onChange = (event, {newValue}) => {
    this.setState({value: newValue, filter_space_id: ""});
  };

  handleInputChange = event => {
    this.setState({query: event.target.value})
  }

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.handleSearchClick();
    }
  }

  handleSearchClick = () => {
    this.props.returnSearch(this.state.query, this.state.value, this.state.filter_space_id);
  }

  // Autosuggest will call this function every time you need to update suggestions.
  // You already implemented this logic above, so just use it.
  onSuggestionsFetchRequested = ({value}) => {
    this.setState({
      suggestions: getSuggestions(value, this.state.space_names)
    });
  };

  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested = () => {
    this.setState({suggestions: []});
  };

  onSuggestionSelected = (event, {suggestion, suggestionValue, suggestionIndex, sectionIndex, method}) => {
    this.setState({filter_space_id: suggestion.space_id})
  }

  render() {
    const {value, suggestions} = this.state;

    // Autosuggest will pass through all these props to the input.
    const inputProps = {
      placeholder: 'park, outdoor space',
      value,
      onChange: this.onChange
    };

    return (<div style={styles.root}>
      <Typography variant='h6' align='left' style={{
          'paddingLeft' : '10px',
          'paddingBottom' : '0px',
          'paddingRight' : '12px'
        }}>Find</Typography>
      <InputBase id="eventSearchInput" style={styles.input} value={this.state.query} placeholder="events, outdoor activities" onChange={this.handleInputChange} onKeyPress={this.handleKeyPress}/>
      <Divider style={styles.divider}/>
      <Typography variant='h6' align='left' style={{
          'paddingLeft' : '10px',
          'paddingBottom' : '0px'
        }}>near</Typography>
      <Autosuggest suggestions={suggestions} onSuggestionsFetchRequested={this.onSuggestionsFetchRequested} onSuggestionsClearRequested={this.onSuggestionsClearRequested} onSuggestionSelected={this.onSuggestionSelected} getSuggestionValue={getSuggestionValue} renderSuggestion={renderSuggestion} inputProps={inputProps} highlightFirstSuggestion={true}/>
      <IconButton id="eventSearchButton" style={styles.iconButton} aria-label="Search" onClick={this.handleSearchClick}>
        <SearchIcon/>
      </IconButton>
    </div>)
  }
}

EventSearchBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(EventSearchBar);
