import React from 'react';
import Typography from '@material-ui/core/Typography';
import DateDisplay from './DateDisplay.js'
import moment from 'moment';
import ScheduleIcon from '@material-ui/icons/Schedule';
import PlaceOutlinedIcon from '@material-ui/icons/PlaceOutlined';
// import GroupIcon from '@material-ui/icons/Group';
import PersonIcon from '@material-ui/icons/Person';
import LinkIcon from '@material-ui/icons/Link';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';

const styles = {
  icon: {
    fontSize: '20px',
    verticalAlign: 'top',
    paddingTop: '5px',
    paddingRight: '5px',
    position: 'relative',
    top: '-1px',
    color: '#1BC86C'
  },
  smallIcon: {
    fontSize: '14px',
    verticalAlign: 'top',
    paddingTop: '2px',
    paddingRight: '5px',
    paddingLeft: '2px',
    position: 'relative',
    top: '-1px',
    color: '#1BC86C'
  }
}

const EventSummary = (props) => {
  const {classes} = props;
  // for indexing into by date object
  const DAYS = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
  ];

  // date given
  var eventDate = new Date(props.data.local_date);

  // Convert military time to standard
  var timeString = moment(props.data.local_time, 'HH:mm').format('hh:mm a')
  // day + time for display
  var eventDateString = DAYS[(eventDate.getDay()+1) % DAYS.length] + " " + timeString;

  return (<div style={{
      height: '150px',
      width: '100%'
    }}>
    <div style={{
        float: 'left',
        height: '90%',
        width: '110px',
        paddingTop: '20px'
      }}>
      <DateDisplay date={props.data.local_date}/>
    </div>
    <div style={{
        float: 'right',
        height: '100%',
        width: 'calc(100% - 110px)'
      }}>
      <Typography id='eventSummaryName' variant="h5" className="instanceTitle">
        {props.data.name}
      </Typography>
      <Typography variant="subtitle1">
        <PlaceOutlinedIcon className={classes.icon}/> {props.venue}
      </Typography>
      <Typography variant="caption" style={{
          paddingLeft: '30px'
        }}>
        {props.address}
      </Typography>
      <Typography variant="subtitle1" style={{
          'color' : '#1BC86C'
        }}>
        <ScheduleIcon className={classes.icon}/> {eventDateString}
      </Typography>
      <Typography variant="subtitle1" style={{
          'color' : '#1BC86C'
        }}>
        <PersonIcon className={classes.icon}/> {props.data.rsvp_count} Attendees
      </Typography>
      <Typography variant="subtitle1" style={{
          'color' : '#1BC86C'
        }}>
        <LinkIcon className={classes.icon}/>
        <a style={{
            color: '#1BC86C',
            'textDecoration' : 'none'
          }} href={props.data.link}>Event Link
        </a>
      </Typography>
    </div>
  </div>)
}
EventSummary.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(EventSummary);
