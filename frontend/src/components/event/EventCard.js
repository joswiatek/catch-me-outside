import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import Grid from '@material-ui/core/Grid';
import { Link } from "react-router-dom";
import { calcDistance } from "../../config/Helper.js";
import DateDisplay from './DateDisplay.js'
import moment from 'moment';

//icons
import ScheduleIcon from '@material-ui/icons/Schedule';
import PlaceIcon from '@material-ui/icons/Place';
import GroupIcon from '@material-ui/icons/Group';
import PersonIcon from '@material-ui/icons/Person';
import NearMeIcon from '@material-ui/icons/NearMe';

import API from '../../config/api.js'

const styles = {
  card: {
    maxWidth: '100%',
  },
  media: {
    height: 200
  },
  chip:{
    margin: 2,
  },
  icon: {
    fontSize: '18px',
    verticalAlign: 'top',
    paddingBottom:'1px',
    paddingRight: '5px',
    position: 'relative',
    top: '-1px',
    color: '#1BC86C',
  },
  dateDisplay: {
    align:'left',
    backgroundColor:'rgba(255, 255, 255, 0.8)',
    width:'80px',
    'border-bottom-right-radius':'10px'
  },
  tag: {
    color:'white',
    backgroundColor:'#1BC86C',
    fontSize:'12px',
    'width':'auto'
  }

};

class EventCard extends Component{
  constructor(props){
    super(props);
    this.state = {
      spaceName: "",
      groupName: "",
      timeValue: "",
      groupPhoto: "",
      spacelat: 0.0,
      spacelng: 0.0,
      distanceAway: null,
    }
  }
  componentDidMount(){
    // Try to set the photo from the photo, but if it's not present then set a
    // flag to get the space photo
    let space_photo = true;
    if (this.props.item.photo !== "") {
      space_photo = false;
      this.setState({
        groupPhoto: this.props.item.photo
      });
    }

    // Get the associated group
    API.getGroup(this.props.item.group_id).then(json => {
      // Set the group name
      this.setState({
        groupName: json.data.name,
      })
    })

    // Get the associated space
    API.getSpace(this.props.item.space_id).then(json => {
      // Set the space name
      this.setState({
        spaceName: json.data.name,
        spacelat: json.data.lat,
        spacelng: json.data.long
      })
      // If we didn't have an event/group photo, get the space photo
      if (space_photo) {
        let photo_num = Math.floor(Math.random() * 10);
        let photo = JSON.parse(json.data.photos[photo_num]);
        let photo_url = API.buildSpaceImageURL(this.props.item.space_id, photo['photo_reference']);
        this.setState({
          groupPhoto: photo_url
        })
      }
      if(this.props.userlng !== 0 && this.props.userlat !== 0){
        this.setState({distanceAway: calcDistance(
          this.props.userlat,
          this.props.userlng,
          this.state.spacelat,
          this.state.spacelng).toFixed(2)});
      }
    })

    // Set the event time
    this.setState({
      timeValue: moment(this.props.item.local_time, 'HH:mm').format('hh:mm a')
    })


  };
  render (){
    return(
      <Card className={this.props.classes.card} id={this.props.id}>
        <CardActionArea component={Link} to={"/events/" + this.props.item.event_id}>
          <CardMedia
            className={this.props.classes.media}
            image={this.state.groupPhoto}
            title="">
            <div style={styles.dateDisplay}>
              <DateDisplay date={this.props.item.local_date}/>
            </div>
          </CardMedia>
          <CardContent style={{maxHeight: 'auto', overflow:'hidden'}}>
            <Typography id='eventCardName' gutterBottom variant="h5" component="h2" align="left">
              {this.props.item.name}
            </Typography>
            <Typography component="p" variant='caption' align="left">
              <ScheduleIcon className={this.props.classes.icon} style={{paddingTop:'-1px'}}/>{this.state.timeValue}
            </Typography>
            <Typography component="p" variant='caption' align="left">
               <PlaceIcon className={this.props.classes.icon}/>{this.state.spaceName}
            </Typography>
            {this.state.distanceAway != null &&
            <Typography component="p" variant='caption' align="left">
               <NearMeIcon className={this.props.classes.icon}/>{this.state.distanceAway} mi away
            </Typography>
            }
            <Typography component="p" variant='caption' align="left">
              <GroupIcon className={this.props.classes.icon}/>{this.state.groupName}
            </Typography>
            <Typography component="p" variant='caption' align="left">
              <PersonIcon className={this.props.classes.icon}/>{this.props.item.rsvp_count} Attendees
            </Typography>
            <br/>
            <Grid container spacing={0}>
            {this.props.item.description_tags.map(item =>
              <Chip
                className={this.props.classes.chip}
                label={item}
                style={styles.tag}
              />)}
            </Grid>
          </CardContent>
        </CardActionArea>
      </Card>
    )
  }

}
EventCard.propTypes = {
    classes: PropTypes.object.isRequired,
  };
export default withStyles(styles)(EventCard);
