import React, { Component } from 'react'
import PropTypes from "prop-types";

import Sort from '../Sort.js';
import Typography from '@material-ui/core/Typography';

import MaterialUIPickers from '../filter/MaterialUIPickers.js';
import MinMaxInput from '../filter/MinMaxInput.js';
import CheckboxLabels from '../filter/CheckboxLabels.js';

const eventTimes = [
  { label: 'Morning', value: 'checkedMorning'},
  { label: 'Afternoon', value: 'checkedAfternoon'},
  { label: 'Night', value: 'checkedNight'}
];

const choices = [
  { value: 'date', displayName: 'Date', defOrder: 'asc'},
  { value: 'name', displayName: 'A to Z', defOrder: 'asc'},
  { value: 'dist', displayName: 'Distance', defOrder: 'asc'},
  { value: 'rsvp_count', displayName: '# of attendees', defOrder: 'desc'},
];
const styles = {
  filterText: {
      'fontWeight': 600,
  },
  coloredText: {
      'fontWeight': 600,
      'color': '#1BC86C'
  }
};

class EventFilterSortBar extends Component {
  render() {
    return (
      <div className="filter-sort-container">
        <div className="filter-body">
          <Typography variant='subtitle1' align='left' style={styles.coloredText}>Filter by</Typography>
          <div className="filter-item">
            <Typography variant='subtitle2' align='left' style={styles.filterText}>Date Range</Typography>
            <MaterialUIPickers returnFilter={this.props.updateDateFilterCallback.bind(this)}/>
          </div>
          <div className="filter-item">
            <Typography variant='subtitle2' align='left' style={styles.filterText}>Event Time</Typography>
            <CheckboxLabels returnFilter={this.props.updateTimeFilterCallback} checklist={eventTimes}/>
          </div>
          <div className="filter-item">
            <Typography variant='subtitle2' align='left' style={styles.filterText}>Number of Attendees</Typography>
            <MinMaxInput returnFilter={this.props.updateAttendeeFilterCallback.bind(this)}/>
          </div>
        </div>
        <div className="sort-body">
          <Typography variant='subtitle1' align='left' style={styles.coloredText}>Sort by</Typography>
          <Sort options={choices} updateSortData={this.props.updateEventSortData.bind(this)} locEnabled={this.props.locEnabled}/>
        </div>
      </div>
    )
  }
}

EventFilterSortBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default EventFilterSortBar;
