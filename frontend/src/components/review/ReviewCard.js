import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardMedia from '@material-ui/core/CardMedia';

const styles = {
  card: {
    maxWidth: '100%'
  },
  media: {
    height: '250px'
  }
};

const ReviewCard = (props) => {

  let description = props.description

  // trim description if too long
  if (description.length > 170) {
    description = description.substr(0, 170)
    description = description.substr(0, description.lastIndexOf(".") + 1)
  }
  if (description.length === 0) {
    description = props.description.substr(0, 170)
    description = description.substr(0, description.lastIndexOf("!") + 1)
  }

  // name splitting
  let name_arr = props.name.split(' ')
  let name_result = name_arr[0]
  if (name_arr.length > 1) {
    name_result += " "
    name_result += name_arr[1].substr(0, 1)
    name_result += "."
  }

  return (<Card style={styles.media}>
    <CardMedia src='img' style={{
        'paddingTop' : '15px',
        'textAlign' : 'center'
      }}>
      <img src={props.photo} alt={props.alt_text} style={{
          'height' : '50px',
          'width' : 'auto'
        }}/>
    </CardMedia>
    <CardContent style={{
        maxHeight: 'auto',
        overflow: 'hidden'
      }}>
      <Typography gutterBottom="gutterBottom" variant='h6' component="h6" align='center' style={{
          'maxHeight' : '20px',
          'overflow' : 'hidden'
        }}>
        {name_result}
      </Typography>
      <Typography component="p" variant='caption' align="center" style={{
          'marginTop' : '-8px',
          'marginBottom' : '10px',
          'maxHeight' : '115px',
          'overflow' : 'hidden'
        }}>
        {description}
      </Typography>
    </CardContent>
  </Card>)
}

ReviewCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ReviewCard);
