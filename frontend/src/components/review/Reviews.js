import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import ReviewCard from './ReviewCard.js'

import API from '../../config/api.js'

class Reviews extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: []
    };
  }

  componentDidMount() {
    API.getReviews(this.props.id).then(json => {
      this.setState({data: json.data})
    })
  }

  render() {

    let review_data = this.state.data;

    // cut down to a random 3-length subarray
    if (review_data.length > 3) {
      let randIndex = Math.floor(Math.random() * (review_data.length - 3));
      review_data = review_data.slice(randIndex, randIndex + 3)
    }

    // map review items to cards
    const reviews = review_data.map(item => <Grid item="item" key={item.review_id} xs={4}>
      <ReviewCard name={item.name} photo={item.photo} alt_text={item.name} description={item.body}/>
    </Grid>);

    // render if we have any reviews to show
    if (review_data.length > 0) {
      return (<div >
        <Grid container="container" spacing={16}>
          {reviews}
        </Grid>
      </div>);
    } else {
      return (<div/>);
    }
  }
}

export default Reviews;
