import React from 'react';
//import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

// material-ui imports
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const styles = {
  card: {
    maxWidth: '100%'
  },
  media: {
    height: 250
  }
};

const SplashCard = (props) => {
  return (<Card className="splashCard" id={props.id}>
    <CardActionArea component={Link} to={{
        pathname: props.path
      }}>
      <CardMedia className="splashCardMedia" image={props.photo} title={props.name}></CardMedia>
      <CardContent style={{
          maxHeight: '125px',
          overflow: 'hidden'
        }}>
        <Typography gutterBottom="gutterBottom" variant="h5" component="h2">
          {props.name}
        </Typography>
      </CardContent>
    </CardActionArea>
  </Card>)
}

export default withStyles(styles)(SplashCard);
