import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import GroupCard from './group/GroupCard.js';
import SpaceCard from './space/SpaceCard.js';
import EventCard from './event/EventCard.js';
import API from '../config/api.js'

class CardContainer extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data: null,
      userLocation: {
        accuracy: 0,
        altitude: 0,
        altitudeAccuracy: 0,
        heading: 0,
        latitude: 0.0,
        longitude: 0.0,
        speed: 0
      }
    };
  }

  // will make api call
  componentDidMount() {
    switch (this.props.type) {
      case "event":
        API.getEvent(this.props.uid).then(json => {
          this.setState({data: json.data})
        })
        break;
      case "group":
        API.getGroup(this.props.uid).then(json => {
          this.setState({data: json.data})
        })
        break;
      case "space":
        API.getSpace(this.props.uid).then(json => {
          this.setState({data: json.data})
        })
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(position => {
            this.setState({userLocation: position.coords})
          });
        } else {
          console.error("Geolocation is not supported by this browser.");
        }
        break;
      default:
        break;
    }
  }

  render() {
    let card;

    // if our data has loaded
    if (this.state.data != null) {
      switch (this.props.type) {
        case "event":
          card = <EventCard id={this.props.name} item={this.state.data} key={this.props.id} userLat={this.state.userLocation.latitude} userLng={this.state.userLocation.longitude}/>
          break;
        case "group":
          card = <GroupCard id={this.props.name} item={this.state.data} key={this.props.id}/>
          break;
        case "space":
          card = <SpaceCard id={this.props.name} item={this.state.data} key={this.props.id} userLat={this.state.userLocation.latitude} userLng={this.state.userLocation.longitude}/>
          break;
        default:
          break;
      }
    }

    return (<Grid key={this.props.id}>
      {card}
    </Grid>);
  }
}

export default CardContainer;
