import React, {Component} from 'react'

import Typography from '@material-ui/core/Typography';

import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';

import Autosuggest from 'react-autosuggest';
import API from '../../config/api.js';
import {getSuggestions} from '../../config/Helper.js'

const styles = {
  root: {
    padding: '2px 12px',
    display: 'flex',
    alignItems: 'center',
    width: 325,
    height: 50,
    backgroundColor: '#eee',
    borderRadius: 50
  },
  input: {
    marginLeft: 8,
    flex: 1
  },
  iconButton: {
    padding: 10
  },
  divider: {
    width: 1,
    height: 28,
    margin: 4
  }
};

// When suggestion is clicked, Autosuggest needs to populate the input
// based on the clicked suggestion. Teach Autosuggest how to calculate the
// input value for every given suggestion.
const getSuggestionValue = suggestion => suggestion.name;

// Use your imagination to render suggestions.
const renderSuggestion = suggestion => (<div>
  {suggestion.name}
</div>);

class SpaceSearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      //search query
      query: '',
      // autosuggest locations
      value: '',
      suggestions: []
    };
  }

  componentDidMount() {
    // Retrieve and save the space names
    API.getSpaceNames().then(json => {
      this.setState({space_names: json.data})
    })
  }

  onChange = (event, {newValue}) => {
    this.setState({value: newValue, query: newValue});
  };

  handleInputChange = () => {
    this.setState({query: this.search.value})
  }
  handleSearchClick = () => {
    this.props.returnSearch(this.state.value);
  }

  handleSelection = (val) => {
    this.setState({query: val})
    this.props.returnSearch(val);
  }

  // Autosuggest will call this function every time you need to update suggestions.
  // You already implemented this logic above, so just use it.
  onSuggestionsFetchRequested = ({value}) => {
    this.setState({
      suggestions: getSuggestions(value, this.state.space_names)
    });
  };

  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested = () => {
    this.setState({suggestions: []});
  };

  render() {
    const {value, suggestions} = this.state;

    // Autosuggest will pass through all these props to the input.
    const inputProps = {
      placeholder: 'parks, outdoor spaces',
      value,
      onChange: this.onChange,
      id: "spaceSearchInput"
    };

    return (<div style={styles.root} elevation={1}>
      <Typography variant='h6' align='left' style={{
          'paddingLeft' : '10px',
          'paddingBottom' : '0px'
        }}>Find</Typography>
      <Autosuggest suggestions={suggestions} onSuggestionsFetchRequested={this.onSuggestionsFetchRequested} onSuggestionsClearRequested={this.onSuggestionsClearRequested} getSuggestionValue={getSuggestionValue} renderSuggestion={renderSuggestion} inputProps={inputProps} highlightFirstSuggestion={true} onSuggestionSelected={(event, vals) => {
          this.handleSelection(vals.suggestionValue)
        }}/>
      <IconButton id="spaceSearchButton" style={styles.iconButton} aria-label="Search" onClick={this.handleSearchClick}>
        <SearchIcon/>
      </IconButton>
    </div>)
  }
}

SpaceSearchBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SpaceSearchBar);
