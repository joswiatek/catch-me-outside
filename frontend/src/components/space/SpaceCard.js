import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

// material-ui imports
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import WeatherDisplay from './WeatherDisplay.js';

//icons
import ScheduleIcon from '@material-ui/icons/Schedule';
import PlaceOutlinedIcon from '@material-ui/icons/PlaceOutlined';
import NearMeOutlinedIcon from '@material-ui/icons/NearMeOutlined';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import LocalActivityOutlinedIcon from '@material-ui/icons/LocalActivityOutlined';

import API from '../../config/api.js';
import {calcDistance} from '../../config/Helper.js';

const styles = {
  card: {
    maxWidth: '100%'
  },
  media: {
    height: 250
  },
  icon: {
    fontSize: '18px',
    verticalAlign: 'top',
    paddingBottom: '1px',
    paddingRight: '5px',
    position: 'relative',
    top: '-1px',
    color: '#1BC86C'
  }
};

class SpaceCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      photo_url: 'placeholder',
      open_hours: '',
      average_rating: 0,
      distanceAway: 0,
      numEvents: 0
    }
  }

  componentDidMount() {
    // Get number of events for space
    API.getEventsForSpace(this.props.item.space_id).then(json => {
      // Set the group name
      this.setState({numEvents: json.data.length})
    })
    // Build photo URl
    let photo_num = Math.floor(Math.random() * 10);
    let photo = JSON.parse(this.props.item.photos[photo_num]);
    let photo_url = API.buildSpaceImageURL(this.props.item.space_id, photo['photo_reference'])
    this.setState({photo_url: photo_url});

    // Extract the open hours
    let day_of_week = (new Date().getDay() - 1) % 7;
    if (day_of_week < 0)
      day_of_week += 7;
    if (this.props.item.open_hours.length > 0) {
      let open_hours = this.props.item.open_hours[day_of_week].split(':');
      open_hours.shift()
      open_hours = open_hours.join(':')
      open_hours = open_hours.trim();
      this.setState({open_hours: open_hours});
    } else {
      this.setState({open_hours: '-'});
    }
    this.setState({
      distanceAway: calcDistance(this.props.userLat, this.props.userLng, this.props.item.lat, this.props.item.long)
    });
  };

  render() {
    return (<Card id={this.props.id} className={this.props.classes.card}>
      <CardActionArea component={Link} to={'/spaces/' + this.props.item.space_id}>
        <CardMedia className={this.props.classes.media} image={this.state.photo_url} title={this.props.item.name}>
          <div style={{
              align: 'left',
              backgroundColor: 'rgba(255, 255, 255, 0.8)',
              width: '80px',
              height: '80px',
              borderBottomRightRadius: '10px',
              textAlign: 'center'
            }}>
            <WeatherDisplay spaceId={this.props.item.space_id} fullDisplay={true}/>
          </div>
        </CardMedia>
        <CardContent style={{
            maxHeight: 'auto',
            overflow: 'hidden'
          }}>
          <Typography id='spaceCardName' gutterBottom="gutterBottom" variant='h5' component='h2' align="left">
            {this.props.item.name}
          </Typography>
          <Typography component="p" variant="caption" align="left" style={{
              fontWeight: '400'
            }}>
            <PlaceOutlinedIcon className={this.props.classes.icon}/>{this.props.item.address}
          </Typography>
          <Typography component="p" variant="caption" align="left" style={{
              fontWeight: '400'
            }}>
            <ScheduleIcon className={this.props.classes.icon}/>{this.state.open_hours}
          </Typography>
          <Typography component="p" variant="caption" align="left" style={{
              fontWeight: '400'
            }}>
            <StarBorderIcon className={this.props.classes.icon}/>{(this.props.item.rating).toFixed(1)} rating
          </Typography>
          {
            this.props.userLat !== 0 && this.props.userLng !== 0 && <Typography component="p" variant="caption" align="left" style={{
                  fontWeight: '400'
                }}>
                <NearMeOutlinedIcon className={this.props.classes.icon}/>{(this.state.distanceAway).toFixed(2)} mi away
              </Typography>
          }
          <Typography component="p" variant="caption" align="left" style={{
              fontWeight: '400'
            }}>
            <LocalActivityOutlinedIcon className={this.props.classes.icon}/>{this.state.numEvents} events
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>)
  }
}

SpaceCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SpaceCard);
