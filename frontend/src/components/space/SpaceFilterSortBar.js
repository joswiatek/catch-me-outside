import React, {Component} from 'react'
import PropTypes from "prop-types";

import Sort from '../Sort.js';

import Typography from '@material-ui/core/Typography';
import OpenNowButton from '../filter/OpenNowButton.js';
import SingleSelectDropdown from '../filter/SingleSelectDropdown.js';

const ratings = [
  {
    name: '1-star & up',
    value: '1.0'
  }, {
    name: '1.5-star & up',
    value: '1.5'
  }, {
    name: '2-stars & up',
    value: '2.0'
  }, {
    name: '2.5-stars & up',
    value: '2.5'
  }, {
    name: '3-stars & up',
    value: '3.0'
  }, {
    name: '3.5-stars & up',
    value: '3.5'
  }, {
    name: '4-stars & up',
    value: '4.0'
  }, {
    name: '4.5-stars & up',
    value: '4.5'
  }, {
    name: '5-stars',
    value: '5.0'
  }
];

const activity_lvl = [
  {
    name: 'Low Activity',
    value: [1, 7]
  }, {
    name: 'Medium Activity',
    value: [11, 20]
  }, {
    name: 'High Activity',
    value: [30, 1000]
  }
];

const choices = [
  {
    value: 'rating',
    displayName: 'Popularity',
    defOrder: 'desc'
  }, {
    value: 'name',
    displayName: 'A to Z',
    defOrder: 'asc'
  }, {
    value: 'dist',
    displayName: 'Distance',
    defOrder: 'asc'
  }, {
    value: 'event_count',
    displayName: '# of events',
    defOrder: 'desc'
  }
];

class SpaceFilterSortBar extends Component {
  render() {
    return (<div className="filter-sort-container">
      <div className="filter-body">
        <Typography variant='subtitle1' align='left' style={{
            'fontWeight' : '600',
            'color' : '#1BC86C'
          }}>Filter by</Typography>
        <div className="filter-item">
          <SingleSelectDropdown list={ratings} name='Ratings' updateSelection={this.props.updateRating.bind(this)}/>
        </div>
        <div className="filter-item">
          <SingleSelectDropdown list={activity_lvl} name='Activity Level' updateSelection={this.props.updateActivity.bind(this)}/>
        </div>
        <div className="filter-item">
          <OpenNowButton updateOpenNow={this.props.updateOpenNow.bind(this)}/>
        </div>
      </div>
      <div className="sort-body">
        <Typography variant='subtitle1' align='left' style={{
            'fontWeight' : '600',
            'color' : '#1BC86C'
          }}>Sort by</Typography>
        <Sort options={choices} updateSortData={this.props.updateSpaceSortData.bind(this)} locEnabled={this.props.locEnabled}/>
      </div>
    </div>)
  }
}

SpaceFilterSortBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default SpaceFilterSortBar;
