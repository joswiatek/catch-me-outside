import React, {Component} from 'react';
import WeatherIcon from 'react-icons-weather';
import API from '../../config/api.js'
import Typography from '@material-ui/core/Typography'

class WeatherDisplay extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data: [
        {}
      ],
      high: '-',
      low: '-',
      icon: '200'
    };
  }

  componentDidMount() {
    API.getWeather(this.props.spaceId).then(json => {
      this.setState({
        data: json.data, high: json.data[0].high.split('.')[0],
        low: json.data[0].low.split('.')[0],
        icon: json.data[0].icon_id
      })
    })
  }
  render() {
    return (<div >
      <WeatherIcon name="owm" iconId={this.state.icon} flip="horizontal" rotate="90" style={{
          marginTop: '22px'
        }}/> {
        this.props.fullDisplay
          ? <Typography component="p" variant="subtitle2" style={{
                fontWeight: '400'
              }}>
              {this.state.high}° {this.state.low}°
            </Typography>
          : <div/>
      }
    </div>);
  }
}

export default WeatherDisplay;
