import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import {Link} from "react-router-dom";
import Highlighter from "react-highlight-words";
import PlaceOutlinedIcon from '@material-ui/icons/PlaceOutlined';
import API from '../../config/api.js';

const styles = {
  card: {
    maxWidth: '100%',
    display: 'flex',
    flexDirection: 'row',
    height: 100
  },
  media: {
    width: 150,
    height: 100,
    float: 'left',
    marginBottom: '5px',
    marginRight: '15px'
  },
  mediaColumn: {
    height: '100%',
    float: 'left'
  },
  content: {
    flex: '1 0 auto',
    height: '200px'
  },
  icon: {
    fontSize: '18px',
    verticalAlign: 'top',
    paddingBottom: '1px',
    paddingRight: '5px',
    position: 'relative',
    top: '-1px',
    color: '#1BC86C'
  }
};

class SpaceSearchResultCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      photo: ""
    }
  }
  componentDidMount() {
    let photo_num = Math.floor(Math.random() * 10);
    let photo = JSON.parse(this.props.item.photos[photo_num]);
    let photo_url = API.buildSpaceImageURL(this.props.item.space_id, photo['photo_reference']);
    this.setState({photo: photo_url});
  };
  render() {
    return (<Card className={this.props.classes.card} id={this.props.id}>
      <CardActionArea component={Link} to={"/spaces/" + this.props.item.space_id}>
        <CardMedia className={this.props.classes.media} image={this.state.photo} title=""></CardMedia>
        <CardContent className={this.props.classes.content} overflow='hidden'>
          <Typography id='SearchResultCardName' gutterBottom="gutterBottom" variant="h5" component="h2" align="left">
            <Highlighter highlightClassName="YourHighlightClass" searchWords={this.props.query}
              // search terms to highlight
              autoEscape={true} textToHighlight={this.props.item.name}/>
          </Typography>
          <Typography component="p" variant='subtitle2' align="left" style={{
              marginBottom: '10px'
            }}>
            <PlaceOutlinedIcon className={this.props.classes.icon}/>
            <Highlighter highlightClassName="YourHighlightClass" searchWords={this.props.query}
              // search terms to highlight
              autoEscape={true} textToHighlight={this.props.item.address}/>
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>)
  }

}
SpaceSearchResultCard.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(SpaceSearchResultCard);
