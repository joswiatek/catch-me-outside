import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';

// material-ui imports
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab'

import logo from '../assets/splash-images/cmo-logo-ko.png'; // Tell Webpack this JS file uses this image

console.log(logo)

const styles = {
  textColorSecondary: {
    color: 'white'
  }
};

class Navbar extends Component {

  constructor(props) {
    super(props);

    var initialTabValue = null;
    let pathname = props.location.pathname

    if (pathname.startsWith("/events")) {
      initialTabValue = 0;
    } else if (pathname.startsWith("/events")) {
      initialTabValue = 1;
    } else if (pathname.startsWith("/groups")) {
      initialTabValue = 2;
    } else if (pathname.startsWith("/spaces")) {
      initialTabValue = 3;
    } else if (pathname.startsWith("/about")) {
      initialTabValue = 4;
    } else {
      initialTabValue = null;
    }

    this.state = {
      value: initialTabValue
    };
  }

  handleChange = (event, value) => {
    this.setState({value});
  };

  handleHomeClick = () => {
    this.setState({value: null})
  }

  render() {
    return (<div>
      <AppBar position='sticky' style={{
          background: '#1BC86C',
          position: "fixed"
        }}>
        <Toolbar>
          <Grid justify="space-between" container="container" spacing={16} style={{
              alignItems: 'center'
            }}>
            <Grid id="homeLink" item="item" onClick={this.handleHomeClick} component={Link} to="/" >
              <img className="logo" src={logo} alt="Logo" />
            </Grid>
            <Grid item="item">
              <Tabs value={this.state.value} onChange={this.handleChange} textColor="secondary">
                <Tab id='visualNavButton' component={Link} to="/visuals" label="Visuals" style={styles.textColorSecondary}/>
                <Tab id='eventNavButton' component={Link} to="/events" label="Events" style={styles.textColorSecondary}/>
                <Tab id='groupNavButton' component={Link} to="/groups" label="Groups" style={styles.textColorSecondary}/>
                <Tab id='spaceNavButton' component={Link} to="/spaces" label="Spaces" style={styles.textColorSecondary}/>
                <Tab id='aboutNavButton' component={Link} to="/about" label="About" style={styles.textColorSecondary}/>
              </Tabs>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </div>)
  }
}

export default withRouter(Navbar);
