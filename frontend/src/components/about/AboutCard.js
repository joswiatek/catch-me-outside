import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const styles = {
  card: {
    maxWidth: '100%'
  },
  media: {
    height: 600,
    width: 300
  },
  image: {
    height: '300px',
    align:'center'
  },
  textSpacing: {
    marginBottom: '10px',
    align: 'center'
  }
};

const AboutCard = (props) => {
  return (<Card style={styles.media}>
    <CardMedia style={styles.image} image={props.photo} title=""/>
    <CardContent>
      <Typography gutterBottom="gutterBottom" variant='h6' component="h4" style={styles.textSpacing}>
        <strong>{props.name}</strong>
      </Typography>
      <Typography component="p" align="left">
        <strong>Bio:
        </strong>{props.bio}
      </Typography>
      <Typography component="p" align="left" style={styles.textSpacing}>
        <strong>Role:
        </strong>{props.role}
      </Typography>
      <Typography component="p" variant="caption" align="left">
        <strong>Commits:
        </strong>{props.commits}
      </Typography>
      <Typography component="p" variant="caption" align="left">
        <strong>Issues:
        </strong>{props.issues}
      </Typography>
      <Typography component="p" variant="caption" align="left">
        <strong>Unit Tests:
        </strong>{props.unitTests}
      </Typography>
    </CardContent>
  </Card>)
}
AboutCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AboutCard);
