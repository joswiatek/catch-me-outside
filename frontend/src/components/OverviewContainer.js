import React, {Component} from 'react';
import ReactPaginate from 'react-paginate';

class OverviewContainer extends Component {
  componentDidMount() {
    this.props.updatePageCallback(1);
  }

  handlePageClick = data => {
    let selected = data.selected;
    this.props.updatePageCallback(selected + 1);
    window.scrollTo(0, 0);
  };

  render() {
    return (<div>
      {this.props.children}
      <div className='tableDiv'>
        <ReactPaginate previousLabel={'previous'} nextLabel={'next'} breakLabel={'...'} breakClassName={'break-me'} forcePage={this.props.page} pageCount={parseInt(this.props.pageCount)} marginPagesDisplayed={1} pageRangeDisplayed={2} onPageChange={this.handlePageClick} containerClassName={'pagination'} pageClassName={'paginationPage'} activeClassName={'paginationActive'} pageLinkClassName={'paginationA'}/>
      </div>
    </div>);
  }
}

export default OverviewContainer;
