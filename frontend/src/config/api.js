import axios from 'axios'

const apiBase = 'https://api.catchmeoutside.today'
const spaceImageBaseUrl = 'https://s3.amazonaws.com/catch-me-outside-assets/google-places-photos';
const cusomterApiBase = "https://api.carefortheair.me"

class API {
  static async getEvents(params) {
    return axios.get(apiBase + '/events', {params: params})
  }

  static async getEvent(id) {
    return axios.get(apiBase + '/events/' + id)
  }

  static async getEventsForGroup(id) {
    return axios.get(apiBase + '/events/group/' + id)
  }

  static async getEventsForSpace(id) {
    return axios.get(apiBase + '/events/space/' + id)
  }

  static async getGroups(params) {
    return axios.get(apiBase + '/groups', {params: params});
  }

  static async getGroup(id) {
    return axios.get(apiBase + '/groups/' + id)
  }

  static async getSpaces(params) {
    return axios.get(apiBase + '/spaces', {params: params});
  }

  static async getSpace(id) {
    return axios.get(apiBase + '/spaces/' + id);
  }

  static async getSpaceForGroup(id) {
    return axios.get(apiBase + '/spaces/group/' + id)
  }

  static async getSpaceNames() {
    return axios.get(apiBase + '/spaces/names');
  }

  static async getGroupCategories() {
    return axios.get(apiBase + '/groups/categories')
  }

  static async getWeather(id) {
    return axios.get(apiBase + '/weather/' + id)
  }

  static async getReviews(id) {
    return axios.get(apiBase + '/reviews/' + id)
  }

  static buildSpaceImageURL(id, photoref) {
    return spaceImageBaseUrl + '/' + id + '/' + photoref
  }

  static async getCustomerData() {
    return axios.get(cusomterApiBase + "/all")
  }

}

export default API;
