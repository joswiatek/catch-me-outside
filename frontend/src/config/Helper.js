import * as Fuse from 'fuse.js';
import stemmer from 'stemmer';

const updateMap = (map1, map2) => {
  for (var i in map2) {
    map1[i] = map2[i];
  }
}

const getSuggestions = (value, list) => {
  var options = {
    shouldSort: true,
    threshold: 0.6,
    location: 0,
    distance: 100,
    maxPatternLength: 32,
    minMatchCharLength: 1,
    keys: ["name"]
  };
  var fuse = new Fuse(list, options); // "list" is the item array
  var result = fuse.search(value);
  return result
}

const getHighlightWords = (queryList, text) => {
  queryList = queryList.map(x => stemmer(x))
  /* eslint-disable no-useless-escape */
  text = text.replace(/(~|`|!|@|#|$|%|^|&|\*|\(|\)|{|}|\[|\]|;|:|\"|'|<|,|\.|>|\?|\/|\\|\||-|_|\+|=)/g, "")
  text = text.replace(/(\r\n|\n|\r)/g, " ")
  /* eslint-enable no-useless-escape */

  text = text.split(" ")
  let retList = []
  text.forEach(function(word) {
    if (queryList.includes(stemmer(word))) {
      retList.push(word)
    }
  })
  return retList
}

const getSnippets = (queryList, text) => {
  queryList = getHighlightWords(queryList, text)
  text = text.split(/[.!?]+/)
  let retList = []
  for (var sentence of text) {
    for (var word of queryList) {
      if (sentence.indexOf(word) !== -1) {
        retList.push(sentence)
        queryList.splice(queryList.indexOf(word), 1)
        break;
      }
    }
    if (retList.length > 2) {
      break
    }
  }
  if (retList.length === 0) {
    retList.push(text[0])
    retList.push(text[text.length - 2])
  }
  return retList.join("...")
}
const calcDistance = (lat1, lon1, lat2, lon2) => {
  if ((lat1 === lat2) && (lon1 === lon2)) {
    return 0;
  } else {
    var radlat1 = Math.PI * lat1 / 180;
    var radlat2 = Math.PI * lat2 / 180;
    var theta = lon1 - lon2;
    var radtheta = Math.PI * theta / 180;
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1) {
      dist = 1;
    }
    dist = Math.acos(dist);
    dist = dist * 180 / Math.PI;
    dist = dist * 60 * 1.1515;
    return dist;
  }
}
export {
  updateMap,
  getSuggestions,
  getHighlightWords,
  getSnippets,
  calcDistance
};
