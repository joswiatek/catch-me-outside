import React, {Component} from 'react';
import SplashCard from '../components/SplashCard.js';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import lottie from "lottie-web";

import SpaceImage from '../assets/splash-images/spacescmo.png';
import GroupImage from '../assets/splash-images/groupcmo.png';
import EventImage from '../assets/splash-images/event.png';
import AllSearchBar from '../components/search/AllSearchBar.js'
import kayakAnimation from '../assets/splash-images/kayak.json'

const styles = {
  heroImage: {
    objectFit: 'cover',
    width: '100%',
    height: '100%',
    zIndex: 0,
    position: 'relative'
  }
};

class Splash extends Component {

    state = {
      retrieved: [],
      scrollHeight: window.pageYOffset,
      absoluteScrollHeight: window.pageYOffset,
      animationInst: null,
    }

    componentDidMount() {
      window.addEventListener('scroll', this.listenToScroll)
      let ani = lottie.loadAnimation({
        container: document.getElementById("animation"),
        loop: true,
        autoplay: false,
        animationData: kayakAnimation,
        rendererSettings: {
          preserveAspectRatio: 'xMidYMid slice'
        }
      })
      this.setState({animationInst: ani})
    }

    componentWillUnmount() {
      window.removeEventListener('scroll', this.listenToScroll)
    }

    listenToScroll = () => {
      const winScroll =
        document.body.scrollTop || document.documentElement.scrollTop

      const height =
        document.documentElement.scrollHeight -
        document.documentElement.clientHeight

      const scrolled = winScroll / height
      const frameNum = Math.trunc((90 * scrolled)%35)
      console.log(frameNum)
      this.state.animationInst.goToAndStop(frameNum, true)
      this.setState({
        scrollHeight: scrolled,
        absoluteScrollHeight: winScroll,
      })
    }

    render() {
      return (
        <div>
          <div className='heroImage'>
            <div style={styles.heroImage}>
              <div id="animation" style = {{position: 'absolute', bottom: -250, right: -250, transform: `rotate(-45deg) scale(0.6) translate(${-300 * this.state.scrollHeight}px, ${-1500 * this.state.scrollHeight}px)`}} />
              <img
                src={require('../assets/splash-images/duck.png')}
                style = {{
                  position: 'absolute',
                  bottom: -350, right: 750,
                  transform: `scale(0.1) translate(
                    ${500 * this.state.scrollHeight + 50*Math.sin(this.state.absoluteScrollHeight/90)}px,
                    ${-500 * this.state.scrollHeight + 50*Math.sin(this.state.absoluteScrollHeight/90)}px)
                    rotate(${-10*Math.sin(this.state.absoluteScrollHeight/90)}deg)`}}
                  alt='duck'/>
              <img
                src={require('../assets/splash-images/duck.png')}
                style = {{
                  position: 'absolute',
                  bottom: -400, right: 795,
                  transform: `scale(0.05) translate(
                    ${850 * this.state.scrollHeight + 150*Math.sin(80+this.state.absoluteScrollHeight/90)}px,
                    ${-850 * this.state.scrollHeight + 150*Math.sin(80+this.state.absoluteScrollHeight/90)}px)
                    rotate(${-10*Math.sin(80+this.state.absoluteScrollHeight/90)}deg)`}}
                  alt='duck'/>
              <img
                src={require('../assets/splash-images/duck.png')}
                style = {{
                  position: 'absolute',
                  bottom: -410, right: 850,
                  transform: `scale(0.05) translate(
                    ${650 * this.state.scrollHeight + 200*Math.sin(200+this.state.absoluteScrollHeight/90)}px,
                    ${-650 * this.state.scrollHeight + 200*Math.sin(200+this.state.absoluteScrollHeight/90)}px)
                    rotate(${-10*Math.sin(200+this.state.absoluteScrollHeight/90)}deg)`}}
                  alt='duck'/>
              <img
                src={require('../assets/splash-images/duck.png')}
                style = {{
                  position: 'absolute',
                  bottom: -360, right: 840,
                  transform: `scale(0.05) translate(
                    ${730 * this.state.scrollHeight + 150*Math.sin(250+this.state.absoluteScrollHeight/90)}px,
                    ${-730 * this.state.scrollHeight + 150*Math.sin(250+this.state.absoluteScrollHeight/90)}px)
                    rotate(${-10*Math.sin(250+this.state.absoluteScrollHeight/90)}deg)`}}
                  alt='duck'/>
            </div>
            <div className='heroText'>
              <Typography variant="h2" style={{paddingBottom:'20px', color: '#1BC86C'}}>Discover Austin</Typography>
              <Typography variant="h2" style={{'fontSize':'20px','paddingTop':'0','fontWeight':'400', 'color':'black'}} >Find amazing spaces, events, and communities in Austin!</Typography>
              <AllSearchBar splashSearch={true}/>
            </div>
          </div>
          <div className="page-body">
            <Typography variant="h5" align="left" style={{'marginBottom':'30px'}}>What are you looking for today?</Typography>
            <Grid container spacing={16}>
              <Grid item xs>
                <SplashCard
                  id='spaceSplashCard'
                  name='Spaces'
                  path='/spaces'
                  photo={SpaceImage}>
                </SplashCard>
              </Grid>
              <Grid item xs>
                <SplashCard
                  id='groupSplashCard'
                  name='Groups'
                  path='/groups'
                  photo={GroupImage}>
                </SplashCard>
              </Grid>
              <Grid item xs>
                <SplashCard
                  id='eventSplashCard'
                  name='Events'
                  path='/events'
                  photo={EventImage}>
                </SplashCard>
              </Grid>
            </Grid>
          </div>
          { this.state.retrieved }
        </div>)
      }
}

export default Splash;
