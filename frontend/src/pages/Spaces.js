import React, {Component} from 'react';
import SpaceCard from '../components/space/SpaceCard.js';
import SearchResultsList from '../components/search/SearchResultsList.js';
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid';
import OverviewContainer from '../components/OverviewContainer.js';
import SpaceSearchBar from '../components/space/SpaceSearchBar.js';
import SpaceFilterSortBar from '../components/space/SpaceFilterSortBar.js';
import API from '../config/api.js'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import {geolocated} from 'react-geolocated';
import moment from 'moment';

class Spaces extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      pageCount: 0,
      userLat: 0,
      userLng: 0,
      searchParams: {},
      filterParams: {},
      sortParams: {},
      locationParams: {},
      pageParams: {},
      currQuery: []
    };
  }

  // check for user location and set in state if available
  componentDidMount() {
    if (this.props.userLat !== 0 && this.props.userLng !== 0) {
      this.setState({userLat: this.props.userLat, userLng: this.props.userLng})
    }
  }

  // runs when async call to get location is reuturned
  componentWillReceiveProps = (nextProps) => {
    if (this.state.userLng === 0 || this.state.userLat === 0) {
      if (nextProps.hasOwnProperty('coords') && nextProps.coords !== null) {
        if (nextProps.coords.latitude !== null && nextProps.coords.longitude !== null) {
          this.setState({userLat: nextProps.coords.latitude, userLng: nextProps.coords.longitude})
        }
      } else if (nextProps.hasOwnProperty('userLat') && nextProps.hasOwnProperty('userLng')) {
        if (nextProps.userLat !== 0 && nextProps.userLng !== 0) {
          this.setState({userLat: nextProps.userLat, userLng: nextProps.userLng})
        }
      }
    }
  }

  updateSpaceSearchData = (query, space_name, location_id) => {
    // Reset to page 1
    let newSearchParams = {}
    if (query !== "") {
      newSearchParams.keyword = query;
      // Update the current query
      this.setState({currQuery: query.split(" ")});
    } else {
      this.setState({currQuery: []});
    }

    let newLocationParams = {};
    if (location_id !== "") {
      newLocationParams.space_id = location_id;
      newLocationParams.space_name = space_name;
    }

    // Update the state's search and location params
    this.setState({searchParams: newSearchParams});
    this.setState({locationParams: newLocationParams});
    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});

    // Add in the filter and sort params
    let queryParams = {
      ...newSearchParams,
      ...newLocationParams,
      ...newPageParams,
      ...this.state.filterParams,
      ...this.state.sortParams
    };

    // Load the new data
    this.loadDataFromServer(queryParams);
  }

  updateSpaceSortData = (sort, order) => {
    let newSortParams = {
      sort: sort,
      order: order
    }

    // TODO: what to do if the user lat/long is not set?
    if (this.state.userLat !== 0 && this.state.userLng !== 0) {
      let userLocStr = String(this.state.userLat) + "," + String(this.state.userLng)
      newSortParams['user_loc'] = userLocStr
    }

    // Update the state's sort params
    this.setState({sortParams: newSortParams});
    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});

    // Add in the search and filter params
    let queryParams = {
      ...newSortParams,
      ...this.state.searchParams,
      ...this.state.locationParams,
      ...this.state.filterParams,
      ...newPageParams
    };

    // Load the new data
    this.loadDataFromServer(queryParams);
  }

  updatePageData = pageNum => {
    let newPageParams = {};

    newPageParams.page = pageNum;

    // Update the state's filter params
    this.setState({pageParams: newPageParams});

    // Add in the other parameters
    let queryParams = {
      ...newPageParams,
      ...this.state.sortParams,
      ...this.state.searchParams,
      ...this.state.locationParams,
      ...this.state.filterParams
    }
    // Load the new data
    this.loadDataFromServer(queryParams);
  }

  updateOpenNowFilterData = (shouldShowOpen) => {
    let newFilterParams = {
      ...this.state.filterParams
    }
    if (shouldShowOpen) {
      newFilterParams.open_during = Math.floor((moment().valueOf()) / 1000)
    } else {
      delete newFilterParams['open_during']
    }
    this.setState({filterParams: newFilterParams});
    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});
    let queryParams = {
      ...newFilterParams,
      ...this.state.searchParams,
      ...this.state.locationParams,
      ...this.state.sortParams,
      ...newPageParams
    }

    // Load the new data
    this.loadDataFromServer(queryParams);
  }

  updateRatingFilterData = (minRating) => {
    let newFilterParams = {
      ...this.state.filterParams
    }
    if (minRating !== 'none') {
      newFilterParams.min_rating = minRating
    } else {
      delete newFilterParams['min_rating']
    }
    this.setState({filterParams: newFilterParams});
    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});
    let queryParams = {
      ...newFilterParams,
      ...this.state.searchParams,
      ...this.state.locationParams,
      ...this.state.sortParams,
      ...newPageParams
    }

    // Load the new data
    this.loadDataFromServer(queryParams);
  }

  updateActivityFilterData = (numEvents) => {
    let newFilterParams = {
      ...this.state.filterParams
    }
    if (numEvents !== 'none') {
      newFilterParams.min_events = numEvents[0]
      newFilterParams.max_events = numEvents[1]
    } else {
      delete newFilterParams['min_events']
      delete newFilterParams['max_events']
    }
    this.setState({filterParams: newFilterParams});
    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});
    let queryParams = {
      ...newFilterParams,
      ...this.state.searchParams,
      ...this.state.locationParams,
      ...this.state.sortParams,
      ...newPageParams
    }

    // Load the new data
    this.loadDataFromServer(queryParams);
  }

  loadDataFromServer = params => {
    API.getSpaces(params).then(json => {
      this.setState({
        data: json.data,
        pageCount: parseInt(json.headers['num-pages'])
      })
    })
  }

  render() {
    const stopPropagation = (e) => e.stopPropagation();
    const InputWrapper = ({children}) => <div onClick={stopPropagation}>
      {children}
    </div>

    let spaces_keyword = "";
    if (typeof(this.state.searchParams.keyword) !== "undefined") {
      spaces_keyword = this.state.searchParams.keyword;
    }

    return (<div className="page-body">
      <Typography variant="h4" align="left" style={{
          'marginTop' : '20px'
        }}>Spaces</Typography>
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <InputWrapper>
            <SpaceSearchBar query={spaces_keyword} returnSearch={this.updateSpaceSearchData.bind(this)}/>
          </InputWrapper>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <SpaceFilterSortBar updateSpaceSortData={this.updateSpaceSortData.bind(this)} locEnabled={this.props.isGeolocationEnabled} updateOpenNow={this.updateOpenNowFilterData.bind(this)} updateRating={this.updateRatingFilterData.bind(this)} updateActivity={this.updateActivityFilterData.bind(this)}/>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <OverviewContainer updatePageCallback={this.updatePageData.bind(this)} page={this.state.pageParams.page - 1} pageCount={this.state.pageCount}>
      <br></br>
        {
          this.state.currQuery.length === 0 && <Grid container="container" spacing={16}>
              {this.state.data.map(item => <Grid item="item" xs={12} sm={4} key={item.space_id}><SpaceCard id='spaceCard' item={item} key={item.space_id} userLat={this.state.userLat} userLng={this.state.userLng}/></Grid>)}
            </Grid>
        }
        {this.state.currQuery.length !== 0 && this.state.data.length !== 0 && <SearchResultsList id="spaceSearchResultsList" data={this.state.data} type="spaces" query={this.state.currQuery}/>}
        {
          this.state.currQuery.length !== 0 && this.state.data.length === 0 && <Typography>
              No results.
            </Typography>
        }
      </OverviewContainer>
    </div>);
  }
}

export default geolocated({
  positionOptions: {
    enableHighAccuracy: false
  },
  userDecisionTimeout: 5000
})(Spaces);
