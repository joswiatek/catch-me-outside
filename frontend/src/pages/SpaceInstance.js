import React, {Component} from 'react';
import EventCard from '../components/event/EventCard.js';
import API from '../config/api.js';
import Reviews from '../components/review/Reviews.js';
import CardContainer from '../components/CardContainer.js';
// material-ui imports
import {withStyles} from '@material-ui/core/styles';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import StarRatings from 'react-star-ratings';
import LinkIcon from '@material-ui/icons/Link';
import PlaceIcon from '@material-ui/icons/Place';
import PhoneIcon from '@material-ui/icons/Phone';

const styles = {
  weblink: {
    color: '#1BC86C'
  },
  staticMap: {
    width: '100%',
    height: 350,
    marginTop: '30px'
  },
  bottomDivs: {
    marginTop: '70px'
  },
  icon: {
    fontSize: '20px',
    verticalAlign: 'top',
    paddingTop: '1px',
    paddingRight: '5px',
    position: 'relative',
    top: '-1px',
    color: '#1BC86C'
  },
  cards: {
    marginTop: '30px'
  }
};

class SpaceInstance extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data: {
        address: "",
        lat: "",
        long: "",
        name: "",
        open_hours: [],
        phone_number: "",
        photos: [],
        rating: "0",
        space_id: "",
        types: [],
        url: ""
      },
      photo_url: '-',
      events: [],
      groups: []
    };
  }

  componentDidMount() {
    const {id} = this.props.match.params;
    API.getSpace(id).then(json => {
      this.setState({data: json.data})
      let photo_num = Math.floor(Math.random() * 10);
      let photo = JSON.parse(json.data.photos[photo_num]);
      let photo_url = API.buildSpaceImageURL(json.data.space_id, photo['photo_reference'])
      this.setState({photo_url: photo_url});

      API.getEventsForSpace(json.data.space_id).then(events => {
        events = events.data.slice(0, 4)
        let groups = events.map(i => i.group_id)
        this.setState({events: events, groups: groups})
      })

    })
  }

  render() {
    return (<div className='pageColumn'>
      <div className='pageContent'>
        <div>
          <CardMedia style={{
              height: '275px',
              marginTop: "55px",
              borderRadius: "25px"
            }} image={this.state.photo_url} title=""/>
          <Typography id='spaceName' variant="h5" className='instanceTitle' align='left'>
            {this.state.data.name}
          </Typography>
          <StarRatings rating={parseFloat(this.state.data.rating)} numberOfStars={5} isSelectable={false} starDimension='20px' starSpacing='3px' starRatedColor={styles.weblink.color}/>
          <Typography variant="subtitle2" align='left'>
            <PlaceIcon style={styles.icon}/>{this.state.data.address}
          </Typography>
          <div style={{
              'margin' : '10px 0px'
            }}>
            <Typography variant="subtitle2" className='instanceSubtext' align='left'>
              <PhoneIcon style={styles.icon}/>{this.state.data.phone_number}
            </Typography>
            <Typography variant="subtitle2" className='instanceSubtext' align='left'>
              <LinkIcon style={styles.icon}/>
              <a href={this.state.data.url} style={styles.weblink} rel='noopener noreferrer'>{this.state.data.url}</a>
            </Typography>
          </div>
          {this.state.data.name && <iframe title='static map' style={styles.staticMap} src={'https://www.google.com/maps/embed/v1/place?key=' + process.env.REACT_APP_GOOGLE_MAPS_API_KEY + '&q=' + this.state.data.name} allowFullScreen="allowFullScreen"></iframe>}
        </div>
        <div style={styles.bottomDivs}>
          <Typography variant="h6">Reviews of this Space</Typography>
          {this.state.data.space_id && <Reviews id={this.state.data.space_id}/>}
        </div>

        <div style={styles.bottomDivs}>
          <Typography variant="h6">Upcoming events here</Typography>
          <Grid container="container" spacing={16}>
            {this.state.events.map(item => <Grid item="item" xs={12} sm={6} key={item.event_id}><EventCard id='spaceEventCard' item={item} key={item.event_id}/></Grid>)}
          </Grid>
        </div>

        <div style={styles.bottomDivs}>
          <Typography variant="h6">Groups that come here</Typography>
          <Grid container="container" spacing={16}>
            {this.state.groups.map(item => <Grid item="item" xs={12} sm={6} key={item}><CardContainer name='spaceGroupCard' type="group" uid={item}/></Grid>)}
          </Grid>
        </div>
      </div>
    </div>);
  }
}

export default withStyles(styles)(SpaceInstance);
