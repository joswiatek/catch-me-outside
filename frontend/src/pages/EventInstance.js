import React, {Component} from 'react';
import CardMedia from '@material-ui/core/CardMedia';
import EventSummary from "../components/event/EventSummary.js";
import CardContainer from '../components/CardContainer.js';
import Typography from '@material-ui/core/Typography';
import API from '../config/api.js'

const styles = {
  bottomDivs: {
    marginTop: '70px'
  }
};

class EventInstance extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data: {},
      photo: "",
      address: "",
      placeName: ""
    };
  }

  componentDidMount() {
    const {key} = this.props.match.params;
    // get event
    API.getEvent(key).then(json => {
      this.setState({data: json.data, placeName: json.data.venue_name, address: json.data.venue_address})

      // Try to set the group photo
      if (json.data.photo !== "") {
        this.setState({photo: json.data.photo});
      } else {
        // If there is no group photo, then get a photo from the space
        let space_id = json.data.space_id
        API.getSpace(space_id).then(json => {
          let photo_num = Math.floor(Math.random() * 10);
          let photo = JSON.parse(json.data.photos[photo_num]);
          let photo_url = API.buildSpaceImageURL(space_id, photo['photo_reference']);
          this.setState({photo: photo_url});
        });
      }

      if (json.data.venue_name === "" || json.data.venue_address === "") {
        API.getSpace(this.state.data.space_id).then(json => {
          this.setState({placeName: json.data.name, address: json.data.address});
        });
      }
    })
  };

  render() {
    return (<div className="pageColumn">
      <div className="pageContent">
        <CardMedia style={{
            height: '275px',
            marginTop: "55px",
            borderRadius: "25px"
          }} image={this.state.photo} title=""/> {this.state.placeName !== "" && <EventSummary data={this.state.data} venue={this.state.placeName} address={this.state.address}/>}
        <Typography variant="h6" style={{
            paddingTop: '40px'
          }}>Description</Typography>
        <Typography variant="body1">{this.state.data.description}</Typography>
        <div style={styles.bottomDivs}>
          <Typography variant="h6">Event Location</Typography>
          {this.state.data.space_id && <CardContainer name='eventSpaceCard' type="space" uid={this.state.data.space_id}/>}
        </div>
        <div style={styles.bottomDivs}>
          <Typography variant="h6">Group Hosting Event</Typography>
          {this.state.data.group_id && <CardContainer name='eventGroupCard' type="group" uid={this.state.data.group_id}/>}
        </div>
      </div>
    </div>);
  }
}

export default EventInstance;
