import React, {Component} from 'react';
import Typography from '@material-ui/core/Typography'
import API from '../config/api.js'
import {geolocated} from 'react-geolocated';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import SwipeableViews from 'react-swipeable-views';
import * as d3 from "d3";
import d3Tip from "d3-tip";

class Visuals extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabIndex: 0,
      space_names_ids: [],
      space_data_loaded: false,
      space_data_rendered: false,
      group_category_data_loaded: false,
      group_category_data_rendered: false,
      event_date_data_loaded: false,
      event_date_data_rendered: false,
      commitee_count_data_loaded: false,
      committee_count_data_rendered: false,
      charity_data_loaded: false,
      charity_data_rendered: false,
      pollutant_data_loaded: false,
      pollutant_data_rendered: false,
    }
  }

  // check for user location and set in state if available
  componentDidMount() {
    this.loadSpaceData();
    this.loadEventDateData();
    this.loadGroupCategoryData();
    this.loadCustomerData();
  }

  runD3 = () =>   {
    if (this.state.space_data_loaded && !this.state.space_data_rendered) {
      this.setState({space_data_rendered: true}, this.spaceBubbleChart('#ourSpaceVisDiv'));
    }
    if (this.state.event_date_data_loaded && !this.state.event_date_data_rendered) {
      this.setState({event_date_data_rendered: true}, this.eventDateChart('#ourEventVisDiv'));
    }
    if (this.state.group_category_data_loaded && !this.state.group_category_data_rendered) {
      this.setState({group_category_data_rendered: true}, this.groupCategoryChart('#ourGroupVisDiv'));
    }
    if (this.state.commitee_count_data_loaded && !this.state.committee_count_data_rendered) {
      this.setState({committee_count_data_rendered: true}, this.committeBubbleChart('#providerCommitteesVisDiv'));
    }
    if (this.state.charity_data_loaded && !this.state.charity_data_rendered) {
      this.setState({charity_data_rendered: true}, this.charityBarChart('#providerCharitiesVisDiv'));
    }
    if (this.state.pollutant_data_loaded && !this.state.pollutant_data_rendered) {
      this.setState({pollutant_data_rendered: true}, this.pollutantPieChart('#providerPollutantsVisDiv'));
    }
  }

  // inspired by https://vallandingham.me/bubble_charts_with_d3v4.html
  spaceBubbleChart = (selector) => {
    // Set up the simulation
    var width = 940;
    var height = 600;
    var center = {
      x: width / 2,
      y: height / 2
    };
    var forceStrength = 0.02;
    var toolTip = floatingTooltip('space_tt', 240);
    var svg = null;
    var bubbles = null;
    var nodes = [];

    // Create the nodes based on the data
    let space_data = this.state.space_data;
    var maxEvents = d3.max(Object.keys(this.state.space_data), function(k, index) {
      return space_data[k];
    });
    var minEvents = d3.min(Object.keys(this.state.space_data), function(k, index) {
      return space_data[k];
    });
    // For setting radius
    var radiusScale = d3.scalePow().exponent(0.5).range([20, 85]).domain([minEvents, maxEvents]);
    // For setting colors
    var fillColor = d3.scaleLinear().domain([minEvents, maxEvents]).range(['#e8f9f0', '#1BC86C']);

    nodes = this.state.space_names_ids.map(function(n) {
      return {
        name: n.name,
        id: n.space_id,
        numEvents: space_data[n.space_id],
        radius: radiusScale(space_data[n.space_id]),
        color: fillColor(space_data[n.space_id])
      }
    });
    nodes.sort(function(a, b) {
      return b.radius - a.radius;
    });

    function showSpaceDetail(d) {
      d3.select(this).attr('stroke', 'black');
      var content = '<span class="name">Space Name: </span><span class="value">' + d.name + '</span><br/><span class="name">Number of Events: </span><span class="value">' + d.numEvents + ' events</span>'
      toolTip.showTooltip(content, d3.event);
    }

    function hideSpaceDetail(d) {
      d3.select(this).attr('stroke', function(d) {return d3.rgb(fillColor(d.numEvents)).darker();});
      toolTip.hideTooltip();
    }

    // Select the given div and add our data
    svg = d3.select(selector).append('svg').attr('width', width).attr('height', height)

    bubbles = svg.selectAll('.bubble').data(nodes, function(d) {
      return d.id;
    });

    var bubblesE = bubbles.enter().append('circle').classed('bubble', true).attr('r', 0).attr('fill', function(d) {
      return fillColor(d.numEvents);
    }).attr('stroke', function(d) {
      return d3.rgb(fillColor(d.numEvents)).darker();
    }).attr('stroke-width', 2).on('mouseover', showSpaceDetail).on('mouseout', hideSpaceDetail).on('click', (d) => {
      window.open('https://catchmeoutside.today/spaces/' + d.id, '_self');
    });

    bubbles = bubbles.merge(bubblesE);
    bubbles.transition().duration(2000).attr('r', function(d) {
      return d.radius;
    });

    function simulationTicked() {
      bubbles.attr('cx', (d) => {
        return d.x
      }).attr('cy', (d) => {
        return d.y
      })
    }

    var bubbleCollideForce = d3.forceCollide().radius(function(d) {
      return d.radius + 0.5;
    }).iterations(4)

    // Create and start our simulation
    /* eslint-disable no-unused-vars */
    var simulation = d3.forceSimulation().velocityDecay(0.2).force('x', d3.forceX().strength(forceStrength).x(center.x)).force('y', d3.forceY().strength(forceStrength).y(center.y))
    // .force('charge', d3.forceManyBody().strength(repulseForce))
      .force('collide', bubbleCollideForce).on('tick', simulationTicked).nodes(nodes);
    /* eslint-enable no-unused-vars */
  };

  // inspired by https://observablehq.com/@d3/calendar-view
  eventDateChart = (selector) => {
    let width = 940;
    let height = 300;
    let maxEvents = d3.max(this.state.event_date_data, function(d) {
      return d.count;
    });
    let minEvents = d3.min(this.state.event_date_data, function(d) {
      return d.count;
    });

    let dayColor = d3.scalePow().exponent(0.5).domain([minEvents, maxEvents]).range(['#e8f9f0', '#1BC86C']);
    let timeWeek = d3.utcSunday;
    let countDay = d => d.getUTCDay();
    let formatDate = d3.utcFormat("%x");
    let formatDay = d => "SMTWTFS"[d.getUTCDay()];
    let formatMonth = d3.utcFormat("%b");
    function pathMonth(t) {
      const n = 7;
      const d = Math.max(0, Math.min(n, countDay(t)));
      const w = timeWeek.count(d3.utcYear(t), t);
      return `${d === 0
        ? `M${w * cellSize},0`
        : d === n
          ? `M${ (w + 1) * cellSize},0`
          : `M${ (w + 1) * cellSize},0V${d * cellSize}H${w * cellSize}`}V${n * cellSize}`;
    }

    let cellSize = 34;
    let yearHeight = cellSize * 9;

    var tip = d3Tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html(function(d) {
        return "<strong>" + formatDate(d.date) + " Event count:</strong> <span style='color:#1BC86C'>" + d.count + "</span>";
      })

    const years = d3.nest()
      .key(d => d.date.getUTCFullYear())
      .entries(this.state.event_date_data)
      .reverse();

    const svg = d3.select(selector)
      .append('svg')
      .style("font", "10px sans-serif")
      .style("width", width)
      .style("height", height)
      .call(tip);

    const year = svg.selectAll("g")
      .data(years)
      .join("g")
      .attr("transform", (d, i) => `translate(40,${yearHeight * i + cellSize * 1.5})`);

    year.append("text")
      .attr("x", -5)
      .attr("y", -5)
      .attr("font-weight", "bold")
      .attr("text-anchor", "end")
      .text(d => d.key);

    year.append("g")
      .attr("text-anchor", "end")
      .selectAll("text")
      .data(d3.range(7).map(i => new Date(1995, 0, i)))
      .join("text")
      .attr("x", -5)
      .attr("y", d => (countDay(d) + 0.5) * cellSize)
      .attr("dy", "0.31em")
      .text(formatDay);

    // TODO: update this if the offset starts being weird
    let monthBlockOffset = 400;
    year.append("g")
      .selectAll("rect")
      .data(d => d.values)
      .join("rect")
      .attr("width", cellSize - 1)
      .attr("height", cellSize - 1)
      .attr("x", d => timeWeek.count(d3.utcYear(d.date), d.date) * cellSize + 0.5 - monthBlockOffset)
      .attr("y", d => countDay(d.date) * cellSize + 0.5)
      .attr("fill", d => dayColor(d.count))
      .on('mouseover', tip.show)
      .on('mouseout', tip.hide)
      .append("title")
      .text(d => `${formatDate(d.date)}: ${d.count}`);

    const month = year.append("g")
      .selectAll("g")
      .data(d => d3.utcMonths(d3.utcMonth(d.values[0].date), d.values[d.values.length - 1].date))
      .join("g");

    month.filter((d, i) => i).append("path")
      .attr("fill", "none")
      .attr("stroke", "#fff")
      .attr("stroke-width", 3)
      .attr("d", pathMonth);

    month.append("text")
      .attr("x", d => timeWeek.count(d3.utcYear(d), timeWeek.ceil(d)) * cellSize + 2 - monthBlockOffset)
      .attr("y", -5)
      .text(formatMonth);
  }

  // inspired by http://bl.ocks.org/Caged/6476579
  groupCategoryChart = (selector) => {
    var margin = {top: 20, right: 55, bottom: 150, left: 120};
    let width = 960 - margin.left - margin.right;
    let height = 500 - margin.top - margin.bottom;
    let maxGroups = d3.max(this.state.group_category_data, function(d) {
      return d.count;
    });

    var x = d3.scaleBand()
      .domain(this.state.group_category_data.map(function(d) { return d.category; }))
      .rangeRound([0, width])
      .padding(0.1);

    var y = d3.scaleLinear()
      .domain([0, maxGroups+10])
      .range([height, 0]);

    var tip = d3Tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html(function(d) {
        return "<span style='color:#1BC86C'>" + d.count + "</span>";
      })

    var svg = d3.select(selector).append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.bottom + margin.top)
    .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    svg.call(tip);

    svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x))
    .selectAll("text")
      .attr("y", 12)
      .attr("x", 4)
      .attr("font-size", 16)
      .attr("dy", ".35em")
      .attr("transform", "rotate(45)")
      .style("text-anchor", "start")

    svg.append("g")
      .attr("class", "y axis")
      .call(d3.axisLeft(y))
    .append("text")
      .attr("x", -130)
      .attr("y", -60)
      .attr("font-size", 20)
      .attr("dy", ".91em")
      .attr("fill", "currentColor")
      .attr("transform", "rotate(-90)")
      .style("text-anchor", "end")
      .text("Group Count");

    svg.selectAll(".bar")
      .data(this.state.group_category_data)
    .enter().append("rect")
      .attr("class", "bar")
      .attr("x", function(d) { return x(d.category); })
      .attr("width", x.bandwidth())
      .attr("y", function(d) { return y(d.count); })
      .attr("height", function(d) { return height - y(d.count); })
      .on('mouseover', tip.show)
      .on('mouseout', tip.hide)
  }

  committeBubbleChart = (selector) => {
    // Set up the simulation
    var width = 940;
    var height = 600;
    var center = {
      x: width / 2,
      y: height / 2
    };
    var forceStrength = 0.02;
    var toolTip = floatingTooltip('committee_tt', 240);
    var svg = null;
    var bubbles = null;
    var nodes = [];

    // Create the nodes based on the data
    let committee_data = this.state.committee_count_data;
    var maxBills = d3.max(Object.keys(committee_data), function(k) {
      return committee_data[k].count;
    });
    var minBills = d3.min(Object.keys(committee_data), function(k, index) {
      return committee_data[k].count;
    });
    // For setting radius
    var radiusScale = d3.scalePow().exponent(0.5).range([20, 85]).domain([minBills, maxBills]);
    // For setting colors
    var fillColor = d3.scaleLinear().domain([minBills, maxBills]).range(['#e8f9f0', '#1BC86C']);

    nodes = this.state.committee_count_data.map(function(n) {
      return {
        name: n.committee,
        id: n.committee,
        billCount: n.count,
        radius: radiusScale(n.count),
        color: fillColor(n.count)
      }
    });
    nodes.sort(function(a, b) {
      return b.radius - a.radius;
    });

    function showCommitteeDetail(d) {
      d3.select(this).attr('stroke', 'black');
      var content = '<span class="name">Commitee Name: </span><span class="value">' + d.name + '</span><br/><span class="name">Number of Bills: </span><span class="value">' + d.billCount + '</span>'
      toolTip.showTooltip(content, d3.event);
    }

    function hideCommitteeDetail(d) {
      d3.select(this).attr('stroke', function(d) {return d3.rgb(d.color).darker();})
      toolTip.hideTooltip();
    }

    // Select the given div and add our data
    svg = d3.select(selector).append('svg').attr('width', width).attr('height', height)

    bubbles = svg.selectAll('.bubble').data(nodes, function(d) {
      return d.id;
    });

    var bubblesE = bubbles
      .enter()
      .append('circle')
      .classed('bubble', true)
      .attr('r', 0).attr('fill', function(d) {
        return d.color;})
      .attr('stroke', function(d) {
        return d3.rgb(d.color).darker();})
      .attr('stroke-width', 2)
      .on('mouseover', showCommitteeDetail)
      .on('mouseout', hideCommitteeDetail);

    bubbles = bubbles.merge(bubblesE);
    bubbles.transition().duration(2000).attr('r', function(d) {
      return d.radius;
    });

    function simulationTicked() {
      bubbles.attr('cx', (d) => {
          return d.x
        }).attr('cy', (d) => {
          return d.y
        });
    }

    var bubbleCollideForce = d3.forceCollide().radius(function(d) {
      return d.radius + 0.5;
    }).iterations(4)

    // Create and start our simulation
    /* eslint-disable no-unused-vars */
    var simulation = d3.forceSimulation().velocityDecay(0.2).force('x', d3.forceX().strength(forceStrength).x(center.x)).force('y', d3.forceY().strength(forceStrength).y(center.y))
    // .force('charge', d3.forceManyBody().strength(repulseForce))
      .force('collide', bubbleCollideForce).on('tick', simulationTicked).nodes(nodes);
    /* eslint-enable no-unused-vars */
  };

  charityBarChart = (selector) => {
    var margin = {top: 20, right: 55, bottom: 150, left: 120};
    let width = 960 - margin.left - margin.right;
    let height = 500 - margin.top - margin.bottom;
    let maxGroups = d3.max(this.state.charity_data, function(d) {
      return d.count;
    });

    var x = d3.scaleBand()
      .domain(this.state.charity_data.map(function(d) { return d.state; }))
      .rangeRound([0, width])
      .padding(0.1);

    var y = d3.scaleLinear()
      .domain([0, maxGroups+2])
      .range([height, 0]);

    var tip = d3Tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html(function(d) {
        return "<span style='color:#1BC86C'>" + d.count + "</span>";
      })

    var svg = d3.select(selector).append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.bottom + margin.top)
    .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    svg.call(tip);

    svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x))
    .selectAll("text")
      .attr("y", 12)
      .attr("x", 4)
      .attr("font-size", 16)
      .attr("dy", ".35em")
      .attr("transform", "rotate(45)")
      .style("text-anchor", "start")

    svg.append("g")
      .attr("class", "y axis")
      .call(d3.axisLeft(y))
    .append("text")
      .attr("x", -130)
      .attr("y", -60)
      .attr("font-size", 20)
      .attr("dy", ".91em")
      .attr("fill", "currentColor")
      .attr("transform", "rotate(-90)")
      .style("text-anchor", "end")
      .text("Charity Count");

    svg.selectAll(".bar")
      .data(this.state.charity_data)
    .enter().append("rect")
      .attr("class", "bar")
      .attr("x", function(d) { return x(d.state); })
      .attr("width", x.bandwidth())
      .attr("y", function(d) { return y(d.count); })
      .attr("height", function(d) { return height - y(d.count); })
      .on('mouseover', tip.show)
      .on('mouseout', tip.hide)
  }

  pollutantPieChart = (selector) => {
    let pollutant_data = this.state.pollutant_data;
    let total = pollutant_data.map(d => {return d.count;}).reduce((accum, val) => {return accum + val;});
    var w = 960;
    var h = 300;
    var r = 150;
    var color = d3.scaleOrdinal()
        .domain(pollutant_data.map(d => { return d.pollutant; }))
        .range(['#e8f9f0', '#82e1ae', '#1BC86C']);

    var svg = d3.select(selector)
        .append("svg:svg")              //create the SVG element inside the <body>
        .data([pollutant_data])                   //associate our data with the document
          .attr("width", w)           //set the width and height of our visualization (these will be attributes of the <svg> tag
          .attr("height", h)
        .append("svg:g")                //make a group to hold our pie chart
            // .attr("transform", "translate(" + r + "," + r + ")")    //move the center of the pie chart from 0, 0 to radius, radius
            .attr("transform", "translate(" + (w/2) + "," + (h/2) + ")")    //move the center of the pie chart from 0, 0 to radius, radius

    var arc = d3.arc()              //this will create <path> elements for us using arc data
        .outerRadius(r)
        .innerRadius(0);

    var pie = d3.pie()           //this will create arc data for us given a list of values
        .value(function(d) { return d.count; });    //we must tell it out to access the value of each element in our data array

    var arcs = svg.selectAll("g.slice")     //this selects all <g> elements with class slice (there aren't any yet)
        .data(pie)                          //associate the generated pie data (an array of arcs, each having startAngle, endAngle and value properties)
        .enter()                            //this will create <g> elements for every "extra" data element that should be associated with a selection. The result is creating a <g> for every object in the data array
        .append("svg:g")                //create a group to hold each slice (we will have a <path> and a <text> element associated with each slice)
          .attr("class", "slice");    //allow us to style things in the slices (like text)

        arcs.append("svg:path")
          .attr("fill", function(d, i) { return color(pollutant_data[i].count); } ) //set the color for each slice to be chosen from the color function defined above
          .attr("d", arc);                                    //this creates the actual SVG path using the associated data (pie) with the arc drawing function

        arcs.append("svg:text")                                     //add a label to each slice
          .attr("transform", function(d) {                    //set the label's origin to the center of the arc
              //we have to make sure to set these before calling arc.centroid
              d.innerRadius = 0;
              d.outerRadius = r;
              return "translate(" + arc.centroid(d) + ")";        //this gives us a pair of coordinates like [50, 50]
            })
          .attr("text-anchor", "middle")                          //center the text on it's origin
          .text(function(d, i) { return pollutant_data[i].name + ": " + (100*pollutant_data[i].count/total).toFixed(1) + "%"; });        //get the label from our original data array
  }

  async loadSpaceData() {
    let space_names = await API.getSpaceNames();
    let result = await Promise.all(space_names.data.map(async function(x) {
      let events = await API.getEventsForSpace(x.space_id);
      return {
        [x.space_id]: events.data.length
      };
    }));
    result = result.reduce((accumulator, space) => {
      return {
        ...accumulator,
        ...space
      };
    })
    this.setState({
        space_names_ids: space_names.data,
        space_data: result,
        space_data_loaded: true});
  }

  async loadEventDateData() {
    var event_first_page = await API.getEvents();
    var pageCount = parseInt(event_first_page.headers['num-pages']);
    let event_dates = await Promise.all([...Array(pageCount).keys()].map(async n => {
      var event_json = await API.getEvents({
        page: n + 1
      });
      return event_json.data.map(e => e.local_date);
    }));
    event_dates = event_dates.reduce((accumulator, event_page) => accumulator.concat(event_page));
    var date_counts = {};
    for (var i in event_dates) {
      date_counts[event_dates[i]] = date_counts[event_dates[i]]
        ? date_counts[event_dates[i]] + 1
        : 1;
    }
    var parseTime = d3.timeParse("%Y-%m-%d");
    var result_data = Object.keys(date_counts).map(d => {
      return {date: parseTime(d), count: date_counts[d]}
    });
    this.setState({
        event_date_data: result_data,
        event_date_data_loaded: true});
  };

  async loadGroupCategoryData() {
    var group_first_page = await API.getGroups();
    var pageCount = parseInt(group_first_page.headers['num-pages']);
    let group_categories = await Promise.all([...Array(pageCount).keys()].map(async n => {
      var group_json = await API.getGroups({
        page: n + 1
      });
      return group_json.data.map(e => e.categories);
    }));
    group_categories = group_categories.reduce((accumulator, group_page) => accumulator.concat(group_page));
    var category_counts = {};
    for (var i in group_categories) {
      category_counts[group_categories[i]] = category_counts[group_categories[i]]
        ? category_counts[group_categories[i]] + 1
        : 1;
    }
    var result_data = Object.keys(category_counts).map(d => {
      return {category: d, count: category_counts[d]}
    });
    result_data.sort((a,b) => {
      return b.count - a.count;
    });
    this.setState({
        group_category_data: result_data,
        group_category_data_loaded: true});
  };

  async loadCustomerData() {
    var customer_all = await API.getCustomerData();
    let bills_data = customer_all.data.bills;
    let charity_data = customer_all.data.charities;
    let location_data = customer_all.data.locations;

    // Count the number of bills from each committee
    var committee_counts = {};
    for (var i in bills_data) {
      var committee_arr = [];
      if (bills_data[i].committees != null) {
        committee_arr = bills_data[i].committees.split(';').map((com) => com.trim());
      } else {
        committee_arr = ['Unassigned'];
      }
      for (var j in committee_arr) {
        let type = committee_arr[j];
        committee_counts[type] = committee_counts[type] ? committee_counts[type] + 1 : 1;
      }
    }
    var result_data = Object.keys(committee_counts).map(d => {
      return {committee: d, count: committee_counts[d]}
    });
    result_data.sort((a,b) => {
      return b.count - a.count;
    });
    this.setState({
        committee_count_data: result_data,
        commitee_count_data_loaded: true});

    // Count the number of charities in each state
    var charity_counts = {};
    for (i in charity_data) {
      let type = charity_data[i].state;
      charity_counts[type] = charity_counts[type] ? charity_counts[type] + 1 : 1;
    }
    result_data = Object.keys(charity_counts).map(d => {
      return {state: d, count: charity_counts[d]}
    });
    result_data.sort((a,b) => {
      return b.count - a.count;
    });
    this.setState({
        charity_data: result_data,
        charity_data_loaded: true});

    // Count the number of each type of pollutants
    var pollutant_counts = {};
    for (i in location_data) {
      let type = location_data[i].dominant_pol;
      pollutant_counts[type] = pollutant_counts[type] ? pollutant_counts[type] + 1 : 1;
    }
    result_data = Object.keys(pollutant_counts).map(d => {
      return {name: d, count: pollutant_counts[d]}
    });
    result_data.sort((a,b) => {
      return b.count - a.count;
    });
    this.setState({
        pollutant_data: result_data,
        pollutant_data_loaded: true});
  };

  // For the tabs
  handleChange = (event, tabIndex) => {
    this.setState({tabIndex});
  };
  // For the tabs
  handleChangeIndex = index => {
    this.setState({tabIndex: index});
  };

  render() {
    // Render d3 visuals
    this.runD3();
    return (<div className="page-body">
      <Typography variant="h4" align="left" style={{
          'marginTop' : '20px'
        }}>Visualizations</Typography>

      <Tabs paddingTop="100px" value={this.state.tabIndex} onChange={this.handleChange} indicatorColor="primary" textColor="primary" variant="fullWidth">
        <Tab id="ourVisTab" label={"Catch Me Outside"} style={{textTransform: 'none', fontSize:'16px'}}/>
        <Tab id="providerVisTab" label={"Care for the Air"} style={{textTransform: 'none', fontSize:'16px'}}/>
      </Tabs>

      <SwipeableViews axis={'x'} index={this.state.tabIndex} onChangeIndex={this.handleChangeIndex}>
        <TabContainer>
          <Typography variant="h4" align="left" style={{'marginTop' : '20px'}}>Number of Events Per Space</Typography>
          <Typography component="p" variant='subtitle1' align="left">Click on a bubble to open the relevant space in a new tab!</Typography>
          <div id="ourSpaceVisDiv"></div>
          <Typography variant="h4" align="left" style={{'marginTop' : '20px'}}>Calendar Heatmap of Event Count</Typography>
          <div id="ourEventVisDiv"></div>
          <Typography variant="h4" align="left" style={{'marginTop' : '20px'}}>Categories by Group Count</Typography>
          <div id="ourGroupVisDiv"></div>
        </TabContainer>
        <TabContainer>
          <Typography variant="h4" align="left" style={{'marginTop' : '20px'}}>Number of Bills Per Committee</Typography>
          <div id="providerCommitteesVisDiv"></div>
          <Typography variant="h4" align="left" style={{'marginTop' : '20px'}}>Number of Charities per State</Typography>
          <div id="providerCharitiesVisDiv"></div>
          <Typography variant="h4" align="left" style={{'marginTop' : '20px'}}>Pie Chart of Pollutants</Typography>
          <div id="providerPollutantsVisDiv"></div>
        </TabContainer>
      </SwipeableViews>

    </div>);
  }
}

function TabContainer({children, dir}) {
  return (<Typography component="div" dir={dir} style={{
      padding: 8 * 3
    }}>
    {children}
  </Typography>);
}

// Adopted from https://github.com/vlandham/bubble_chart_v4/blob/master/src/tooltip.js
function floatingTooltip(tooltipId, width) {
  var tt = d3.select('body').append('div').attr('class', 'tooltip').attr('id', tooltipId).style('pointer-events', 'none');

  if (width) {
    tt.style('width', width);
  }

  hideTooltip();

  function showTooltip(content, event) {
    tt.style('opacity', 1.0).html(content);

    updatePosition(event);
  }

  function hideTooltip() {
    tt.style('opacity', 0.0);
  }

  function updatePosition(event) {
    var xOffset = 20;
    var yOffset = 10;

    var ttw = tt.style('width');
    var tth = tt.style('height');

    var wscrY = window.scrollY;
    var wscrX = window.scrollX;

    var curX = (document.all)
      ? event.clientX + wscrX
      : event.pageX;
    var curY = (document.all)
      ? event.clientY + wscrY
      : event.pageY;
    var ttleft = ((curX - wscrX + xOffset * 2 + ttw) > window.innerWidth)
      ? curX - ttw - xOffset * 2
      : curX + xOffset;
    if (ttleft < wscrX + xOffset) {
      ttleft = wscrX + xOffset;
    }
    var tttop = ((curY - wscrY + yOffset * 2 + tth) > window.innerHeight)
      ? curY - tth - yOffset * 2
      : curY + yOffset;
    if (tttop < wscrY + yOffset) {
      tttop = curY + yOffset;
    }
    tt.style('top', tttop + 'px').style('left', ttleft + 'px');
  }
  return {showTooltip: showTooltip, hideTooltip: hideTooltip, updatePosition: updatePosition};
}

export default geolocated({
  positionOptions: {
    enableHighAccuracy: false
  },
  userDecisionTimeout: 5000
})(Visuals);
