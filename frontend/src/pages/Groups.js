import React, {Component} from 'react';
import GroupCard from '../components/group/GroupCard.js';
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid';
import OverviewContainer from '../components/OverviewContainer.js';
import GroupSearchBar from '../components/group/GroupSearchBar.js';
import GroupFilterSortBar from '../components/group/GroupFilterSortBar.js';
import SearchResultsList from '../components/search/SearchResultsList.js';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

import API from '../config/api.js'

class Groups extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      pageCount: 0,
      searchParams: {},
      filterMemberParams: {},
      filterCategoryParams: {},
      filterJoinParams: {},
      sortParams: {},
      pageParams: {},
      currQuery: []
    };
  }

  updateGroupSortData = (sort, order) => {
    let newSortParams = {
      sort: sort,
      order: order
    }

    // Update the state's sort params
    this.setState({sortParams: newSortParams});

    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});

    // Add in the search and filter params
    let queryParams = {
      ...newSortParams,
      ...this.state.searchParams,
      ...newPageParams,
      ...this.state.filterCategoryParams,
      ...this.state.filterJoinParams,
      ...this.state.filterMemberParams
    };
    // Load the new data
    this.loadDataFromServer(queryParams);
  }

  updateGroupSearchData = query => {
    // Reset to page 1
    let newSearchParams = {}
    if (query !== "") {
      newSearchParams.keyword = query;
      this.setState({currQuery: query.split(" ")});
    } else {
      this.setState({currQuery: []});
    }

    // Update the state's search params
    this.setState({searchParams: newSearchParams});

    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});

    // Add in the filter and sort params
    let queryParams = {
      ...newSearchParams,
      ...this.state.filterCategoryParams,
      ...this.state.filterMemberParams,
      ...this.state.filterJoinParams,
      ...newPageParams,
      ...this.state.sortParams
    };
    // Load the new data
    this.loadDataFromServer(queryParams);
  }

  updatePageData = pageNum => {
    let newPageParams = {};

    newPageParams.page = pageNum;

    // Update the state's filter params
    this.setState({pageParams: newPageParams});

    // Add in the other parameters
    let queryParams = {
      ...newPageParams,
      ...this.state.searchParams,
      ...this.state.filterCategoryParams,
      ...this.state.filterJoinParams,
      ...this.state.filterMemberParams,
      ...this.state.sortParams
    };

    // Load the new data
    this.loadDataFromServer(queryParams);
  }
  updateGroupFilterCategory = (value) => {
    let newCategoryFilterParams = {};
    if (value !== '') {
      newCategoryFilterParams.category = value;
    }
    this.setState({filterCategoryParams: newCategoryFilterParams});
    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});
    // Add in the other parameters
    let queryParams = {
      ...newCategoryFilterParams,
      ...this.state.searchParams,
      ...this.state.locationParams,
      ...this.state.filterMemberParams,
      ...this.state.filterJoinParams,
      ...newPageParams,
      ...this.state.sortParams
    }
    this.loadDataFromServer(queryParams);
  }
  updateGroupFilterJoinMode = (value) => {
    let newJoinFilterParams = {};
    if (value !== '' && value !== 'none') {
      newJoinFilterParams.join_mode = value;
    }
    this.setState({filterJoinParams: newJoinFilterParams});
    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});
    // Add in the other parameters
    let queryParams = {
      ...newJoinFilterParams,
      ...this.state.searchParams,
      ...this.state.locationParams,
      ...this.state.filterMemberParams,
      ...this.state.filterCategoryParams,
      ...newPageParams,
      ...this.state.sortParams
    }
    this.loadDataFromServer(queryParams);
  }
  updateGroupFilterMembers = (min, max) => {
    let newMemberFilterParams = {};
    if (min !== '') {
      newMemberFilterParams.min_member_count = parseInt(min);
    }
    if (max !== '') {
      newMemberFilterParams.max_member_count = parseInt(max);
    }
    // Update the state's filter params
    this.setState({filterMemberParams: newMemberFilterParams});
    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});

    // Add in the other parameters
    let queryParams = {
      ...newMemberFilterParams,
      ...this.state.searchParams,
      ...this.state.locationParams,
      ...this.state.filterCategoryParams,
      ...this.state.filterJoinParams,
      ...newPageParams,
      ...this.state.sortParams
    }

    // Load the new data
    this.loadDataFromServer(queryParams);
  }

  loadDataFromServer = params => {
    API.getGroups(params).then(json => {
      this.setState({data: json.data, pageCount: json.headers['num-pages']})
    })
  }

  render() {
    const groups_data = this.state.data;
    const stopPropagation = (e) => e.stopPropagation();
    const InputWrapper = ({children}) => <div onClick={stopPropagation}>
      {children}
    </div>

    let group_keyword = "";
    if (typeof(this.state.searchParams.keyword) !== "undefined") {
      group_keyword = this.state.searchParams.keyword;
    }

    return (<div className="page-body">

      <Typography variant="h4" align="left" style={{
          'marginTop' : '20px'
        }}>Groups</Typography>
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <InputWrapper>
            <GroupSearchBar query={group_keyword} returnSearch={this.updateGroupSearchData.bind(this)}/>
          </InputWrapper>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <GroupFilterSortBar updateGroupSortData={this.updateGroupSortData.bind(this)} updateGroupFilterMembers={this.updateGroupFilterMembers.bind(this)} updateGroupFilterCategory={this.updateGroupFilterCategory.bind(this)} updateGroupFilterJoinMode={this.updateGroupFilterJoinMode.bind(this)}/>
        </ExpansionPanelDetails>
      </ExpansionPanel>

      <OverviewContainer updatePageCallback={this.updatePageData.bind(this)} page={this.state.pageParams.page - 1} pageCount={this.state.pageCount}>
        <br></br>
        {
          this.state.currQuery.length === 0 && <Grid container="container" spacing={16}>
              {
                groups_data.map(item => <Grid item="item" xs={12} sm={4} key={item.group_id}>
                  <GroupCard id='groupCard' item={item} key={item.group_id}/>
                  <br/>
                </Grid>)
              }
            </Grid>
        }
        {this.state.currQuery.length > 0 && <SearchResultsList id="groupSearchResultList" data={groups_data} type="groups" query={this.state.currQuery}/>}
      </OverviewContainer>
    </div>);
  }
}

export default Groups;
