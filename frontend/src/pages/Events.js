import React, {Component} from 'react';
import EventCard from '../components/event/EventCard.js';
import EventSearchBar from '../components/event/EventSearchBar.js';
import EventFilterSortBar from '../components/event/EventFilterSortBar.js';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import OverviewContainer from '../components/OverviewContainer.js';
import SearchResultsList from '../components/search/SearchResultsList.js';
import API from '../config/api.js';
import moment from 'moment';

import {geolocated} from 'react-geolocated';

class Events extends Component {
  constructor(props) {
    super(props);

    let today = new Date();
    let todayFormatted = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();

    this.state = {
      data: [],
      pageCount: 0,
      userLat: 0,
      userLng: 0,
      searchParams: {},
      filterAttendeeParams: {},
      filterDateParams: { startDate: todayFormatted },
      filterTimeParams: {},
      sortParams: {},
      locationParams: {},
      pageParams: {
        page: 1
      },
      currQuery: []
    };
  }

  updateEventSearchData = (query, space_name, location_id) => {
    // Reset to page 1
    let newSearchParams = {}
    if (query !== "") {
      newSearchParams.keyword = query;
      // Update the current query
      this.setState({currQuery: query.split(" ")});
    } else {
      this.setState({currQuery: []});
    }

    let newLocationParams = {};
    if (location_id !== "") {
      newLocationParams.space_id = location_id;
      newLocationParams.space_name = space_name;
    }

    // Update the state's search and location params
    this.setState({searchParams: newSearchParams});
    this.setState({locationParams: newLocationParams});

    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});

    // Add in the filter and sort params
    let queryParams = {
      ...newSearchParams,
      ...newLocationParams,
      ...this.state.filterAttendeeParams,
      ...this.state.filterDateParams,
      ...this.state.filterTimeParams,
      ...newPageParams,
      ...this.state.sortParams
    };

    // Load the new data
    this.loadDataFromServer(queryParams);
  }

  updateEventSortData = (sort, order) => {
    let newSortParams = {
      sort: sort,
      order: order
    }

    // TODO: what to do if the user lat/long is not set?
    if (this.state.userLat !== 0 && this.state.userLng !== 0) {
      let userLocStr = String(this.state.userLat) + "," + String(this.state.userLng)
      newSortParams['user_loc'] = userLocStr
    }

    // Update the state's sort params
    this.setState({sortParams: newSortParams});
    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});

    // Add in the search and filter params
    let queryParams = {
      ...newSortParams,
      ...this.state.searchParams,
      ...this.state.locationParams,
      ...this.state.filterAttendeeParams,
      ...newPageParams,
      ...this.state.filterDateParams,
      ...this.state.filterTimeParams
    };

    // Load the new data
    this.loadDataFromServer(queryParams);
  }

  updateAttendeeFilterData = (min, max) => {
    let newAttendeeFilterParams = {}
    if (min !== '') {
      newAttendeeFilterParams.min_attendees = parseInt(min);
    }
    if (max !== '') {
      newAttendeeFilterParams.max_attendees = parseInt(max);
    }
    // Update the state's filter params
    this.setState({filterAttendeeParams: newAttendeeFilterParams});
    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});

    // Add in the other parameters
    let queryParams = {
      ...newAttendeeFilterParams,
      ...this.state.searchParams,
      ...this.state.locationParams,
      ...this.state.filterDateParams,
      ...this.state.filterTimeParams,
      ...newPageParams,
      ...this.state.sortParams
    }

    // Load the new data
    this.loadDataFromServer(queryParams);
  }

  updateDateFilterData = (start, end) => {
    let newDateFilterParams = {};
    let startDate = moment(start);
    let endDate = moment(end);

    if (start != null) {
      newDateFilterParams.start_date = startDate.format("YYYY-MM-DD");
    }
    if (end != null) {
      newDateFilterParams.end_date = endDate.format("YYYY-MM-DD");
    }
    // Update the state's filter params
    this.setState({filterDateParams: newDateFilterParams});
    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});

    // Add in the other parameters
    let queryParams = {
      ...newDateFilterParams,
      ...this.state.filterAttendeeParams,
      ...this.state.filterTimeParams,
      ...this.state.searchParams,
      ...this.state.locationParams,
      ...newPageParams,
      ...this.state.sortParams
    }

    // Load the new data
    this.loadDataFromServer(queryParams);
  }

  updateTimeFilterData = (morning, afternoon, evening) => {
    let newTimeFilterParams = {};

    let times = [];
    let flag = false;
    if (morning) {
      flag = true;
      times.push("morning");
    }
    if (afternoon) {
      flag = true;
      times.push("afternoon");
    }
    if (evening) {
      flag = true;
      times.push("night");
    }
    if (flag) {
      newTimeFilterParams.times_include = times.join(',');
    }

    // Update the state's filter params
    this.setState({filterTimeParams: newTimeFilterParams});
    let newPageParams = {
      page: 1
    };
    this.setState({pageParams: newPageParams});

    // Add in the other parameters
    let queryParams = {
      ...newTimeFilterParams,
      ...this.state.filterDateParams,
      ...this.state.filterAttendeeParams,
      ...this.state.searchParams,
      ...this.state.locationParams,
      ...newPageParams,
      ...this.state.sortParams
    }

    // Load the new data
    this.loadDataFromServer(queryParams);
  }

  updatePageData = pageNum => {
    let newPageParams = {};

    newPageParams.page = pageNum;

    // Update the state's filter params
    this.setState({pageParams: newPageParams});

    // Add in the other parameters
    let queryParams = {
      ...newPageParams,
      ...this.state.filterDateParams,
      ...this.state.filterTimeParams,
      ...this.state.filterAttendeeParams,
      ...this.state.searchParams,
      ...this.state.locationParams,
      ...this.state.sortParams
    }

    // Load the new data
    this.loadDataFromServer(queryParams);
  }

  // check for user location and set in state if available
  componentDidMount() {
    if (this.props.userLat !== 0 && this.props.userLng !== 0) {
      this.setState({userLat: this.props.userLat, userLng: this.props.userLng})
    }
  }

  // runs when async call to get location is returned
  componentWillReceiveProps = (nextProps) => {
    if (this.state.userLng === 0 || this.state.userLat === 0) {
      if (nextProps.hasOwnProperty('coords') && nextProps.coords !== null) {
        if (nextProps.coords.latitude !== null && nextProps.coords.longitude !== null) {
          this.setState({userLat: nextProps.coords.latitude, userLng: nextProps.coords.longitude})
        }
      } else if (nextProps.hasOwnProperty('userLat') && nextProps.hasOwnProperty('userLng')) {
        if (nextProps.userLat !== 0 && nextProps.userLng !== 0) {
          this.setState({userLat: nextProps.userLat, userLng: nextProps.userLng})
        }
      }
    }
  }

  loadDataFromServer = params => {
    API.getEvents(params).then(json => {
      this.setState({data: json.data, pageCount: json.headers['num-pages']})
    })
  }

  render() {
    const events_data = this.state.data;
    const stopPropagation = (e) => e.stopPropagation();
    const InputWrapper = ({children}) => <div onClick={stopPropagation}>
      {children}
    </div>

    let event_keyword = "";
    if (typeof(this.state.searchParams.keyword) !== "undefined") {
      event_keyword = this.state.searchParams.keyword;
    }
    let space_name = "";
    let space_id = "";
    if (typeof(this.state.locationParams.space_name) !== "undefined" && this.state.locationParams.space_name !== "") {
      space_name = this.state.locationParams.space_name;
      space_id = this.state.locationParams.space_id;
    }

    return (<div className="page-body">
      <Typography variant="h4" align="left" style={{
        'marginTop' : '20px'
        }}>Events</Typography>
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <InputWrapper>
            <EventSearchBar query={event_keyword} value={space_name} space_id={space_id} returnSearch={this.updateEventSearchData.bind(this)} onClick={this.clickSummary}/>
          </InputWrapper>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <EventFilterSortBar updateEventSortData={this.updateEventSortData.bind(this)} updateAttendeeFilterCallback={this.updateAttendeeFilterData.bind(this)} updateDateFilterCallback={this.updateDateFilterData.bind(this)} updateTimeFilterCallback={this.updateTimeFilterData.bind(this)} locEnabled={this.props.isGeolocationEnabled}/>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <OverviewContainer updatePageCallback={this.updatePageData.bind(this)} page={this.state.pageParams.page - 1} pageCount={this.state.pageCount}>
        <br></br>
        {
          this.state.currQuery.length === 0 && <Grid container="container" spacing={16}>
              {events_data.map(item => <Grid item="item" xs={12} sm={4} key={item.event_id}><EventCard id='eventCard' item={item} query={this.state.currQuery} key={item.event_id} userlat={this.state.userLat} userlng={this.state.userLng}/></Grid>)}
            </Grid>
        }
        {this.state.currQuery.length > 0 && <SearchResultsList id="eventSearchResultList" data={events_data} type="events" query={this.state.currQuery}/>}
      </OverviewContainer>
    </div>);
  }
}

export default geolocated({
  positionOptions: {
    enableHighAccuracy: false
  },
  userDecisionTimeout: 5000
})(Events);
