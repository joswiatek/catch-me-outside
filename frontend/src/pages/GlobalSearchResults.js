import React, {Component} from 'react';

import Typography from '@material-ui/core/Typography'
import AllSearchBar from '../components/search/AllSearchBar.js'
import SearchResultsList from '../components/search/SearchResultsList.js';
import OverviewContainer from '../components/OverviewContainer.js';
import API from '../config/api.js'
//icons
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import SwipeableViews from 'react-swipeable-views';

class GlobalSearchResults extends Component {
  constructor(props) {
    super(props);
    let initial_keyword = "";
    if (typeof(this.props.location.state) != "undefined") {
      initial_keyword = this.props.location.state.initialValue;
    } else {
      console.error("location state undefined");
    }

    this.state = {
      event_data: [],
      group_data: [],
      space_data: [],
      eventPageCount: 0,
      groupPageCount: 0,
      spacePageCount: 0,
      eventTotal: 0,
      groupTotal: 0,
      spaceTotal: 0,
      tabIndex: 0,
      // Populate the initial keyword from whatever was passed from the Splash page
      searchParams: {
        keyword: initial_keyword
      },
      eventPageParams: {
        page: 1
      },
      groupPageParams: {
        page: 1
      },
      spacePageParams: {
        page: 1
      }
    };
  }

  componentDidMount() {
    if (this.state.searchParams.keyword !== "") {
      this.updateGlobalSearchData(this.state.searchParams.keyword);
    }
  }

  updateGlobalSearchData = query => {
    let newSearchParams = {}
    if (query !== "") {
      newSearchParams.keyword = query;
      // Update the current query
      this.setState({currQuery: query.split(" ")});
    } else {
      this.setState({currQuery: []});
    }

    // Update the state's search and location params
    this.setState({searchParams: newSearchParams});
    let newPageParams = {
      page: 1
    };
    this.setState({eventPageParams: newPageParams});
    this.setState({groupPageParams: newPageParams});
    this.setState({spacePageParams: newPageParams});

    // Add in the filter and sort params
    let queryParams = {
      ...newSearchParams,
      ...newPageParams
    };

    // Update the event, group, and space data with the new search query
    this.loadEventDataFromServer(queryParams);
    this.loadGroupDataFromServer(queryParams);
    this.loadSpaceDataFromServer(queryParams);
  }

  updateEventPageAndLoadData = pageNum => {
    let newEventPageParams = {};
    newEventPageParams.page = pageNum;

    // Update the state
    this.setState({eventPageParams: newEventPageParams});

    // Add in the other query parameters
    let queryParams = {
      ...newEventPageParams,
      ...this.state.searchParams
    }
    this.loadEventDataFromServer(queryParams);
  }

  updateGroupPageAndLoadData = pageNum => {
    let newGroupPageParams = {};
    newGroupPageParams.page = pageNum;

    // Update the state
    this.setState({groupPageParams: newGroupPageParams});

    // Add in the other query parameters
    let queryParams = {
      ...newGroupPageParams,
      ...this.state.searchParams
    }
    this.loadGroupDataFromServer(queryParams);
  }

  updateSpacePageAndLoadData = pageNum => {
    let newSpacePageParams = {};
    newSpacePageParams.page = pageNum;

    // Update the state
    this.setState({spacePageParams: newSpacePageParams});

    // Add in the other query parameters
    let queryParams = {
      ...newSpacePageParams,
      ...this.state.searchParams
    }
    this.loadSpaceDataFromServer(queryParams);
  }

  loadEventDataFromServer = params => {
    API.getEvents(params).then(json => {
      this.setState({event_data: json.data, eventPageCount: json.headers['num-pages'], eventTotal: json.headers['total']});
    });
  }

  loadGroupDataFromServer = params => {
    // Groups
    API.getGroups(params).then(json => {
      this.setState({group_data: json.data, groupPageCount: json.headers['num-pages'], groupTotal: json.headers['total']});
    });
  }

  loadSpaceDataFromServer = params => {
    // Spaces
    API.getSpaces(params).then(json => {
      this.setState({space_data: json.data, spacePageCount: json.headers['num-pages'], spaceTotal: json.headers['total']});
    });
  }

  handleChange = (event, tabIndex) => {
    this.setState({tabIndex});
  };

  handleChangeIndex = index => {
    this.setState({tabIndex: index});
  };

  render() {
    return (<div className="page-body">
      <div className='search-body' style={{
          paddingBottom: 20
        }}>
        <AllSearchBar query={this.state.searchParams.keyword} splashSearch={false} returnSearch={this.updateGlobalSearchData.bind(this)}/>
      </div>
      <Tabs paddingTop="100px" value={this.state.tabIndex} onChange={this.handleChange} indicatorColor="primary" textColor="primary" variant="fullWidth">
        <Tab id="eventSearchTab" label={"EVENTS: " + this.state.eventTotal + " results"} style={{textTransform: 'capitalize', fontSize:'16px'}}/>
        <Tab id="groupSearchTab" label={"GROUPS: " + this.state.groupTotal + " results"} style={{textTransform: 'capitalize', fontSize:'16px'}}/>
        <Tab id="spaceSearchTab" label={"SPACES: " + this.state.spaceTotal + " results"} style={{textTransform: 'capitalize', fontSize:'16px'}}/>
      </Tabs>

      <SwipeableViews axis={'x'} index={this.state.tabIndex} onChangeIndex={this.handleChangeIndex}>
        <TabContainer>
          {
            this.state.event_data.length > 0 && <OverviewContainer updatePageCallback={this.updateEventPageAndLoadData.bind(this)} page={this.state.eventPageParams.page - 1} pageCount={this.state.eventPageCount}>
                <SearchResultsList id='globalEventResult' data={this.state.event_data} type="events" query={this.state.currQuery}/>
              </OverviewContainer>
          }
          {this.state.event_data.length === 0 && "No events found for your query. :("}
        </TabContainer>
        <TabContainer>
          {
            this.state.group_data.length > 0 && <OverviewContainer updatePageCallback={this.updateGroupPageAndLoadData.bind(this)} page={this.state.groupPageParams.page - 1} pageCount={this.state.groupPageCount}>
                <SearchResultsList id='globalGroupResult' data={this.state.group_data} type="groups" query={this.state.currQuery}/>
              </OverviewContainer>
          }
          {this.state.group_data.length === 0 && "No groups found for your query. :("}
        </TabContainer>
        <TabContainer>
          {
            this.state.space_data.length > 0 && <OverviewContainer updatePageCallback={this.updateSpacePageAndLoadData.bind(this)} page={this.state.spacePageParams.page - 1} pageCount={this.state.spacePageCount}>
                <SearchResultsList id='globalSpaceResult' data={this.state.space_data} type="spaces" query={this.state.currQuery}/>
              </OverviewContainer>
          }
          {this.state.space_data.length === 0 && "No spaces found for your query. :("}
        </TabContainer>
      </SwipeableViews>

    </div>);
  }
}

function TabContainer({children, dir}) {
  return (<Typography component="div" dir={dir} style={{
      padding: 8 * 3
    }}>
    {children}
  </Typography>);
}

export default GlobalSearchResults;
