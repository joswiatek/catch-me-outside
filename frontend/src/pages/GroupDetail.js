import React, {Component} from 'react';
import CardMedia from '@material-ui/core/CardMedia';
import EventCard from '../components/event/EventCard.js';
import Typography from '@material-ui/core/Typography';
import API from '../config/api.js'
import CardContainer from '../components/CardContainer.js';
import Grid from '@material-ui/core/Grid';
import PlaceIcon from '@material-ui/icons/Place';
import GroupIcon from '@material-ui/icons/Group';
import GroupOrganizerCard from '../components/group/GroupOrganizerCard.js';

const styles = {
  cardMedia: {
    height: '250px'
  },
  description: {
    marginTop: '30px'
  },
  icon: {
    fontSize: '20px',
    verticalAlign: 'top',
    paddingTop: '5px',
    paddingRight: '5px',
    position: 'relative',
    top: '-1px',
    color: '#1BC86C'
  }
};

class GroupDetail extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data: {},
      photo_url: 'placeholder',
      events: [],
      space_ids: []
    };
  }

  componentDidMount() {
    const {id} = this.props.match.params;
    API.getGroup(id).then(json => {
      this.setState({data: json.data})
      // Get the events for this group
      API.getEventsForGroup(json.data.group_id).then(jsonEvents => {
        let events = jsonEvents.data.sort((a, b) => {
          return a.local_date < b.local_date
            ? -1
            : 1
        }).slice(0, 4);
        let spaces = events.map(item => item.space_id)
        this.setState({events: events, space_ids: spaces})
        if (json.data.group_photo === "") {
          // Get the space for the first event
          let space_id = jsonEvents.data[0].space_id
          // Get the space instance so we can retrieve the photo references
          API.getSpace(space_id).then(jsonSpace => {
            let photo_num = Math.floor(Math.random() * 10);
            let photo = JSON.parse(jsonSpace.data.photos[photo_num]);
            let photo_url = API.buildSpaceImageURL(space_id, photo['photo_reference']);
            this.setState({photo_url: photo_url});
          });
        } else {
          // If there is a group photo then use it
          this.setState({photo_url: json.data.group_photo});
        }
      });
    })
  }

  render() {
    return (<div className='pageColumn'>
      <div className='pageContent'>
        <CardMedia style={{
            height: '275px',
            marginTop: "55px",
            borderRadius: "25px"
          }} image={this.state.photo_url} title=""/>
        <div>
          <Typography id='groupName' variant="h5" className='instanceTitle'>
            {this.state.data.name}
          </Typography>
          <Typography variant="subtitle1" className='instanceSubtext'>
            <PlaceIcon style={styles.icon}/>{this.state.data.city}
          </Typography>
          <Typography variant="subtitle1" className='instanceSubtext'>
            <GroupIcon style={styles.icon}/>{this.state.data.member_count} members
          </Typography>
        </div>
        <div style={styles.description}>
          <Typography variant="h6">About</Typography>
          <Typography variant="body1" className='instanceSubtitle'>
            {this.state.data.description}
          </Typography>
        </div>
        {
          this.state.data.organizer_name !== '' && <div style={styles.description}>
              <Typography variant="h6">Group Organizer</Typography>
              <GroupOrganizerCard data={this.state.data}/>
            </div>
        }
        <div style={styles.description}>
          <Typography variant="h6">Upcoming Events</Typography>
          <Grid container="container" spacing={16}>
            {this.state.events.map(item => <Grid item="item" xs={12} sm={6} key={item.event_id}><EventCard id='groupEventCard' item={item} key={item.event_id}/></Grid>)}
          </Grid>
          <div style={styles.description}>
            <Typography variant="h6">You can find us here</Typography>
            <Grid container="container" spacing={16}>
              {this.state.space_ids.map(item => <Grid item="item" xs={12} sm={6} key={item}><CardContainer name='groupSpaceCard' type="space" uid={item}/></Grid>)}
            </Grid>
          </div>
        </div>
      </div>
    </div>);
  }
}

export default GroupDetail;
