import React, {Component} from 'react';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import AboutCard from '../components/about/AboutCard.js'
import AboutDataCard from '../components/about/AboutDataCard.js'
import axios from 'axios';

const styles = {
  media: {
    height: 600,
    width: 300
  }
};

class About extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // TODO: fill in image, bio, role
      reedham1: {
        name: 'Reed Hamilton',
        bio: " Reed Hamilton is a senior finishing up his degree in computer science with a minor in business. He enjoys music, sports, and the great outdoors. Upon graduation, he will be staying in Austin working for RetailMeNot.",
        role: " API Designer/Developer, React Developer, THE Postman, Backend Tester",
        commits: 0,
        issues: 0,
        unitTests: 53
      },
      SeaOeid: {
        name: 'Coleman Oei',
        bio: " Coleman is a senior Computer Science major from San Antonio, TX. He enjoys running and ultimate.",
        role: " UX Designer, React Developer, JavaScript Tester, API Developer, The JESTer",
        commits: 0,
        issues: 0,
        unitTests: 25
      },
      joswiatek: {
        name: 'Joel Swiatek',
        bio: " Joel is a senior Computer Science major from Allen, TX. He enjoys running and camping. He will be working for Salesforce after graduation.",
        role: " Cloud Architect, Devops Manager, React Developer, Data Engineer, Backend Tester",
        commits: 0,
        issues: 0,
        unitTests: 25
      },
      lindseythompson: {
        name: 'Lindsey Thompson',
        bio: " Lindsey is a senior Computer Science major from Austin, TX. She enjoys traveling, yoga and not running.",
        role: " UX Designer, React Developer, CSS Stylist",
        commits: 0,
        issues: 0,
        unitTests: 27
      },
      yenchen21: {
        name: 'Yen Chen Wee',
        bio: " Yen is a senior Computer Science Major with a minor in Business from Austin, TX. He enjoys sports, being forced to run by Joel and occasionally playing some squash with the boys.",
        role: " Data Engineer, React Developer, Database Admin, GUI Tester, API Developer",
        commits: 0,
        issues: 0,
        unitTests: 28
      },

      total: {
        commits: 0,
        issues: 0,
        unitTests: 0
      }
    };
  }

  componentDidMount() {
    let currentComponent = this;
    const usernames = [
      [
        'reedham1', '3441598', 'Reed Hamilton'
      ],
      [
        'SeaOeid', '3523372', 'Coleman Oei'
      ],
      [
        'joswiatek', '3409480', 'Joel Swiatek'
      ],
      [
        'lindseythompson', '3512406', 'Lindsey Thompson'
      ],
      [
        'yenchen21', '3495519', 'Yen Chen Wee'
      ]
    ];
    var totalTests = 0;
    for (var i = 0; i < usernames.length; i++) {
      totalTests += this.state[usernames[i][0]].unitTests;
    }
    let currTotalState = this.state.total;
    currTotalState.unitTests = totalTests;
    currentComponent.setState(currTotalState);

    // Get the issues
    usernames.forEach(function(user) {
      // Get the number of issues by using the x-total header
      axios.get('https://gitlab.com/api/v4/projects/10985915/issues?per_page=1&assignee_id=' + user[1]).then(res => {
        let numIssues = Number(res.headers['x-total']);
        let currentUserState = currentComponent.state[user[0]];
        currentUserState.issues = numIssues;

        let currentTotalState = currentComponent.state.total;
        currentTotalState.issues += numIssues;

        let issueUpdate = {};
        issueUpdate[user[0]] = currentUserState;
        issueUpdate.total = currentTotalState;

        currentComponent.setState({issueUpdate});
      });
    });

    // Get the number of commits
    axios.get('https://gitlab.com/api/v4/projects/10985915/repository/contributors?per_page=10').then(res => {
      res.data.forEach(contributor => {
        let groupMember = '';
        switch (contributor.name) {
          case usernames[0][0]:
          case usernames[0][2]:
            groupMember = usernames[0][0];
            break;
          case usernames[1][0]:
          case usernames[1][2]:
            groupMember = usernames[1][0];
            break;
          case usernames[2][0]:
          case usernames[2][2]:
            groupMember = usernames[2][0];
            break;
          case usernames[3][0]:
          case usernames[3][2]:
            groupMember = usernames[3][0];
            break;
          case usernames[4][0]:
          case usernames[4][2]:
            groupMember = usernames[4][0];
            break;
          default:
            console.error('Received an unknown recognized contributor: ' + contributor.name + ' ' + contributor.email);
        }
        let currentUserState = currentComponent.state[groupMember]
        currentUserState.commits = contributor.commits;

        let currentTotalState = currentComponent.state.total;
        currentTotalState.commits += contributor.commits;

        let commitUpdate = {};
        commitUpdate[groupMember] = currentUserState;
        commitUpdate.total = currentTotalState;

        currentComponent.setState({commitUpdate});
      });
    });
  }

  render() {
    return (<div className='pageColumn'>
      <div className='page-body'>
        <Typography variant="h4" align='left'>About</Typography>
        <Typography variant="body1" className='about_text' align='left'>
          The goal of this project is to combat laziness and loneliness in Austin by identifying opportunities for engagement and connection with local health and fitness communities. We realize that Austin has many local parks, but it’s difficult to find communities and activities that utilize those spaces. We will provide information about health and fitness communities, outdoor events, and green spaces in Austin. We hope that by increasing access and awareness to local events, we can help people get outdoors and connect with people around them while promoting active lifestyles.
        </Typography>
      </div>
      <div className='about_content' style={{
          width: '80%'
        }}>
        <Grid container="container" spacing={40} justify='center'>
          <Grid item="item">
            <Card style={styles.media}>
              <CardContent>
                <Typography component='h1' variant='h6' style={{
                    'fontSize' : '40',
                    'marginBottom' : '20px'
                  }} align='left'>
                  <strong>Meet the Team</strong>
                </Typography>
                <Typography component='p' variant='title' style={{
                    'fontSize' : '36',
                    'marginBottom' : '20px'
                  }} align='left'>
                  We're all CS majors from the University of Texas that enjoy Austin and the outdoors.
                </Typography>
                <Typography component='h1' variant='h6' style={{
                    'fontSize' : '40',
                    'marginBottom' : '10px'
                  }} align='left'>
                  <strong>
                    Links:
                  </strong>
                  <br></br>
                  <a href="https://gitlab.com/joswiatek/catch-me-outside" style={{
                      color: '#1BC86C',
                      'textAlign' : 'left',
                      'textDecoration' : 'none'
                    }}>
                    <strong>GitLab Repo</strong>
                  </a>
                  <br></br>
                  <a href="https://documenter.getpostman.com/view/6755734/S1ETPw57#214b8243-a7eb-4ef1-a8db-7358d9ef2666" style={{
                      color: '#1BC86C',
                      'textAlign' : 'left',
                      'textDecoration' : 'none',
                      'fontSize' : '10'
                    }}>
                    <strong>Postman API</strong>
                  </a>
                </Typography>
                <Typography component='h1' variant='h5' style={{
                    'fontSize' : '40',
                    'marginBottom' : '10px'
                  }} align='left'>
                  <strong>
                    Totals:
                  </strong>
                  <br></br>
                </Typography>
                <Typography component='h1' variant='h6' style={{
                    'fontSize' : '40',
                    'marginBottom' : '10px'
                  }} align='left'>
                  <strong>
                    Commits: {this.state.total.commits}</strong>
                  <br></br>
                  <strong>
                    Issues: {this.state.total.issues}</strong>
                  <br></br>
                  <strong>
                    Unit Tests: {this.state.total.unitTests}</strong>
                  <br></br>
                </Typography>
              </CardContent>
            </Card>
          </Grid>
          <Grid item="item">
            <AboutCard photo={require('../assets/bio_assets/reed.jpg')} name='Reed Hamilton' bio={this.state['reedham1']['bio']} role={this.state['reedham1']['role']} link="https://www.linkedin.com/in/reed-hamilton/" commits={' ' + this.state['reedham1']['commits']} issues={' ' + this.state['reedham1']['issues']} unitTests={' ' + this.state['reedham1']['unitTests']}></AboutCard>
          </Grid>
          <Grid item="item">
            <AboutCard photo={require('../assets/bio_assets/coleman.jpg')} name={this.state['SeaOeid']['name']} bio={this.state['SeaOeid']['bio']} role={this.state['SeaOeid']['role']} link="https://www.linkedin.com/in/coleman-oei-58b5277b/" commits={' ' + this.state['SeaOeid']['commits']} issues={' ' + this.state['SeaOeid']['issues']} unitTests={' ' + this.state['SeaOeid']['unitTests']}></AboutCard>
          </Grid>
          <Grid item="item">
            <AboutCard photo={require('../assets/bio_assets/joel.png')} name='Joel Swiatek' bio={this.state['joswiatek']['bio']} role={this.state['joswiatek']['role']} link="https://www.linkedin.com/in/joelswiatek/" commits={' ' + this.state['joswiatek']['commits']} issues={' ' + this.state['joswiatek']['issues']} unitTests={' ' + this.state['joswiatek']['unitTests']}></AboutCard>
          </Grid>
          <Grid item="item">
            <AboutCard photo={require('../assets/bio_assets/lindsey.jpg')} name={this.state['lindseythompson']['name']} bio={this.state['lindseythompson']['bio']} link="https://www.linkedin.com/in/lindseythom/" role={this.state['lindseythompson']['role']} commits={' ' + this.state['lindseythompson']['commits']} issues={' ' + this.state['lindseythompson']['issues']} unitTests={' ' + this.state['lindseythompson']['unitTests']}></AboutCard>
          </Grid>
          <Grid item="item">
            <AboutCard photo={require('../assets/bio_assets/yen.jpg')} name={this.state['yenchen21']['name']} bio={this.state['yenchen21']['bio']} link="https://www.hudl.com/profile/3455578/Yen-Chen-Wee" role={this.state['yenchen21']['role']} commits={' ' + this.state['yenchen21']['commits']} issues={' ' + this.state['yenchen21']['issues']} unitTests={' ' + this.state['yenchen21']['unitTests']}></AboutCard>
          </Grid>
        </Grid>
      </div>
      <div className='pageContent'>
        <h2 className='about_header' align='left' style={{
            'marginTop' : '200px',
            'marginBottom' : '5px'
          }}>
          Data
        </h2>
        <Grid container="container" spacing={40}>
          <Grid item="item">
            <AboutDataCard name='OpenWeather' link='https://openweathermap.org/' photo={require('../assets/tool_assets/openweather.png')} alt_text='OpenWeather Logo' description="OpenWeather is used to collect weather data in a particular location"></AboutDataCard>
          </Grid>
          <Grid item="item">
            <AboutDataCard name='MeetUp' link='https://www.meetup.com/meetup_api/' photo={require('../assets/tool_assets/meetup.png')} alt_text='MeetUp Logo' description="Meetup is used to collect information about outdoor events and groups in a particular location"></AboutDataCard>
          </Grid>
          <Grid item="item">
            <AboutDataCard name='Google Places' link='https://developers.google.com/places/web-service/intro' photo={require('../assets/tool_assets/googleplaces.png')} alt_text='Google Places Logo' description="Google Places is used to find places (outdoor areas of interest) near the user."></AboutDataCard>
          </Grid>
        </Grid>
      </div>
      <div className='pageContent'>
        <h2 className='about_header' align='left' style={{
            'marginTop' : '40px',
            'marginBottom' : '5px'
          }}>
          Tools
        </h2>
        <Grid container="container" spacing={40}>
          <Grid item="item">
            <AboutDataCard name='React' link='https://reactjs.org/' photo={require('../assets/tool_assets/react.png')} alt_text='React Logo' description="JavaScript Library for creating user interfaces."></AboutDataCard>
          </Grid>
          <Grid item="item">
            <AboutDataCard name='Material-UI' link='https://material-ui.com/' photo={require('../assets/tool_assets/materialui.svg')} alt_text='Material-UI Logo' description="React user interface library that features React components that implement Google's Material Design"></AboutDataCard>
          </Grid>
          <Grid item="item">
            <AboutDataCard name='Figma' link='https://www.figma.com/' photo={require('../assets/tool_assets/figma.png')} alt_text='Figma Logo' description="User interface design tool used to mock up and prototype user interfaces."></AboutDataCard>
          </Grid>
          <Grid item="item">
            <AboutDataCard name='Slack' link='https://slack.com/' photo={require('../assets/tool_assets/slack.png')} alt_text='Slack Logo' description="Team collaboration tool used to organize workflow and facilitate project discussion."></AboutDataCard>
          </Grid>
          <Grid item="item">
            <AboutDataCard name='Postman' link='https://www.getpostman.com/' photo={require('../assets/tool_assets/postman.png')} alt_text='Postman Logo' description="API development tool used to test and design APIs."></AboutDataCard>
          </Grid>
          <Grid item="item">
            <AboutDataCard name='Amazon Web Services' link='https://aws.amazon.com/' photo={require('../assets/tool_assets/aws.png')} alt_text='Amazon Web Services Logo' description="Secure cloud computing services platform."></AboutDataCard>
          </Grid>
          <Grid item="item">
            <AboutDataCard name='Mocha' link='https://mochajs.org/' photo={require('../assets/tool_assets/mocha.png')} alt_text='Mocha Logo' description="JavaScript testing framework."></AboutDataCard>
          </Grid>
          <Grid item="item">
            <AboutDataCard name='Selenium' link='https://www.seleniumhq.org/' photo={require('../assets/tool_assets/selenium.png')} alt_text='Selenium Logo' description="Automated web browser testing."></AboutDataCard>
          </Grid>
          <Grid item="item">
            <AboutDataCard name='Flask' link='http://flask.pocoo.org/' photo={require('../assets/tool_assets/flask.png')} alt_text='Flask Logo' description="Python web development framework."></AboutDataCard>
          </Grid>
          <Grid item="item">
            <AboutDataCard name='SQLAlchemy' link='https://www.sqlalchemy.org/' photo={require('../assets/tool_assets/sqlalchemy.png')} alt_text='SQLAlchemy Logo' description="Python SQL toolkit and Object Relational Mapper."></AboutDataCard>
          </Grid>
          <Grid item="item">
            <AboutDataCard name='Docker' link='https://www.docker.com/' photo={require('../assets/tool_assets/docker.png')} alt_text='Docker Logo' description="Containerization of back-end server."></AboutDataCard>
          </Grid>
          <Grid item="item">
            <AboutDataCard name='PostgreSQL' link='https://www.postgresql.org/' photo={require('../assets/tool_assets/postgre.png')} alt_text='PostgreSQL Logo' description="Open-source relational database used with Amazon RDS."></AboutDataCard>
          </Grid>
          <Grid item="item">
            <AboutDataCard name='Whoosh' link='https://bitbucket.org/mchaput/whoosh/wiki/Home' photo={require('../assets/tool_assets/whoosh.png')} alt_text='Whoosh Logo' description="Fast, featureful full-text indexing and searching library."></AboutDataCard>
          </Grid>
          <Grid item="item">
            <AboutDataCard name='D3' link='https://d3js.org/' photo={require('../assets/tool_assets/d3.png')} alt_text='D3 Logo' description="Javascript library for creating data visualization."></AboutDataCard>
          </Grid>
        </Grid>

      </div>
      <div className='pageContent'>
        <h2 className='about_header' align='left' style={{
            'marginTop' : '40px',
            'marginBottom' : '5px'
          }}>
          Commit History Visualization using Gource
        </h2>
          <div>
            <video width="800" height="auto" controls>
              <source src="https://s3.amazonaws.com/catch-me-outside-assets/gource/gource-video.mp4" type="video/mp4"/>
              Your browser does not support the video tag.
            </video>
          </div>
      </div>
    </div>);
  }
}

export default About;
