import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import './App.css';

// custom component imports
import Splash from './pages/Splash.js';
import Visuals from './pages/Visuals.js';
import Events from './pages/Events.js';
import Spaces from './pages/Spaces.js';
import Groups from './pages/Groups.js';
import About from './pages/About.js';
import SpaceInstance from './pages/SpaceInstance.js';
import EventInstance from './pages/EventInstance.js';
import GroupDetail from './pages/GroupDetail.js';
import GlobalSearchResults from './pages/GlobalSearchResults.js';

import Navbar from './components/Navbar.js';
import Footer from './components/Footer.js';

import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';

import {geolocated} from 'react-geolocated';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userLat: 0,
      userLng: 0
    };
  }

  // runs when async call to get location is reuturned
  componentWillReceiveProps = (nextProps) => {
    if (this.state.userLng === 0 || this.state.userLat === 0) {
      if (nextProps.hasOwnProperty('coords') && nextProps.coords !== null) {
        if (nextProps.coords.latitude !== null && nextProps.coords.longitude !== null) {
          this.setState({userLat: nextProps.coords.latitude, userLng: nextProps.coords.longitude})
        }
      } else if (nextProps.hasOwnProperty('userLat') && nextProps.hasOwnProperty('userLng')) {
        if (nextProps.userLat !== 0 && nextProps.userLng !== 0) {
          this.setState({userLat: nextProps.userLat, userLng: nextProps.userLng})
        }
      }
    }
  }

  render() {
    return (<MuiThemeProvider theme={theme}>
      <div className="App">
        <header>
          <Navbar/>
          <br/><br/><br/>
        </header>
        <Route path="/" exact="exact" component={Splash}/>
        <Route path="/visuals" exact="exact" render={(props) => <Visuals {...props} userLat={this.state.userLat} userLng={this.state.userLng}/>}/>
        <Route path="/events" exact="exact" render={(props) => <Events {...props} userLat={this.state.userLat} userLng={this.state.userLng}/>}/>
        <Route path="/groups" exact="exact" component={Groups}/>
        <Route path="/spaces" exact="exact" render={(props) => <Spaces {...props} userLat={this.state.userLat} userLng={this.state.userLng}/>}/>
        <Route path="/about" component={About}/>
        <Route path="/spaceInstance" component={SpaceInstance}/>
        <Route path="/events/:key" component={EventInstance}/>
        <Route path="/groups/:id" component={GroupDetail}/>
        <Route path="/spaces/:id" component={SpaceInstance}/>
        <Route path="/search" component={GlobalSearchResults}/>
      </div>
      <Footer/>
    </MuiThemeProvider>);
  }
}

// custom Global styles for Material UI
const theme = createMuiTheme({
  typography: {
    // Use the system font instead of the default Roboto font.
    fontFamily: 'Avenir, Trebuchet, sans-serif',
    h2: {
      color: 'white',
      fontWeight: 'bold',
      paddingTop: 48,
      paddingBottom: 48
    },
    h4: {
      color: '#1BC86C',
      fontWeight: 'bold',
      paddingBottom: 24
    },
    h5: {
      color: '#494B4E',
      fontWeight: 'bold'
    },
    h6: {
      color: '#494B4E',
      fontWeight: '600',
      paddingBottom: 15,
      textAlign: 'left'
    }
  },
  palette: {
    primary: {
      main: '#1BC86C',
      light: '#1BC86C',
      dark: '#1BC86C'
    },
    secondary: {
      main: '#fff'
    }
  },
  overrides: {
    MuiPickersToolbar: {
      toolbar: {
        backgroundColor: '#1BC86C'
      }
    },
    MuiPickersDay: {
      day: {
        color: '#494B4E'
      },
      isSelected: {
        backgroundColor: '#1BC86C'
      },
      current: {
        color: '#1BC86C'
      }
    },
    MuiPickersModal: {
      dialogAction: {
        color: '#494B4E'
      }
    }
  }
});

export default geolocated({
  positionOptions: {
    enableHighAccuracy: false
  },
  userDecisionTimeout: 5000
})(App);
