import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme, { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'jest-canvas-mock';

import API from './config/api';
import {getHighlightWords, getSnippets, calcDistance} from './config/Helper.js'
import AboutCard from './components/about/AboutCard.js';
import AboutDataCard from './components/about/AboutDataCard.js';
import CardContainer from './components/CardContainer.js';
import DateDisplay from './components/event/DateDisplay.js';
import EventCard from './components/event/EventCard.js';
import EventSummary from './components/event/EventSummary.js';
import Footer from './components/Footer.js';
import GroupCard from './components/group/GroupCard.js';
import Navbar from './components/Navbar.js';
import ReviewCard from './components/review/ReviewCard.js';
import Reviews from './components/review/Reviews.js';
import ScrollToTop from './components/ScrollToTop.js';
import SpaceCard from './components/space/SpaceCard.js';
import SplashCard from './components/SplashCard.js';
import WeatherDisplay from './components/space/WeatherDisplay.js';

import About from './pages/About.js';
import EventInstance from './pages/EventInstance.js';
import Events from './pages/Events.js';
import GroupDetail from './pages/GroupDetail.js';
import Groups from './pages/Groups.js';
import SpaceInstance from './pages/SpaceInstance.js';
import Spaces from './pages/Spaces.js';
import Splash from './pages/Splash.js';

Enzyme.configure({ adapter: new Adapter() });

describe('getSnippets', function() {
  describe('getSnippets(queryList, text)', function() {
    it('regular test case. return short description with sentences from word passed in (hike)', function() {
      expect(getSnippets(['hike'], 'Join us for this weeks Community Hike! Thursday morning hike around Stage Coach Park. We will meet at The Budaful Hiker at 7:55am and begin the hike at 8. We will hike to and around Stage Coach Park and back to the shop. This is a 3 mile beginner hike on paved or gravel paths. be sure to wear comfortable, supportive shoes and bring water. This hike is open to the community. See you Thursday morning! Hike is from 8-9am and Breakfast is 9-10am *Anyone who cares to join, we will grab some breakfast after at Helen’s Casa Alda*')).toBe("Join us for this weeks Community Hike... Thursday morning hike around Stage Coach Park... We will meet at The Budaful Hiker at 7:55am and begin the hike at 8");
    });
  });
  describe('getSnippets(queryList, text)', function() {
    it('case where there are no words passed in', function() {
      expect(getSnippets([], 'Join us for this weeks Community Hike! Thursday morning hike around Stage Coach Park. We will meet at The Budaful Hiker at 7:55am and begin the hike at 8. We will hike to and around Stage Coach Park and back to the shop. This is a 3 mile beginner hike on paved or gravel paths. be sure to wear comfortable, supportive shoes and bring water. This hike is open to the community. See you Thursday morning! Hike is from 8-9am and Breakfast is 9-10am *Anyone who cares to join, we will grab some breakfast after at Helen’s Casa Alda*')).toBe("Join us for this weeks Community Hike... See you Thursday morning");
    });
  });
});

describe('getHighlightWords', function() {
  describe('getHighlightWords = (queryList, text)', function() {
    it('regular case', function() {
      expect(getHighlightWords(['hike'], 'going for a hike')).toEqual(['hike']);
    });
  });
  describe('getHighlightWords = (queryList, text)', function() {
    it('case where there is a conjigation', function() {
      expect(getHighlightWords(['hike'], 'Onion Creek and Homestead Trail in McKinney Falls State Park is a 6.5 mile loop trail that features a waterfall. The trail is good for all skill levels. Please wear comfortable shoes suitable for hiking. Dogs are welcome! The park entry costs $6 per adult.')).toEqual(['hiking']);
    });
  });
  describe('getHighlightWords = (queryList, text)', function() {
    it('punctuation case', function() {
      expect(getHighlightWords(['hike'], 'going for a hike!')).toEqual(['hike']);
    });
  });
});

describe('getHighlightWords', function() {
  describe('calcDistance = (lat1, lon1, lat2, lon2)', function() {
    it('regular case', function() {
      expect(calcDistance(32.9697, -96.80322, 29.46786, -98.53506)).toBe(262.6777938054349);
    });
  });
});

describe('getEvent(id)', function() {
  it('checking status of return', async() => {
    const ret = await API.getEvent('260689530')
    await expect(ret).toHaveProperty('status', 200);
  });
  it('checking event id', async() => {
    const ret = await API.getEvent('260689530')
    await expect(ret.data).toHaveProperty('event_id', '260689530');
  });
  it('checking event name', async() => {
    const ret = await API.getEvent('260689530')
    await expect(ret.data).toHaveProperty('name', 'Ride Like A Girl - Southie Trails & Post Ride Party');
  });
});

describe('getSpace(id)', function() {
  it('checking status of return', async() => {
    const ret = await API.getSpace('ChIJSfh_QgW1RIYRMGJ2BEpGG1c')
    await expect(ret).toHaveProperty('status', 200);
  });
  it('checking space id', async() => {
    const ret = await API.getSpace('ChIJSfh_QgW1RIYRMGJ2BEpGG1c')
    await expect(ret.data).toHaveProperty('space_id', 'ChIJSfh_QgW1RIYRMGJ2BEpGG1c');
  });
  it('checking space name', async() => {
    const ret = await API.getSpace('ChIJSfh_QgW1RIYRMGJ2BEpGG1c')
    await expect(ret.data).toHaveProperty('name', 'Auditorium Shores at Town Lake Metropolitan Park');
  });
});

describe('getGroup(id)', function() {
  it('checking status of return', async() => {
    const ret = await API.getGroup('428572')
    await expect(ret).toHaveProperty('status', 200);
  });
  it('checking group id', async() => {
    const ret = await API.getGroup('428572')
    await expect(ret.data).toHaveProperty('group_id', '428572');
  });
  it('checking group name', async() => {
    const ret = await API.getGroup('428572')
    await expect(ret.data).toHaveProperty('name', 'Mosaic Jewish Outdoor Club of Central Texas');
  });
});

describe('getWeather(id)', function() {
  it('checking status of return', async() => {
    const ret = await API.getWeather('ChIJSfh_QgW1RIYRMGJ2BEpGG1c')
    await expect(ret).toHaveProperty('status', 200);
  });
});

describe('getReviews(id)', function() {
  it('checking status of return', async() => {
    const ret = await API.getReviews('ChIJSfh_QgW1RIYRMGJ2BEpGG1c')
    await expect(ret).toHaveProperty('status', 200);
  });
});

describe('getSpaces(pageNum)', function() {
  it('checking status of return', async() => {
    const ret = await API.getSpaces('1')
    await expect(ret).toHaveProperty('status', 200);
  });
});

describe('getGroups(pageNum)', function() {
  it('checking status of return', async() => {
    const ret = await API.getGroups('1')
    await expect(ret).toHaveProperty('status', 200);
  });
});

describe('getEvents(pageNum)', function() {
  it('checking status of return', async() => {
    const ret = await API.getEvents('1')
    await expect(ret).toHaveProperty('status', 200);
  });
});

describe('getEventsForGroup(id)', function() {
  it('checking status of return', async() => {
    const ret = await API.getEventsForGroup('428572')
    await expect(ret).toHaveProperty('status', 200);
  });
});

describe('getEventsForSpace(id)', function() {
  it('checking status of return', async() => {
    const ret = await API.getEventsForSpace('ChIJSfh_QgW1RIYRMGJ2BEpGG1c')
    await expect(ret).toHaveProperty('status', 200);
  });
});

describe('Function Tests', function() {
  describe('buildSpaceImageURL(id, photoref)', function() {
    it('should build a url to an S3 photo', function() {
      expect(API.buildSpaceImageURL('testID', 'testPhotoRef')).toBe('https://s3.amazonaws.com/catch-me-outside-assets/google-places-photos/testID/testPhotoRef');
    });
  });
});

describe('Component Tests', function() {
  describe('<AboutCard>', () => {
    it('component renders', () => {
      const wrapper = shallow(<AboutCard />);
      expect(wrapper.exists()).toBe(true);
    });
    it('setting date', () => {
      const container = shallow(<AboutCard date='2019-03-29'/>);
      expect(container.prop('date')).toBe("2019-03-29");
    });
    it('can create', () => {
     const container = shallow(<AboutCard date='2019-03-29'/>);
     expect(Object.keys(container.prop('classes'))).toHaveLength(4);
    });
  });

  describe('<AboutDataCard>', () => {
    it('component renders', () => {
      const wrapper = shallow(<AboutDataCard />);
      expect(wrapper.exists()).toBe(true);
    });
    it('can set title', () => {
      const container = shallow(<AboutDataCard title='test title'/>);
      expect(container.prop('title')).toBe('test title');
    });
    it('can create with props', () => {
      const container = shallow(<AboutDataCard />);
      expect(Object.keys(container.prop('classes'))).toHaveLength(6);
    });
  });

  describe('<CardContainer>', () => {
    it('component renders', () => {
      const wrapper = shallow(<CardContainer />);
      expect(wrapper.exists()).toBe(true);
    });
  });

  describe('<DateDisplay>', () => {
    it('component renders', () => {
      const wrapper = shallow(<DateDisplay />);
      expect(wrapper.exists()).toBe(true);
    });
  });

  describe('<EventCard>', () => {
    it('component renders', () => {
      const wrapper = shallow(<EventCard />);
      expect(wrapper.exists()).toBe(true);
    });
  });

  describe('<EventSummary>', () => {
    it('component renders', () => {
      const wrapper = shallow(<EventSummary />);
      expect(wrapper.exists()).toBe(true);
    });
    it('can set title', () => {
      const container = shallow(<EventSummary title='test title'/>);
      expect(container.prop('title')).toBe('test title');
    });
    it('can create with props', () => {
      const container = shallow(<EventSummary />);
      expect(Object.keys(container.prop('classes'))).toHaveLength(2); // recieves "card","media"
    });
  });

  describe('<Footer>', () => {
    it('component renders', () => {
      const wrapper = shallow(<Footer />);
      expect(wrapper.exists()).toBe(true);
    });
  });

  describe('<GroupCard>', () => {
    it('component renders', () => {
      const wrapper = shallow(<GroupCard />);
      expect(wrapper.exists()).toBe(true);
    });
  });

  describe('<Navbar>', () => {
    it('component renders', () => {
      const wrapper = shallow(<Navbar />);
      expect(wrapper.exists()).toBe(true);
    });
  });

  describe('<ReviewCard>', () => {
    it('component renders', () => {
      const wrapper = shallow(<ReviewCard />);
      expect(wrapper.exists()).toBe(true);
    });
    it('can create with props', () => {
      const container = shallow(<ReviewCard />);
      expect(Object.keys(container.prop('classes'))).toHaveLength(2);
    });
  });

  describe('<Reviews>', () => {
    it('component renders', () => {
      const wrapper = shallow(<Reviews />);
      expect(wrapper.exists()).toBe(true);
    });
  });

  describe('<ScrollToTop>', () => {
    it('component renders', () => {
      const wrapper = shallow(<ScrollToTop />);
      expect(wrapper.exists()).toBe(true);
    });
  });

  describe('<SpaceCard>', () => {
    it('component renders', () => {
      const wrapper = shallow(<SpaceCard />);
      expect(wrapper.exists()).toBe(true);
    });
    it('can create with props', () => {
      const container = shallow(<SpaceCard />);
      expect(Object.keys(container.prop('classes'))).toHaveLength(3);
    });
  });

  describe('<SplashCard>', () => {
    it('component renders', () => {
      const wrapper = shallow(<SplashCard />);
      expect(wrapper.exists()).toBe(true);
    });
    it('can create with props', () => {
      const container = shallow(<SplashCard />);
      expect(Object.keys(container.prop('classes'))).toHaveLength(2);
    });
  });

  describe('<WeatherDisplay>', () => {
    it('component renders', () => {
      const wrapper = shallow(<WeatherDisplay />);
      expect(wrapper.exists()).toBe(true);
    });
  });
});

describe('Page Tests', function() {
  describe('<About>', () => {
    it('component renders', () => {
      const wrapper = shallow(<About />);
      expect(wrapper.exists()).toBe(true);
    });
  });

  //TODO: fix geolocation provider in this test
  /*
  describe('<Events>', () => {
    it('component renders', () => {
      const wrapper = shallow(<Events userLat={30.2828681} userLng={-97.7380372}/>);
      expect(wrapper.exists()).toBe(true);
    });
  });
  */

  describe('<Groups>', () => {
    it('component renders', () => {
      const wrapper = shallow(<Groups />);
      expect(wrapper.exists()).toBe(true);
    });
  });

  // TODO: fix the geolocation
  /*
  describe('<Spaces>', () => {
    it('component renders', () => {
      const wrapper = shallow(<Spaces />);
      expect(wrapper.exists()).toBe(true);
    });
  });
  */

  // describe('<Splash>', () => {
  //   it('component renders', () => {
  //     const wrapper = shallow(<Splash />);
  //     expect(wrapper.exists()).toBe(true);
  //   });
  // });
});
