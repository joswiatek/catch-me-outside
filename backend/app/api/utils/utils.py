from flask import Flask, abort
from flask_sqlalchemy import Pagination
import re
import math
import arrow
from datetime import datetime

TIME_REGEX = "^\w*: ((1[0-2]|0?[1-9]):([0-5][0-9]) ([AaPp][Mm])) *. ((1[0-2]|0?[1-9]):([0-5][0-9]) ([AaPp][Mm]))"


################################################################################
### Query Helper Functions
################################################################################


def paginate(query, page, per_page=20, error_out=True):
    """ Paginate function for db_session queries. Taken from Flask-SQLAlchemy's
    paginate implementation for the `BaseQuery` class."""
    if error_out and page < 1:
        abort(404)
    results = query.limit(per_page).offset((page - 1) * per_page).all()
    if not results and page != 1 and error_out:
        abort(404)

    if page == 1 and len(results) < per_page:
        total = len(results)
    else:
        total = query.order_by(None).count()
    return Pagination(query, page, per_page, total, results)


def first_or_404(query):
    """ Returns first result from query or aborts with a 404 status if unsuccessful.
    Behaves identically to the `first_or_404` function found in `BaseQuery`.
    """
    result = query.first()
    if result is None:
        abort(404)
    return result


def get_or_404(query, id):
    """ Returns a result by id from query or aborts with a 404 status if unsuccessful.
    Behaves identically to the `get_or_404` function found in `BaseQuery`.
    """
    result = query.get(id)
    if result is None:
        abort(404)
    return result


################################################################################
### Other Helper Functions
################################################################################


def isFloat(element):
    if re.match("^\d+?\.\d+?$", element) is None:
        return False
    else:
        return True


def time_is_in_range(hours, user_query_time):
    """ Helper function that returns `True` if `user_query_time` is within the
    range `hours`. """
    hour_string = hours[0]
    try:
        hour_string = hour_string[user_query_time.weekday()]
    except IndexError:
        # no hours included for the weekday passed in so return false
        return False
    m = re.search(TIME_REGEX, hour_string)
    if m == None:
        # Special case if no match. Either 'Open 24 hours' or 'closed'
        m = re.search("^\w*: Open 24 hours", hour_string)
        if m == None:
            return False
        else:
            return True
    start_time_string = m.group(1)
    start_time = arrow.get(start_time_string, "H:mm A").time()
    end_time_string = m.group(5)
    end_time = arrow.get(end_time_string, "H:mm A").time()
    return start_time <= user_query_time.time() <= end_time


def find_dist(lat1, long1, lat2, long2):
    """ Helper function that returns the distance between two lat/lng pairs. Uses
    the Haversine formula. """
    if lat1 == lat2 and long1 == long2:
        return 0
    else:
        radlat1 = math.pi * float(lat1) / 180
        radlat2 = math.pi * float(lat2) / 180
        theta = float(long1) - float(long2)
        radtheta = math.pi * theta / 180
        dist = math.sin(radlat1) * math.sin(radlat2) + math.cos(radlat1) * math.cos(
            radlat2
        ) * math.cos(radtheta)
        if dist > 1:
            dist = 1
        dist = math.acos(dist)
        dist = dist * 180 / math.pi
        dist = dist * 60 * 1.1515
        return dist
