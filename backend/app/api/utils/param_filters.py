from flask import Flask, request, abort
from api.utils import utils
from sqlalchemy.sql.expression import or_
import re

# Regex expression to parse lat/lng
LATLNG_REGEX = "^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$"

# Supported query parameters
space_sort_params = {"name", "dist", "event_count", "rating"}
event_sort_params = {"name", "dist", "rsvp_count", "date"}
group_sort_params = {"name", "creation_time", "member_count", "event_count"}
event_time_filter_params = {"morning", "afternoon", "night"}


################################################################################
### Query Validation Functions
################################################################################


def filter_space_query_params(request):
    """ Validates Space query parameters."""
    new_args = {}
    # Get page for pagination. Defaults to 1
    new_args["page"] = request.args.get("page", 1, type=int)

    # default sort to rating if no sort specified
    new_args["sort"] = request.args.get("sort", "rating")
    # check sort param is valid, if not, error
    if (new_args["sort"] == "dist" and "user_loc" not in request.args) or new_args[
        "sort"
    ] not in space_sort_params:
        abort(400)

    if "order" not in request.args:
        # set default sort order if not specified by request
        sortBy = new_args["sort"]
        if sortBy == "rating":
            new_args["order"] = "desc"
        elif sortBy == "dist":
            new_args["order"] = "asc"
        elif sortBy == "event_count":
            new_args["order"] = "desc"
        elif sortBy == "name":
            new_args["order"] = "asc"
    else:
        new_args["order"] = request.args.get("order")
        # check order param is valid, if not, error
        if new_args["order"] != "desc" and new_args["order"] != "asc":
            abort(400)

    if "open_during" in request.args:
        time = request.args.get("open_during")
        if time.isdigit():
            new_args["open_during"] = request.args.get("open_during")
        else:
            abort(400)

    if "space_type" in request.args:
        new_args["space_type"] = request.args.get("space_type").split(",")

    if "min_rating" in request.args:
        if utils.isFloat(request.args.get("min_rating")):
            new_args["min_rating"] = request.args.get("min_rating")
        else:
            abort(400)

    if "max_rating" in request.args:
        if utils.isFloat(request.args.get("max_rating")):
            new_args["max_rating"] = request.args.get("max_rating")
        else:
            abort(400)

    if "user_loc" in request.args:
        if re.compile(LATLNG_REGEX).match(request.args.get("user_loc")):
            # split latlng string by comma, strip, and cast two values to floats
            split = [float(x.strip()) for x in request.args.get("user_loc").split(",")]
            new_args["user_loc"] = (split[0], split[1])
        else:
            abort(400)

    if "min_events" in request.args:
        new_args["min_events"] = int(request.args.get("min_events"))

    if "max_events" in request.args:
        new_args["max_events"] = int(request.args.get("max_events"))

    if "keyword" in request.args:
        new_args["keyword"] = request.args.get("keyword")

    return new_args


def filter_event_query_params(request):
    """ Validates Event query parameters."""
    new_args = {}
    new_args["page"] = request.args.get("page", 1, type=int)
    # default to date if no sort specified
    new_args["sort"] = request.args.get("sort", "date")
    # check sort param is valid, if not, error
    if (new_args["sort"] == "dist" and "user_loc" not in request.args) or new_args[
        "sort"
    ] not in event_sort_params:
        abort(400)

    # set default sort order if order is not specified
    if "order" not in request.args:
        sortBy = new_args["sort"]
        if sortBy == "date":
            new_args["order"] = "asc"
        elif sortBy == "dist":
            new_args["order"] = "asc"
        elif sortBy == "rsvp_count":
            new_args["order"] = "desc"
        elif sortBy == "name":
            new_args["order"] = "asc"
    else:
        # default order to asc if not specified
        new_args["order"] = request.args.get("order")
        # check order param is valid, if not, error
        if new_args["order"] != "desc" and new_args["order"] != "asc":
            abort(400)

    if "start_date" in request.args:
        new_args["start_date"] = request.args.get("start_date")

        # if both a start and end date, but end date before start date, error
        if "end_date" in request.args:
            if request.args.get("end_date") < request.args.get("start_date"):
                abort(400)

    if "end_date" in request.args:
        new_args["end_date"] = request.args.get("end_date")

    if "times_include" in request.args:
        times_include_set = set(request.args.get("times_include").lower().split(","))

        # if set of arguments are valid
        if times_include_set.issubset(event_time_filter_params):
            new_args["times_include"] = times_include_set
        else:
            abort(400)

    if "min_attendees" in request.args:
        if request.args.get("min_attendees").isdigit():
            new_args["min_attendees"] = request.args.get("min_attendees")
        else:
            abort(400)

    if "max_attendees" in request.args:
        if request.args.get("max_attendees").isdigit():
            new_args["max_attendees"] = request.args.get("max_attendees")
        else:
            abort(400)
        # if both a min and a max given, but the max < min, error
        if (
            "min_attendees" in request.args
            and request.args.get("min_attendees").isdigit()
            and int(request.args.get("min_attendees"))
            > int(request.args.get("max_attendees"))
        ):
            abort(400)

    if "category" in request.args:
        new_args["category"] = request.args.get("category").split(",")

    if "user_loc" in request.args:
        if re.compile(LATLNG_REGEX).match(request.args.get("user_loc")):
            # split latlng string by comma, strip, and cast two values to floats
            split = [float(x.strip()) for x in request.args.get("user_loc").split(",")]
            new_args["user_loc"] = (split[0], split[1])
        else:
            abort(400)

    if "space_id" in request.args:
        new_args["space_id"] = request.args.get("space_id")

    if "keyword" in request.args:
        new_args["keyword"] = request.args.get("keyword")

    return new_args


def filter_group_query_params(request):
    """ Validate Group query parameters. """
    new_args = {}

    new_args["page"] = request.args.get("page", 1, type=int)
    # default to member_count if no sort specified

    new_args["sort"] = request.args.get("sort", "member_count")

    # check sort param is valid, if not, error
    if new_args["sort"] not in group_sort_params:
        abort(400)

    # set default sort order if order not specified
    if "order" not in request.args:
        sortBy = new_args["sort"]
        if sortBy == "creation_time":
            new_args["order"] = "asc"
        elif sortBy == "member_count":
            new_args["order"] = "desc"
        elif sortBy == "event_count":
            new_args["order"] = "desc"
        elif sortBy == "name":
            new_args["order"] = "asc"
    else:
        new_args["order"] = request.args.get("order")
        # check order param is valid, if not, error
        if new_args["order"] != "desc" and new_args["order"] != "asc":
            abort(400)

    if "category" in request.args:
        new_args["category"] = request.args.get("category").split(",")

    if "join_mode" in request.args:
        join_mode = request.args.get("join_mode")
        if join_mode == "open" or join_mode == "closed" or join_mode == "approval":
            new_args["join_mode"] = join_mode
        else:
            abort(400)

    if "keyword" in request.args:
        new_args["keyword"] = request.args.get("keyword")

    if "min_member_count" in request.args:
        new_args["min_member_count"] = request.args.get("min_member_count")

        # if both min and max, check min <= max, if not, abort
        if "max_member_count" in request.args:
            if request.args.get("min_member_count") > request.args.get(
                "max_member_count"
            ):
                abort(400)

    if "max_member_count" in request.args:
        new_args["max_member_count"] = request.args.get("max_member_count")

    return new_args
