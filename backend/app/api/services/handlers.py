from flask import Flask
from api import api, models
from api.utils import utils
from api.services import sorts, filters
from sqlalchemy.exc import ArgumentError

# max instance cards per page
MAX_PER_PAGE = 12


# implements logic for get events API endpoint
def getEventsHandler(args, session):
    if "keyword" in args:
        # If searching, store event_ids of search results
        try:
            args["search_ids"] = [
                x.event_id for x in models.Event.query.whoosh_search(args["keyword"])
            ]
        except ArgumentError:
            # No results found
            args["search_ids"] = []
    if args["sort"] == "dist":
        # If sorting by distance, query associated lat/lng of each event from spaces
        query_stmt = session.query(
            models.Event, models.Space.lat, models.Space.long
        ).join(models.Space, models.Event.space_id == models.Space.space_id)
    else:
        query_stmt = session.query(models.Event)

    # Filter
    query_stmt = filters.event_filter(query_stmt, args)

    if args["sort"] == "dist":
        # Sort by distance
        result = sorts.event_dist_sort(query_stmt, args)
        # Manually paginate results
        if len(result) < MAX_PER_PAGE:
            num_pages = 1
        else:
            num_pages = int(len(result) / MAX_PER_PAGE + 1)
            result = result[
                (args["page"] - 1) * MAX_PER_PAGE : args["page"] * MAX_PER_PAGE
            ]
        total = len(result)
    else:
        # Sort events
        query_stmt = sorts.event_sort(query_stmt, args)
        # paginate
        pag = utils.paginate(query_stmt, page=args["page"], per_page=MAX_PER_PAGE)
        result = pag.items
        num_pages = pag.pages
        total = pag.total

    return result, num_pages, total


# implements logic for get groups API endpoint
def getGroupsHandler(args, session):
    query_stmt = session.query(models.Group)
    if "keyword" in args:
        try:
            # Store group_ids of search results to filter later
            args["search_ids"] = [
                x.group_id for x in models.Group.query.whoosh_search(args["keyword"])
            ]
        except ArgumentError:
            args["search_ids"] = []
    # Filter groups
    query_stmt = filters.group_filter(query_stmt, args)
    # Sort groups
    query_stmt = sorts.group_sort(query_stmt, args, session)
    # Paginate results
    pag = utils.paginate(query_stmt, args["page"], per_page=MAX_PER_PAGE)

    return pag.items, pag.pages, pag.total


# implements logic for get spaces API endpoint
def getSpacesHandler(args, session):
    if "keyword" in args:
        try:
            # Store space ids of search results to filter later
            args["search_ids"] = [
                x.space_id for x in models.Space.query.whoosh_search(args["keyword"])
            ]
        except ArgumentError:
            # No Results
            args["search_ids"] = []
    query_stmt = session.query(models.Space)
    # Filter
    query_stmt = filters.space_filter(query_stmt, args, session)
    # Sort
    if args["sort"] == "dist":
        # Sort by distance
        result = sorts.space_dist_sort(query_stmt, args)
        # Manually paginate results
        if len(result) < MAX_PER_PAGE:
            num_pages = 1
        else:
            num_pages = int(len(result) / MAX_PER_PAGE + 1)
            result = result[
                (args["page"] - 1) * MAX_PER_PAGE : args["page"] * MAX_PER_PAGE
            ]
        total = len(result)
    else:
        query_stmt = sorts.space_sort(query_stmt, args, session)
        # Paginate
        pag = utils.paginate(query_stmt, page=args["page"], per_page=MAX_PER_PAGE)
        result = pag.items
        total = pag.total
        num_pages = pag.pages

    return result, num_pages, total
