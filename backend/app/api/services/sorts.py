from flask import Flask
from api import api, models
from api.utils import utils
from sqlalchemy.sql import func
from datetime import datetime


################################################################################
### Query Sort Functions
################################################################################


def event_sort(query, args):
    """ Order Events by query parameters. """
    if args["sort"] == "date":
        if args["order"] == "asc":
            query = query.order_by(
                models.Event.local_date.asc(), models.Event.local_time.asc()
            )
        else:
            query = query.order_by(
                models.Event.local_date.desc(), models.Event.local_time.desc()
            )
    elif args["sort"] == "rsvp_count":
        if args["order"] == "asc":
            query = query.order_by(models.Event.rsvp_count.asc())
        if args["order"] == "desc":
            query = query.order_by(models.Event.rsvp_count.desc())
    elif args["sort"] == "name":
        if args["order"] == "asc":
            query = query.order_by(models.Event.name.asc())
        if args["order"] == "desc":
            query = query.order_by(models.Event.name.desc())
        pass

    return query


def event_dist_sort(query, args):
    """ Specific sort function for Events to sort by nearest distance. """
    dists = []
    event_list = query.all()
    for event in event_list:
        dists.append(
            utils.find_dist(
                float(event[1]),
                float(event[2]),
                float(args["user_loc"][0]),
                float(args["user_loc"][1]),
            )
        )

    if args["order"] == "asc":
        result = [x[0] for _, x in sorted(zip(dists, event_list), key=lambda x: x[0])]
    else:
        result = [
            x[0]
            for _, x in sorted(zip(dists, event_list), reverse=True, key=lambda x: x[0])
        ]
    return result


def group_sort(query, args, session):
    """ Order Groups by query parameters. """
    if args["sort"] == "name":
        if args["order"] == "asc":
            query = query.order_by(models.Group.name.asc())
        else:
            query = query.order_by(models.Group.name.desc())
    elif args["sort"] == "creation_time":
        if args["order"] == "asc":
            query = query.order_by(models.Group.creation_time.asc())
        else:
            query = query.order_by(models.Group.creation_time.desc())
    elif args["sort"] == "member_count":
        if args["order"] == "asc":
            query = query.order_by(models.Group.member_count.asc())
        else:
            query = query.order_by(models.Group.member_count.desc())
    elif args["sort"] == "event_count":
        if args["order"] == "asc":
            query = query.order_by(
                (
                    session.query(func.count(models.Event.event_id)).filter(
                        models.Event.group_id == models.Group.group_id
                    )
                )
                .as_scalar()
                .asc()
            )
        else:
            query = query.order_by(
                (
                    session.query(func.count(models.Event.event_id)).filter(
                        models.Event.group_id == models.Group.group_id
                    )
                )
                .as_scalar()
                .desc()
            )

    return query


def space_sort(query, args, session):
    """ Order Spaces by query parameters. """
    if args["sort"] == "name":
        if args["order"] == "asc":
            query = query.order_by(models.Space.name.asc())
        else:
            query = query.order_by(models.Space.name.desc())
    elif args["sort"] == "event_count":
        if args["order"] == "asc":
            query = query.order_by(
                (
                    session.query(func.count(models.Event.event_id)).filter(
                        models.Event.space_id == models.Space.space_id
                    )
                )
                .as_scalar()
                .asc()
            )
        else:
            query = query.order_by(
                (
                    session.query(func.count(models.Event.event_id)).filter(
                        models.Event.space_id == models.Space.space_id
                    )
                )
                .as_scalar()
                .desc()
            )
    elif args["sort"] == "rating":
        if args["order"] == "asc":
            query = query.order_by(models.Space.rating.asc())
        else:
            query = query.order_by(models.Space.rating.desc())
    return query


def space_dist_sort(query, args):
    """ Specific sort function for Spaces to sort by nearest distance. """
    dists = []
    space_list = query.all()
    for space in space_list:
        dists.append(
            utils.find_dist(
                float(space.lat),
                float(space.long),
                float(args["user_loc"][0]),
                float(args["user_loc"][1]),
            )
        )

    if args["order"] == "asc":
        result = [x[1] for x in sorted(zip(dists, space_list), key=lambda x: x[0])]
    else:
        result = [
            x[1]
            for x in sorted(zip(dists, space_list), reverse=True, key=lambda x: x[0])
        ]
    return result
