from flask import Flask
from api import api, models
from api.utils import utils
from sqlalchemy.sql import func
from sqlalchemy.sql.expression import or_
import arrow
from datetime import datetime


################################################################################
### Query Filter Functions
################################################################################


def event_filter(query, args):
    """ Filters events by query parameters. """
    if "start_date" in args:
        query = query.filter(models.Event.local_date >= args["start_date"])
    if "end_date" in args:
        query = query.filter(models.Event.local_date <= args["end_date"])
    if "times_include" in args:
        query = time_filter(query, args["times_include"])
    if "min_attendees" in args:
        query = query.filter(models.Event.rsvp_count >= args["min_attendees"])
    if "max_attendees" in args:
        query = query.filter(models.Event.rsvp_count <= args["max_attendees"])
    if "category" in args:
        query = query.filter(models.Event.category.overlap(args["category"]))
    if "space_id" in args:
        query = query.filter(models.Event.space_id == args["space_id"])
    if "search_ids" in args:
        query = query.filter(models.Event.event_id.in_(args["search_ids"]))

    return query


def group_filter(query, args):
    """ Filter Groups by query parameters. """
    if "category" in args:
        print(args["category"])
        query = query.filter(models.Group.categories.in_(args["category"]))
    if "join_mode" in args:
        query = query.filter(models.Group.join_mode == args["join_mode"])
    if "min_member_count" in args:
        query = query.filter(models.Group.member_count >= args["min_member_count"])
    if "max_member_count" in args:
        query = query.filter(models.Group.member_count <= args["max_member_count"])
    if "search_ids" in args:
        query = query.filter(models.Group.group_id.in_(args["search_ids"]))

    return query


def space_filter(query, args, session):
    """ Filter Spaces by query parameters. """
    if "space_type" in args:
        query = query.filter(models.Space.types.overlap(args["space_type"]))
    if "min_events" in args:
        query = query.filter(
            args["min_events"]
            <= (
                session.query(func.count(models.Event.event_id)).filter(
                    models.Event.space_id == models.Space.space_id
                )
            ).as_scalar()
        )
    if "max_events" in args:
        query = query.filter(
            args["max_events"]
            >= (
                session.query(func.count(models.Event.event_id)).filter(
                    models.Event.space_id == models.Space.space_id
                )
            ).as_scalar()
        )
    if "min_rating" in args:
        query = query.filter(models.Space.rating >= args["min_rating"])
    if "max_rating" in args:
        query = query.filter(models.Space.rating <= args["max_rating"])
    # getting open_during hours
    if "open_during" in args:
        db_session = models.Session()
        oh = db_session.query(models.Space.open_hours, models.Space.space_id).all()
        user_query_time = arrow.Arrow.fromtimestamp(args["open_during"])
        open_list = []
        for e in oh:
            if utils.time_is_in_range(e, user_query_time):
                open_list.append(e[1])
        query = query.filter(models.Space.space_id.in_(open_list))
    if "search_ids" in args:
        query = query.filter(models.Space.space_id.in_(args["search_ids"]))

    return query


# return ranges of times we want in results (uised by event filter)
def time_filter(query, include_set):
    times = {
        "morning": models.Event.local_time.between("03:00:00", "11:59:59"),
        "afternoon": models.Event.local_time.between("12:00:00", "17:59:59"),
        "night": or_(
            models.Event.local_time >= "18:00:00", models.Event.local_time <= "02:59:59"
        ),
    }

    include_list = list(include_set)

    # if one range to include, return that range
    if len(include_list) == 1:
        query = query.filter(times[include_list[0]])
        return query
    elif "morning" not in include_set:
        query = query.filter(or_(times["afternoon"], times["night"]))
        return query
    elif "afternoon" not in include_set:
        query = query.filter(or_(times["morning"], times["night"]))
        return query
    elif "night" not in include_set:
        query = query.filter(or_(times["morning"], times["afternoon"]))
        return query
    else:
        return None
