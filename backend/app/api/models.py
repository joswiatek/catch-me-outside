from api import api
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import sessionmaker
import flask_whooshalchemyplus as whoosh

db = SQLAlchemy(api)
Session = sessionmaker(bind=db.engine)
ma = Marshmallow(api)

# for database refection into models' python classes
# see slack shared_resources for database/object schema
db.reflect()


# database models class definitions with SQLAlchemy


class Space(db.Model):
    __tablename__ = "spaces"
    __searchable__ = ["name", "address"]


class Event(db.Model):
    __tablename__ = "events"
    __searchable__ = ["name", "description"]
    __table_args__ = {"extend_existing": True}
    space_id = db.Column(db.String)
    group_id = db.Column(db.String)


class Group(db.Model):
    __tablename__ = "groups"
    __searchable__ = ["name", "description"]


class Weather(db.Model):
    __tablename__ = "weather"
    __table_args__ = {"extend_existing": True}
    space_id = db.Column(db.String)


class Review(db.Model):
    __tablename__ = "reviews"
    __table_args__ = {"extend_existing": True}
    space_id = db.Column(db.String)


# model class schema definitions with Marshmallow (response output)


class spaceSchema(ma.ModelSchema):
    class Meta:
        model = Space


class eventSchema(ma.ModelSchema):
    class Meta:
        model = Event


class groupSchema(ma.ModelSchema):
    class Meta:
        model = Group


class weatherSchema(ma.ModelSchema):
    class Meta:
        model = Weather


class reviewSchema(ma.ModelSchema):
    class Meta:
        model = Review


whoosh.index_all(api)
