from flask import Flask, jsonify, request, abort
from api import api, models
from api.utils import param_filters
from api.services import handlers
from flask.json import loads
from sqlalchemy.exc import ArgumentError

# get schemas from models
spaces_schema = models.spaceSchema(many=True)
space_schema = models.spaceSchema(many=False)
events_schema = models.eventSchema(many=True)
event_schema = models.eventSchema(many=False)
groups_schema = models.groupSchema(many=True)
group_schema = models.groupSchema(many=False)
weather_schema = models.weatherSchema(many=True)
review_schema = models.reviewSchema(many=True)


################################################################################
### Events API
################################################################################


@api.route("/events", methods=["GET"])
def getEvents():
    """ Main GET endpoint for Events """
    db_session = models.Session()
    args = param_filters.filter_event_query_params(request)
    result, num_pages, total = handlers.getEventsHandler(args, db_session)
    resp = events_schema.jsonify(result)
    resp.headers.add("Access-Control-Expose-Headers", "num-pages")
    resp.headers.add("Access-Control-Expose-Headers", "total")
    resp.headers.add("num-pages", num_pages)
    resp.headers.add("total", total)
    db_session.close()
    return resp, 200


# returns data entry for event with id
@api.route("/events/<id>", methods=["GET"])
def getEventsInstance(id):
    """ Get a single Event instance by event id """
    event = models.Event.query.filter_by(event_id=id).first_or_404()
    return event_schema.jsonify(event), 200


@api.route("/events/group/<group_id>", methods=["GET"])
def getEventInstancesForGroup(group_id):
    """ Get Events associated with a group id """
    events = models.Event.query.filter_by(group_id=group_id).all()
    return events_schema.jsonify(events), 200


@api.route("/events/space/<space_id>", methods=["GET"])
def getEventInstancesForSpace(space_id):
    """ Get Events associated with a space id """
    events = models.Event.query.filter_by(space_id=space_id).all()
    return events_schema.jsonify(events), 200


################################################################################
### Groups API
################################################################################


@api.route("/groups", methods=["GET"])
def getGroups():
    """ Main GET endpoing for Groups """
    args = param_filters.filter_group_query_params(request)
    db_session = models.Session()
    results, num_pages, total = handlers.getGroupsHandler(args, db_session)
    resp = groups_schema.jsonify(results)
    resp.headers.add("Access-Control-Expose-Headers", "num-pages")
    resp.headers.add("Access-Control-Expose-Headers", "total")
    resp.headers["num-pages"] = num_pages
    resp.headers["total"] = total
    db_session.close()
    return resp, 200


@api.route("/groups/space/<space_id>", methods=["GET"])
def getGroupInstancesForSpace(space_id):
    """ Get Groups associated with `space_id` """
    db_session = models.Session()
    groups = (
        db_session.query(models.Group)
        .filter(
            models.Group.group_id.in_(
                db_session.query(models.Event.group_id).filter(
                    models.Event.space_id == space_id
                )
            )
        )
        .all()
    )
    db_session.close()
    return groups_schema.jsonify(groups), 200


@api.route("/groups/<id>", methods=["GET"])
def getGroupsInstance(id):
    """ Get a single Group instance with `group_id` """
    group = models.Group.query.filter_by(group_id=id).first_or_404()
    return group_schema.jsonify(group), 200


@api.route("/groups/categories", methods=["GET"])
def getGroupCategories():
    """ Get all distinct Group categories """
    categories = list(set([x.categories for x in models.Group.query.all()]))
    resp = jsonify(categories)
    resp.status_code = 200
    return resp


################################################################################
### Spaces API
################################################################################


@api.route("/spaces", methods=["GET"])
def getSpaces():
    """ Main GET endpoint for Spaces """
    args = param_filters.filter_space_query_params(request)
    db_session = models.Session()
    result, num_pages, total = handlers.getSpacesHandler(args, db_session)
    resp = spaces_schema.jsonify(result)
    resp.headers.add("Access-Control-Expose-Headers", "num-pages")
    resp.headers.add("Access-Control-Expose-Headers", "total")
    resp.headers.add("num-pages", num_pages)
    resp.headers.add("total", total)
    db_session.close()
    print("HERE")
    return resp, 200


@api.route("/spaces/<id>", methods=["GET"])
def getSpacesInstance(id):
    """ Get a single Space instance by space_id """
    space = models.Space.query.filter_by(space_id=id).first_or_404()
    return space_schema.jsonify(space), 200


@api.route("/spaces/group/<group_id>", methods=["GET"])
def getSpaceForGroup(group_id):
    """ Get a single space instance associated with a group_id """
    space = models.Space.query.filter_by(
        space_id=(
            models.Event.query.with_entities(models.Event.space_id)
            .filter_by(group_id=group_id)
            .limit(1)
        )
    ).first_or_404()
    return space_schema.jsonify(space), 200


@api.route("/spaces/names", methods=["GET"])
def getSpaceNames():
    """ Get all Space names available."""
    space_names = [
        {"name": x.name, "space_id": x.space_id} for x in models.Space.query.all()
    ]
    resp = jsonify(space_names)
    resp.status_code = 200
    return resp


@api.route("/weather/<space_id>", methods=["GET"])
def getWeatherInstance(space_id):
    """ Get all weather instances by space_id """
    weather = models.Weather.query.filter_by(space_id=space_id).all()
    return weather_schema.jsonify(weather), 200


@api.route("/reviews/<space_id>", methods=["GET"])
def getReviewInstance(space_id):
    """ Get all review instances by space_id """
    review = models.Review.query.filter_by(space_id=space_id)
    return review_schema.jsonify(review), 200
