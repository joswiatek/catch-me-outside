from flask import Flask
from flask_cors import CORS

api = Flask(__name__)
CORS(api)
api.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgres://master:glenndowning@catchmeoutside-db-prod.c2ebtxljrxtw.us-east-1.rds.amazonaws.com:5432/cmodb"


__all__ = ["services", "utils", "models", "routes"]
from api import services, utils, models, routes
