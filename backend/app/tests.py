#!/usr/bin/env python
# TODO: remove this import and import the functions you need. leaving it here as an example
from api import models
from api.services import filters
from api.utils import param_filters, utils
from unittest import main, TestCase
import werkzeug
import arrow
from sqlalchemy.sql.expression import or_


class MockRequest:
    def __init__(self, d):
        self.args = werkzeug.datastructures.MultiDict(d)


class MyUnitTests(TestCase):
    ###################
    ### SPACE TESTS ###
    ###################

    # Test space filter with no arguments
    def test_space_filter_query_args_empty(self):
        mock_request = MockRequest({})
        result = param_filters.filter_space_query_params(mock_request)
        expected = {"page": 1, "sort": "rating", "order": "desc"}
        self.assertEqual(expected, result)

    # Test space filter with dummy arguments
    def test_space_filter_query_args_dummy(self):
        mock_request = MockRequest({"dummy arg": 10})
        result = param_filters.filter_space_query_params(mock_request)
        expected = {"page": 1, "sort": "rating", "order": "desc"}
        self.assertEqual(expected, result)

    # Test space filter with various arguments
    def test_space_filter_query_args_success(self):
        mock_request = MockRequest(
            {"page": 1, "min_rating": "2.0", "max_rating": "4.0", "keyword": "test"}
        )
        result = param_filters.filter_space_query_params(mock_request)
        expected = {
            "page": 1,
            "sort": "rating",
            "order": "desc",
            "min_rating": "2.0",
            "max_rating": "4.0",
            "keyword": "test",
        }
        self.assertEqual(expected, result)

    # Test space filter with invalid lat_long
    def test_space_filter_query_args_invalid_lat_long(self):
        mock_request = MockRequest({"page": 1, "user_loc": "invalid,invalid"})
        expected = "400 Bad Request: The browser (or proxy) sent a request that this server could not understand."
        try:
            result = param_filters.filter_space_query_params(mock_request)
        except werkzeug.exceptions.BadRequest as e:
            self.assertEqual(expected, str(e))

    # Test space filter with no lat_long
    def test_space_filter_query_args_no_lat_long(self):
        mock_request = MockRequest({"page": 1, "sort": "dist"})
        expected = "400 Bad Request: The browser (or proxy) sent a request that this server could not understand."
        try:
            result = param_filters.filter_space_query_params(mock_request)
        except werkzeug.exceptions.BadRequest as e:
            self.assertEqual(expected, str(e))

    ###################
    ### GROUP TESTS ###
    ###################

    # Test group filter with no arguments
    def test_group_filter_query_args_empty(self):
        mock_request = MockRequest({})
        result = param_filters.filter_group_query_params(mock_request)
        expected = {"page": 1, "sort": "member_count", "order": "desc"}
        self.assertEqual(expected, result)

    # Test group filter with dummy arguments
    def test_group_filter_query_args_empty(self):
        mock_request = MockRequest({"dummy arg": 10})
        result = param_filters.filter_group_query_params(mock_request)
        expected = {"page": 1, "sort": "member_count", "order": "desc"}
        self.assertEqual(expected, result)

    # Test group filter with various arguments
    def test_group_filter_query_args_success(self):
        mock_request = MockRequest(
            {"join_mode": "open", "category": "catA,catB,catC", "keyword": "test"}
        )
        result = param_filters.filter_group_query_params(mock_request)
        expected = {
            "page": 1,
            "sort": "member_count",
            "order": "desc",
            "keyword": "test",
            "join_mode": "open",
            "category": ["catA", "catB", "catC"],
        }
        self.assertEqual(expected, result)

    # Test group filter with invalid lat_long
    def test_group_filter_query_args_invalid_member_count(self):
        mock_request = MockRequest(
            {"page": 1, "min_member_count": 10, "max_member_count": 5}
        )
        expected = "400 Bad Request: The browser (or proxy) sent a request that this server could not understand."
        try:
            result = param_filters.filter_group_query_params(mock_request)
        except werkzeug.exceptions.BadRequest as e:
            self.assertEqual(expected, str(e))

    ###################
    ### EVENT TESTS ###
    ###################

    # Test event filter with no arguments
    def test_event_filter_query_args_empty(self):
        mock_request = MockRequest({})
        result = param_filters.filter_event_query_params(mock_request)
        expected = {"page": 1, "sort": "date", "order": "asc"}
        self.assertEqual(expected, result)

    # Test event filter with dummy arguments
    def test_event_filter_query_args_empty(self):
        mock_request = MockRequest({"dummy arg": 10})
        result = param_filters.filter_event_query_params(mock_request)
        expected = {"page": 1, "sort": "date", "order": "asc"}
        self.assertEqual(expected, result)

    # Test event filter with various arguments
    def test_event_filter_query_args_success(self):
        mock_request = MockRequest(
            {
                "keyword": "test",
                "start_date": "2019-01-01",
                "end_date": "2019-01-03",
                "times_include": "morning,afternoon",
            }
        )
        result = param_filters.filter_event_query_params(mock_request)
        expected = {
            "page": 1,
            "sort": "date",
            "order": "asc",
            "keyword": "test",
            "start_date": "2019-01-01",
            "end_date": "2019-01-03",
            "times_include": {"morning", "afternoon"},
        }
        self.assertEqual(expected, result)

    # Test event filter with categories
    def test_event_filter_query_args_category(self):
        mock_request = MockRequest({"category": "catA,catB,catC"})
        result = param_filters.filter_event_query_params(mock_request)
        expected = {
            "page": 1,
            "sort": "date",
            "order": "asc",
            "category": ["catA", "catB", "catC"],
        }
        self.assertEqual(expected, result)

    # Test event filter with invalid lat_long
    def test_event_filter_query_args_invalid_lat_long(self):
        mock_request = MockRequest(
            {
                "page": 1,
                "sort": "dist"
                # no user_loc => error
            }
        )
        expected = "400 Bad Request: The browser (or proxy) sent a request that this server could not understand."
        try:
            result = param_filters.filter_event_query_params(mock_request)
        except werkzeug.exceptions.BadRequest as e:
            self.assertEqual(expected, str(e))

    #####################
    ### UTILITY TESTS ###
    #####################

    # Test first case of get_or_404
    def test_first_or_404_success(self):
        class Mock:
            def first(self):
                return 1

        result = utils.first_or_404(Mock())
        expected = 1
        self.assertEqual(expected, result)

    # Test 404 case of get_or_404
    def test_first_or_404_fail(self):
        class Mock:
            def first(self):
                return None

        expected = "404 Not Found: The requested URL was not found on the server. If you entered the URL manually please check your spelling and try again."
        try:
            result = utils.first_or_404(Mock())
        except werkzeug.exceptions.NotFound as e:
            self.assertEqual(expected, str(e))

    # Test get case of get_or_404
    def test_get_or_404_success(self):
        class Mock:
            def get(self, id):
                return id + 1

        result = utils.get_or_404(Mock(), 1)
        expected = 2
        self.assertEqual(expected, result)

    # Test 404 case of get_or_404
    def test_get_or_404_fail(self):
        class Mock:
            def get(self, id):
                return None

        expected = "404 Not Found: The requested URL was not found on the server. If you entered the URL manually please check your spelling and try again."
        try:
            result = utils.get_or_404(Mock(), 1)
        except werkzeug.exceptions.NotFound as e:
            self.assertEqual(expected, str(e))

    # Test time in range success case
    def test_time_in_range_success(self):
        hours = [
            [
                "Monday: 5:00 AM - 10:00 PM",
                "Tuesday: 5:00 AM - 10:00 PM",
                "Wednesday: 5:00 AM - 10:00 PM",
                "Thursday: 5:00 AM - 10:00 PM",
                "Friday: 5:00 AM - 10:00 PM",
                "Saturday: 5:00 AM - 10:00 PM",
                "Sunday: 5:00 AM - 10:00 PM",
            ]
        ]
        # 12 pm is inside the open hours
        user_time = arrow.get("2013-05-05 12:30:45", "YYYY-MM-DD HH:mm:ss")
        expected = True
        self.assertEqual(expected, utils.time_is_in_range(hours, user_time))

    # Test time in range false case
    def test_time_in_range_false(self):
        hours = [
            [
                "Monday: 5:00 AM - 10:00 PM",
                "Tuesday: 5:00 AM - 10:00 PM",
                "Wednesday: 5:00 AM - 10:00 PM",
                "Thursday: 5:00 AM - 10:00 PM",
                "Friday: 5:00 AM - 10:00 PM",
                "Saturday: 5:00 AM - 10:00 PM",
                "Sunday: 5:00 AM - 10:00 PM",
            ]
        ]
        # 3 am is outside the open hours
        user_time = arrow.get("2013-05-05 03:30:45", "YYYY-MM-DD HH:mm:ss")
        expected = False
        self.assertEqual(expected, utils.time_is_in_range(hours, user_time))

    def test_find_dist(self):
        # Dallas
        dallas = (32.7767, 96.7970)
        # Austin
        austin = (30.2672, 97.7431)

        expected = 182
        self.assertEqual(
            expected, int(utils.find_dist(dallas[0], dallas[1], austin[0], austin[1]))
        )

    # Test filtering on one time period
    def test_time_filter_one(self):
        class Mock:
            def filter(mock_self, param):
                self.assertEqual(
                    str(param),
                    str(models.Event.local_time.between("03:00:00", "11:59:59")),
                )
                return

        include_set = {"morning"}
        result = filters.time_filter(Mock(), include_set)

    # Test filtering on two time periods
    def test_time_filter_two(self):
        class Mock:
            def filter(mock_self, param):
                self.assertEqual(
                    str(param),
                    str(
                        or_(
                            models.Event.local_time.between("03:00:00", "11:59:59"),
                            models.Event.local_time.between("12:00:00", "17:59:59"),
                        )
                    ),
                )

        include_set = {"morning", "afternoon"}
        result = filters.time_filter(Mock(), include_set)


if __name__ == "__main__":
    main()
