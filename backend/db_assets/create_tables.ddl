CREATE TABLE spaces (
space_id text NOT NULL,
name text NOT NULL,
lat double precision NOT NULL,
long double precision NOT NULL,
address text NOT NULL,
phone_number text NOT NULL,
url text NOT NULL,
open_hours text[] NOT NULL,
types text[] NOT NULL,
rating double precision,
photos text[],
PRIMARY KEY (space_id)
);
CREATE TABLE weather (
weather_id serial NOT NULL,
space_id text NOT NULL REFERENCES spaces(space_id) DEFERRABLE,
high text NOT NULL,
low text NOT NULL,
local_date date NOT NULL,
local_time time NOT NULL,
description text NOT NULL,
icon_id text NOT NULL,
PRIMARY KEY (weather_id)
);
CREATE TABLE reviews (
review_id serial NOT NULL,
space_id text NOT NULL REFERENCES spaces(space_id) DEFERRABLE,
name text NOT NULL,
rating double precision NOT NULL,
photo text,
url text,
time text NOT NULL,
body text NOT NULL,
PRIMARY KEY (review_id)
);
CREATE TABLE groups (
group_id text NOT NULL,
name text NOT NULL,
status text NOT NULL,
link text NOT NULL,
url_name text NOT NULL,
description text NOT NULL,
creation_time bigint NOT NULL,
categories text NOT NULL,
join_mode text NOT NULL,
city text NOT NULL,
country text NOT NULL,
member_count integer NOT NULL,
organizer_name text NOT NULL,
organizer_bio text NOT NULL,
organizer_photo text NOT NULL,
group_photo text NOT NULL,
PRIMARY KEY (group_id)
);
CREATE TABLE events (
event_id text NOT NULL,
space_id text NOT NULL REFERENCES spaces(space_id) DEFERRABLE INITIALLY IMMEDIATE,
group_id text NOT NULL REFERENCES groups(group_id) DEFERRABLE INITIALLY IMMEDIATE,
name text NOT NULL,
rsvp_limit integer NOT NULL,
rsvp_count integer NOT NULL,
status text NOT NULL,
local_date date NOT NULL,
local_time time NOT NULL,
venue_name text NOT NULL,
venue_address text NOT NULL,
link text NOT NULL,
description text NOT NULL,
how_to_find_us text NOT NULL,
category text[] NOT NULL,
photo text NOT NULL,
description_tags text[] NOT NULL,
PRIMARY KEY (event_id)
);
