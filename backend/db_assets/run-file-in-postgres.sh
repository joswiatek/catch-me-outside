#!/bin/sh
# Running this file will run the sql in the file given in the first argument
psql "dbname=cmodb host=$DB_HOST user=$DB_USER password=$DB_PASS port=5432 sslmode=require" -f $1
