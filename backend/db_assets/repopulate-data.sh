#!/bin/bash
BACKEND_DIR="$HOME/cmo-repo/backend/"
SHELL="/bin/bash"

# This script will repopulate the events, groups, weather, and reviews in the
# postgres database. It's designed to run nightly. It first drops all the
# tables, then queries our APIs to gather data and INSERT it into the database.

# Load DB secrets
source $BACKEND_DIR/db_assets/export_db_keys.sh

# Kill all other connections
$SHELL $BACKEND_DIR/db_assets/run-file-in-postgres.sh $BACKEND_DIR/db_assets/kill-all-connections.sql

# Drop tables
$SHELL $BACKEND_DIR/db_assets/run-file-in-postgres.sh $BACKEND_DIR/db_assets/drop-tables.sql

# Create tables
$SHELL $BACKEND_DIR/db_assets/run-file-in-postgres.sh $BACKEND_DIR/db_assets/create_tables.ddl

# Query data and INSERT into the database
python $BACKEND_DIR/insert_db.py -a
