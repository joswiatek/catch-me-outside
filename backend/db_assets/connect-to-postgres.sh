#!/bin/sh
# Running this file will establish a connection to the postgres db
psql "dbname=cmodb host=$DB_HOST user=$DB_USER password=$DB_PASS port=5432 sslmode=require"
