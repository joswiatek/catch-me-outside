""" Python Script used to pull and insert data from API sources into the db."""
import json
import requests
import sys
import pickle
import psycopg2
import time
import numpy as np
import argparse
import nltk
import os
from textblob import TextBlob as tb
import math
from os import environ

CURR_DIR=os.path.dirname(os.path.realpath(__file__))
print(CURR_DIR)

db_params = {
    'host': environ['DB_HOST'],
    'database':'cmodb',
    'user': environ['DB_USER'],
    'password': environ['DB_PASS'],
    'port': 5432
}

english_words = {}
with open(os.path.join(CURR_DIR, 'db_assets/words_alpha.txt')) as word_file:
    english_words = set(word_file.read().split())

def db_execute(sql, values=None):
    """ Executes a write query to the database. Caller must commit changes and
    close `db_conn`.
    """
    try:
        # create a cursor
        cur = db_conn.cursor()
        # execute a statement
        if values is None:
            cur.execute(sql)
        else:
            cur.execute(sql, values)
        # close the communication with the PostgreSQL
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print("ERROR during db_execute: ", error)
        if db_conn is not None:
            db_conn.close()
        sys.exit(1)

def db_query(sql):
    """ Queries data from database."""
    db_conn = psycopg2.connect(**db_params)
    try:
        cur = db_conn.cursor()
        cur.execute(sql)
        results = cur.fetchall()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print("ERROR during db_query: ", error)
        if db_conn is not None:
            db_conn.close()
    finally:
        db_conn.close()

    return results


def insert_places(values):
    sql = """INSERT INTO spaces (space_id, name, lat, long, address,
            phone_number, url, open_hours, types, rating, photos) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"""
    db_execute(sql, tuple(values))

def insert_review(values):
    sql = """INSERT INTO reviews (space_id, name, rating, photo, url,
            time, body) VALUES (%s,%s,%s,%s,%s,%s,%s);"""
    db_execute(sql, values)

def insert_events(event_dict):
    disable_trigger = """SET CONSTRAINTS events_group_id_fkey DEFERRED;"""
    db_execute(disable_trigger)
    disable_trigger = """SET CONSTRAINTS events_space_id_fkey DEFERRED;"""
    db_execute(disable_trigger)
    key_list = ['event_id', 'space_id', 'group_id', 'name', 'rsvp_limit',
            'rsvp_count', 'status', 'local_date', 'local_time', 'venue_name', 'venue_address',
            'link', 'description', 'how_to_find_us', 'category', 'photo', 'description_tags']
    e_ind = 0
    e_total = len(event_dict.keys())
    for id in event_dict.keys():
        event= event_dict[id]
        values = [event[key] for key in key_list]
        sql = """INSERT INTO events (event_id, space_id, group_id, name, rsvp_limit,
                rsvp_count, status, local_date, local_time, venue_name, venue_address,
                link, description, how_to_find_us, category, photo, description_tags) VALUES (%s,%s,%s,%s,%s,
                %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"""
        sys.stdout.write("\rInserting event (%d/%d)..." % (e_ind, e_total))
        sys.stdout.flush()
        db_execute(sql, tuple(values))
        e_ind += 1
    sys.stdout.flush()
    print("Finished inserting events!")


def insert_groups(values):
    sql = """INSERT INTO groups (group_id, name, status, link, url_name, description,
            creation_time, categories, join_mode, city, country, member_count,
            organizer_name, organizer_bio, organizer_photo, group_photo) VALUES
            (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"""
    db_execute(sql, tuple(values))


def insert_weather(values):
    sql = """INSERT INTO weather (space_id, high, low, local_date, local_time,
            description, icon_id) VALUES (%s,%s,%s,%s,%s,%s,%s);"""
    db_execute(sql, tuple(values))


def request_places():
    db_conn = psycopg2.connect(**db_params)
    #r = requests.get('https://maps.googleapis.com/maps/api/place/textsearch/json',
    #    params={
    #        'key': environ['SPACE_API']
    #        'query': "parks+in+austin+texas"})
    #print(r.url)
    #data = json.loads(r.text)
    with open(os.path.join(CURR_DIR,'db_assets/places_data.dat'), 'rb') as f:
        data = pickle.load(f)
    with open(os.path.join(CURR_DIR,'db_assets/details_data.dat'), 'rb') as f:
        detail_data = pickle.load(f)
    p_ind = 0
    p_total = len(data['results'])
    for place in data['results']:
        place_data = []
        id = place['place_id']
        lat = place['geometry']['location']['lat']
        lng = place['geometry']['location']['lng']
        place_data.append(id)
        place_data.append(place['name'])
        place_data.append(str(lat))
        place_data.append(str(lng))
        place_data.append(place['formatted_address'])
        # Details
        #r = requests.get('https://maps.googleapis.com/maps/api/place/details/json
        #    params={
        #        'key': environ['SPACE_API'],
        #        'placeid': )
        # details = json.loads(r.text)['result']
        details = detail_data[id]['result']
        place_data.append(details['formatted_phone_number'])
        place_data.append(details['website'])
        if 'opening_hours' in details:
            open_hours = details['opening_hours']['weekday_text']
        else:
            open_hours= []
        place_data.append(open_hours)
        types = [type.lower() for type in details['types']]
        place_data.append(types)
        place_data.append(str(details['rating']))
        photo_list = []
        for photo in details['photos']:
            url = photo['html_attributions'][0]
            url = url.split("href=\"")[1].split("\">")[0]
            photo_ref = photo['photo_reference']
            photo_list.append(json.dumps({'html_attribution': url, 'photo_reference': photo_ref}))
        place_data.append(photo_list)
        sys.stdout.write("\rInserting place (%d/%d)..." % (p_ind, p_total))
        sys.stdout.flush()
        insert_places(place_data)
        p_ind += 1
        r_ind = 0
        r_total = len(details['reviews'])
        for review in details['reviews']:
            review_data = []
            review_data.append(id)
            review_data.append(review['author_name'])
            review_data.append(review['rating'])
            review_data.append(review['profile_photo_url'])
            review_data.append(review['author_url'])
            review_data.append(str(review['time']))
            review_data.append(review['text'])
            sys.stdout.write("\rInserting review (%d/%d)..." % (r_ind, r_total))
            sys.stdout.flush()
            insert_review(review_data)
            r_ind += 1
    sys.stdout.flush()
    print("Finished committing Places and Reviews!")

def get_place_data():
    sql = """SELECT space_id, lat, long from spaces;"""
    results = db_query(sql)
    return {place[0]: (float(place[1]), float(place[2])) for place in results}

def update_events_groups(place_data):
    event_dict = {}
    for id in place_data.keys():
        place = place_data[id]
        request_events(place[0], place[1], id, 4, "Community & Environment", event_dict)
        request_events(place[0], place[1], id, 23, "Outdoors & Adventure", event_dict)
        request_events(place[0], place[1], id, 9, "Fitness", event_dict)
    # If events show up in multiple places, find closest place
    for key in event_dict.keys():
        ind = find_event_dist(event_dict[key], place_data)
        event_dict[key]['space_id'] = event_dict[key]['space_id'][ind]
        event_dict[key].pop('lat')
        event_dict[key].pop('lon')
        event_dict[key]['category'] = [c.lower() for c in list(event_dict[key]['category'])]
    group_urls = [event_dict[key].pop('group_url_name') for key in event_dict.keys()]

    # Generate description tags for events using TFIDF scores
    event_dict = generate_tags(event_dict)

    # Insert the events into the database
    insert_events(event_dict)

    g_ind = 0
    g_total = len(set(group_urls))
    for group in set(group_urls):
        sys.stdout.write("\rInserting group (%d/%d)..." % (g_ind, g_total))
        sys.stdout.flush()
        request_groups(group)
        g_ind+=1
        time.sleep(.4)
    sys.stdout.flush()
    print("Finished inserting groups!")

def generate_tags(event_dict):
    """Generates tags for event descriptions based on TFIDF scores across all events"""
    def tf(word, blob):
        # Term frequency
        return blob.words.count(word) / len(blob.words)
    def n_containing(word, bloblist):
        # Document frequency
        return sum(1 for blob in bloblist if word in blob[1].words)
    def idf(word, bloblist):
        # Inverse document frequency
        return math.log(len(bloblist) / (1 + n_containing(word, bloblist)))
    def tfidf(word, blob, bloblist):
        # Term frequency inverse document frequency
        return tf(word, blob) * (idf(word, bloblist) * 0.25)

    nltk.download('stopwords')
    stop_words = set(nltk.corpus.stopwords.words('english'))

    bloblist = []
    for key, value in event_dict.items():
        description = value['description']
        tokens = [w for w in nltk.tokenize.word_tokenize(description) if w not in stop_words and w in english_words]
        filtered_description = ' '.join(tokens).lower()
        bloblist.append((key,tb(filtered_description)))

    for i, (key, blob) in enumerate(bloblist):
        scores = {word: tfidf(word, blob, bloblist) for word in blob.words}
        sorted_words = sorted(scores.items(), key=lambda x: x[1], reverse=True)
        filtered_words = [w for w in sorted_words if str(w[0]) in english_words and not check_sentiment_neg(str(w[0]))]
        # Try to take the top three tags
        try:
            tags = filtered_words[:3]
            tag_words = [str(tag[0]) for tag in tags]
            event_dict[key]['description_tags'] = tag_words
        except Exception as e:
            # If there aren't three then just use empty strings
            event_dict[key]['description_tags'] = ['','','']

    return event_dict

def check_sentiment_neg(word):
    """Returns true if the sentiment of the word is negative."""
    return tb(str(word)).sentiment.polarity < 0

def find_event_dist(event, place_data):
    """ Finds index of closest place_id for an event"""
    if len(event['space_id']) == 1 or event['lat'] is None:
        return 0
    dists = [(event['lat'] - place_data[key][0])**2 + (event['lon'] - place_data[key][1])**2 for key in event['space_id']]
    return np.argmin(dists)

def request_events(lat, lon, place_id, category_id, category_name, event_dict):
    # Community & Environment
    r = requests.get('https://api.meetup.com/find/upcoming_events',
        params={
            'sign':'True',
            'key': environ['MEETUP_API'],
            'lat': lat,
            'lon': lon,
            'radius': 15,
            'page': 250,
            'fields': 'plain_text_description,group_photo,featured_photo',
            'topic_category': category_id})
    data = json.loads(r.text)
    for event in data['events']:
        try:
            if event['visibility'] == "public":
                if event['id'] not in event_dict:
                    event_data = {}
                    event_data['event_id'] = str(event["id"])
                    event_data['space_id'] = [place_id]
                    event_data['group_id'] = str(event['group']['id'])
                    event_data['name'] = event['name']
                    event_data['link'] = event.get('link', "")
                    event_data['description'] = event.get('plain_text_description', "")
                    event_data['rsvp_limit'] = int(event.get('rsvp_limit', -1))
                    event_data['rsvp_count'] = int(str(event.get('yes_rsvp_count', 0)))
                    event_data['status'] = event.get('status', 'upcoming')
                    event_data['local_date'] = event['local_date']
                    event_data['local_time'] = event['local_time']
                    event_data['how_to_find_us'] = event.get('how_to_find_us', "")
                    if 'featured_photo' in event:
                        event_data['photo'] = event['featured_photo'].get('highres_link', "")
                    elif 'group' in event and 'photo' in event['group']:
                        event_data['photo'] = event['group']['photo'].get('highres_link', "")
                    else:
                        event_data['photo'] = ''
                    if 'venue' in event:
                        event_data['venue_name'] = event['venue'].get('name', "")
                        event_data['venue_address'] = event['venue'].get('address_1', "")
                        event_data['lat'] = event['venue'].get('lat', None)
                        event_data['lon'] = event['venue'].get('lon', None)
                    else:
                        event_data['venue_name'] = ""
                        event_data['venue_address'] = ""
                        event_data['lat'] = None
                        event_data['lon'] = None
                    event_data['category'] = set([str(category_name).lower()])
                    event_data['group_url_name'] = event['group']['urlname']
                    event_dict[event['id']] = event_data
                else:
                    event_dict[event['id']]['space_id'].append(place_id)
                    event_dict[event['id']]['category'].add(category_name)
        except KeyError as e:
            print(e)
            print(json.dumps(event, indent=True))
            sys.exit(10)

def request_groups(urlname):
    r = requests.get('https://api.meetup.com/%s' %(urlname),
        params={
            'sign':'True',
            'key': environ['MEETUP_API'],
            'fields': 'plain_text_description'})
    #print(json.dumps(r.text, indent=True))
    data = json.loads(r.text)
    try:
        group_data = []
        group_data.append(data['id'])
        group_data.append(data['name'])
        group_data.append(data['status'])
        group_data.append(data['link'])
        group_data.append(data['urlname'])
        group_data.append(data['plain_text_description'])
        group_data.append(data['created'])
        group_data.append(str(data['category']['name']).lower())
        group_data.append(data['join_mode'])
        group_data.append(data['city'])
        group_data.append(data['country'])
        group_data.append(data['members'])
        group_data.append(data['organizer']['name'])
        group_data.append(data['organizer']['bio'])
        if 'photo' in data['organizer']:
            group_data.append(data['organizer']['photo'].get('highres_link', ""))
        else:
            group_data.append("")
        if 'group_photo' in data:
            group_data.append(data['group_photo'].get('highres_link'))
        else:
            group_data.append("")
        insert_groups(group_data)
    except KeyError as e:
        print("Request_Group: ", e)
        print(json.dumps(data, indent=True))
        sys.exit(1)


def request_weather(place_id, lat, lon):
        r = requests.get('https://api.openweathermap.org/data/2.5/forecast',
            params={
                'appid': environ['WEATHER_API'],
                'lat': lat,
                'lon': lon,
                'units':'imperial'})

        data = json.loads(r.text)['list']
        w_ind = 0
        w_total = len(data)
        for item in data:
            weather_data = []
            weather_data.append(place_id)
            weather_data.append(str(item['main']['temp_max']))
            weather_data.append(str(item['main']['temp_min']))
            weather_data.append(item['dt_txt'].split(" ")[0])
            weather_data.append(item['dt_txt'].split(" ")[1])
            weather_data.append(item['weather'][0]['description'])
            weather_data.append(item['weather'][0]['id'])
            sys.stdout.write("\rInserting weather (%d/%d)..." % (w_ind, w_total))
            sys.stdout.flush()
            insert_weather(weather_data)
            w_ind +=1
        sys.stdout.flush()
        print("Finished inserting weather!")

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--spaces", "-s",  help="Pull and insert spaces data",
                    action="store_true")
    parser.add_argument("--event", "-e", help="Pull and insert events and groups data",
                    action="store_true")
    parser.add_argument("--weather", "-w", help="Pull and insert weather data",
                    action="store_true")
    parser.add_argument("--all", "-a", help="Pull and insert all data",
                    action="store_true")
    args = parser.parse_args()
    db_conn = None
    place_data = None
    if args.spaces or args.all:
         db_conn = psycopg2.connect(**db_params)
         request_places()
         db_conn.commit()
         db_conn.close()
    if args.event or args.all:
         if place_data is None:
             place_data = get_place_data()
         db_conn = psycopg2.connect(**db_params)
         update_events_groups(place_data)
         db_conn.commit()
         db_conn.close()
    if args.weather or args.all:
         db_conn = psycopg2.connect(**db_params)
         if place_data is None:
             place_data = get_place_data()
         for id in place_data.keys():
             place = place_data[id]
             request_weather(id, place[0], place[1])
         db_conn.commit()
         db_conn.close()
    print("Checking Space event data...")
    space_q = """SELECT space_id, name from spaces;"""
    spaces = db_query(space_q)
    for item in spaces:
        query = """SELECT count(*) from events where space_id = \'%s\'""" % item[0]
        result = db_query(query)
        print(item[1], "has ", result, " events.")
