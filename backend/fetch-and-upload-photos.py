# Copyright 2010-2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# This file is licensed under the Apache License, Version 2.0 (the "License").
# You may not use this file except in compliance with the License. A copy of the
# License is located at
#
# http://aws.amazon.com/apache2.0/
#
# This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either express or implied. See the License for the specific
# language governing permissions and limitations under the License.

import logging
import boto3
from botocore.exceptions import ClientError
import requests
import googlemaps
from functools import reduce

client = googlemaps.Client(key="AIzaSyA6TyP3kmmF9XraDRRxpla7Ogeg7bpF8Ic")


def put_object(dest_bucket_name, dest_object_name, src_data):
    """Add an object to an Amazon S3 bucket

    The src_data argument must be of type bytes or a string that references
    a file specification.

    :param dest_bucket_name: string
    :param dest_object_name: string
    :param src_data: bytes of data or string reference to file spec
    :return: True if src_data was added to dest_bucket/dest_object, otherwise
    False
    """

    # Construct Body= parameter
    if isinstance(src_data, bytes):
        object_data = src_data
    elif isinstance(src_data, str):
        try:
            object_data = open(src_data, "rb")
            # possible FileNotFoundError/IOError exception
        except Exception as e:
            logging.error(e)
            return False
    else:
        logging.error(
            "Type of "
            + str(type(src_data))
            + " for the argument 'src_data' is not supported."
        )
        return False

    # Put the object
    s3 = boto3.client("s3")
    try:
        s3.put_object(Bucket=dest_bucket_name, Key=dest_object_name, Body=object_data)
    except ClientError as e:
        # AllAccessDisabled error == bucket not found
        # NoSuchKey or InvalidRequest error == (dest bucket/obj == src bucket/obj)
        logging.error(e)
        return False
    finally:
        if isinstance(src_data, str):
            object_data.close()
    return True


def main():
    """Exercise put_object()"""

    # Assign these values before running the program
    bucket_name = "catch-me-outside-assets"
    max_image_width = 600
    # Alternatively, specify object contents using bytes.
    # filename = b'This is the data to store in the S3 object.'

    # Set up logging
    logging.basicConfig(
        level=logging.DEBUG, format="%(levelname)s: %(asctime)s: %(message)s"
    )

    url = "https://maps.googleapis.com/maps/api/place/photo"
    spaces_responses = [
        requests.get("http://devapi.catchmeoutside.today/spaces?page=" + str(i)).text
        for i in range(1, 3)
    ]
    spaces = eval(spaces_responses[0]) + eval(spaces_responses[1])

    for space in spaces:
        print("Fetching photo for: %s" % space["name"])
        print("space_id: %s" % space["space_id"])
        for photo in space["photos"]:
            photo = eval(photo)

            image = client.places_photo(
                photo["photo_reference"], max_width=max_image_width
            )
            full_image = reduce(lambda x, y: x + y, image)
            object_name = (
                "google-places-photos/"
                + space["space_id"]
                + "/"
                + photo["photo_reference"]
            )

            # Put the object into the bucket
            success = put_object(bucket_name, object_name, full_image)
            if success:
                logging.info(f"Added {object_name} to {bucket_name}")


if __name__ == "__main__":
    main()
